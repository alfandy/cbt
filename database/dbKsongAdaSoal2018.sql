-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2018 at 06:57 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cbt201802`
--

-- --------------------------------------------------------

--
-- Table structure for table `log_masuk`
--

CREATE TABLE `log_masuk` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `id_ruangan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_masuk`
--

-- --------------------------------------------------------

--
-- Table structure for table `tr_guru_tes`
--

CREATE TABLE `tr_guru_tes` (
  `id` int(6) NOT NULL,
  `id_jenis_soal` int(6) NOT NULL,
  `jumlah_soal` int(6) NOT NULL,
  `waktu` int(6) NOT NULL,
  `jenis` enum('acak','set') NOT NULL,
  `detil_jenis` varchar(500) NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `terlambat` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_guru_tes`
--

INSERT INTO `tr_guru_tes` (`id`, `id_jenis_soal`, `jumlah_soal`, `waktu`, `jenis`, `detil_jenis`, `tgl_mulai`, `terlambat`) VALUES
(1, 1, 60, 90, 'acak', '', '2017-06-23 06:05:00', 3);

-- --------------------------------------------------------

--
-- Table structure for table `t_admin`
--

CREATE TABLE `t_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('admin','petugas') NOT NULL,
  `kon_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_admin`
--

INSERT INTO `t_admin` (`id`, `username`, `password`, `level`, `kon_id`) VALUES
(1, 'admin', '767e955464233667bfd855686a55b352', 'admin', 0),
(2, 'petugas1', '21232f297a57a5a743894a0e4a801fc3', 'petugas', 1),
(3, 'petugas2', '21232f297a57a5a743894a0e4a801fc3', 'petugas', 2),
(4, 'petugas3', '21232f297a57a5a743894a0e4a801fc3', 'petugas', 3);

-- --------------------------------------------------------

--
-- Table structure for table `t_gelombang`
--

CREATE TABLE `t_gelombang` (
  `id_gelombang` int(11) NOT NULL,
  `gelombang` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_gelombang`
--

INSERT INTO `t_gelombang` (`id_gelombang`, `gelombang`) VALUES
(1, 'I'),
(2, 'II'),
(3, 'III'),
(4, 'IV');

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis_soal`
--

CREATE TABLE `t_jenis_soal` (
  `id` int(11) NOT NULL,
  `jenis_soal` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jenis_soal`
--

INSERT INTO `t_jenis_soal` (`id`, `jenis_soal`) VALUES
(1, 'TPA'),
(2, 'Sains'),
(3, 'Soshum');

-- --------------------------------------------------------

--
-- Table structure for table `t_list_option_soal`
--

CREATE TABLE `t_list_option_soal` (
  `kd_option_soal` varchar(10) NOT NULL,
  `option_soal` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_log_soal`
--

CREATE TABLE `t_log_soal` (
  `id` int(11) NOT NULL,
  `kd_soal` varchar(10) NOT NULL,
  `kd_peserta` varchar(25) NOT NULL,
  `list_soal` longtext NOT NULL,
  `list_jawaban` longtext NOT NULL,
  `jml_benar` int(6) NOT NULL,
  `jml_salah` int(6) NOT NULL,
  `nilai` int(6) NOT NULL,
  `nilai_bobot` int(6) NOT NULL,
  `waktu_mulai` datetime NOT NULL,
  `waktu_selesai` datetime NOT NULL,
  `waktu` datetime NOT NULL,
  `status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_option_soal`
--

CREATE TABLE `t_option_soal` (
  `id_jawaban` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `file` varchar(150) NOT NULL,
  `tipe_file` varchar(50) NOT NULL,
  `jawaban` longtext NOT NULL,
  `benar` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_option_soal`
--

INSERT INTO `t_option_soal` (`id_jawaban`, `id_soal`, `file`, `tipe_file`, `jawaban`, `benar`) VALUES
(80, 26, 'gambar_soal_80.png', 'image/png', '', 'S'),
(81, 26, 'gambar_soal_81.png', 'image/png', '', 'B'),
(82, 26, 'gambar_soal_82.png', 'image/png', '', 'S'),
(83, 26, 'gambar_soal_83.png', 'image/png', '', 'S'),
(84, 26, 'gambar_soal_84.png', 'image/png', '', 'S'),
(85, 27, 'gambar_soal_85.png', 'image/png', '', 'S'),
(86, 27, 'gambar_soal_86.png', 'image/png', '', 'B'),
(87, 27, 'gambar_soal_87.png', 'image/png', '', 'S'),
(88, 27, 'gambar_soal_88.png', 'image/png', '', 'S'),
(89, 27, 'gambar_soal_89.png', 'image/png', '', 'S'),
(90, 28, 'gambar_soal_90.png', 'image/png', '', 'S'),
(91, 28, 'gambar_soal_91.png', 'image/png', '', 'S'),
(92, 28, 'gambar_soal_92.png', 'image/png', '', 'S'),
(93, 28, 'gambar_soal_93.png', 'image/png', '', 'S'),
(94, 28, 'gambar_soal_94.png', 'image/png', '', 'B'),
(95, 29, 'gambar_soal_95.png', 'image/png', '', 'S'),
(96, 29, 'gambar_soal_96.png', 'image/png', '', 'S'),
(97, 29, 'gambar_soal_97.png', 'image/png', '', 'S'),
(98, 29, 'gambar_soal_98.png', 'image/png', '', 'B'),
(99, 29, 'gambar_soal_99.png', 'image/png', '', 'S'),
(100, 30, 'gambar_soal_100.png', 'image/png', '', 'S'),
(101, 30, 'gambar_soal_101.png', 'image/png', '', 'B'),
(102, 30, 'gambar_soal_102.png', 'image/png', '', 'S'),
(103, 30, 'gambar_soal_103.png', 'image/png', '', 'S'),
(104, 30, 'gambar_soal_104.png', 'image/png', '', 'S'),
(105, 31, 'gambar_soal_105.png', 'image/png', '', 'S'),
(106, 31, 'gambar_soal_106.png', 'image/png', '', 'S'),
(107, 31, 'gambar_soal_107.png', 'image/png', '', 'B'),
(108, 31, 'gambar_soal_108.png', 'image/png', '', 'S'),
(109, 31, 'gambar_soal_109.png', 'image/png', '', 'S'),
(110, 32, 'gambar_soal_110.png', 'image/png', '', 'S'),
(111, 32, 'gambar_soal_111.png', 'image/png', '', 'S'),
(112, 32, 'gambar_soal_112.png', 'image/png', '', 'S'),
(113, 32, 'gambar_soal_113.png', 'image/png', '', 'B'),
(114, 32, 'gambar_soal_114.png', 'image/png', '', 'S'),
(115, 33, 'gambar_soal_115.png', 'image/png', '', 'S'),
(116, 33, 'gambar_soal_116.png', 'image/png', '', 'S'),
(117, 33, 'gambar_soal_117.png', 'image/png', '', 'S'),
(118, 33, 'gambar_soal_118.png', 'image/png', '', 'B'),
(119, 33, 'gambar_soal_119.png', 'image/png', '', 'S'),
(120, 34, 'gambar_soal_120.png', 'image/png', '', 'S'),
(121, 34, 'gambar_soal_121.png', 'image/png', '', 'S'),
(122, 34, 'gambar_soal_122.png', 'image/png', '', 'B'),
(123, 34, 'gambar_soal_123.png', 'image/png', '', 'S'),
(124, 34, 'gambar_soal_124.png', 'image/png', '', 'S'),
(125, 35, 'gambar_soal_125.png', 'image/png', '', 'B'),
(126, 35, 'gambar_soal_126.png', 'image/png', '', 'S'),
(127, 35, 'gambar_soal_127.png', 'image/png', '', 'S'),
(128, 35, 'gambar_soal_128.png', 'image/png', '', 'S'),
(129, 35, 'gambar_soal_129.png', 'image/png', '', 'S'),
(130, 36, '', '', '<p>David</p>\r\n', 'B'),
(131, 36, '', '', '<p>Dimas</p>\r\n', 'B'),
(132, 36, '', '', '<p>Sigit</p>\r\n', 'B'),
(133, 36, '', '', '<p>Candra</p>\r\n', 'B'),
(134, 36, '', '', '<p>Andi</p>\r\n', 'B'),
(135, 37, '', '', '<p>Pabrik Baja</p>\r\n', 'B'),
(136, 37, '', '', '<p>Pabrik Keramik</p>\r\n', 'B'),
(137, 37, '', '', '<p>Pabrik Kaca</p>\r\n', 'B'),
(138, 37, '', '', '<p>Pabrik Kayu</p>\r\n', 'B'),
(139, 37, '', '', '<p>Pabrik Tanah.</p>\r\n', 'B'),
(140, 38, '', '', '<p>P</p>\r\n', 'S'),
(141, 38, '', '', '<p>T</p>\r\n', 'B'),
(142, 38, '', '', '<p>Q</p>\r\n', 'S'),
(143, 38, '', '', '<p>R</p>\r\n', 'S'),
(144, 38, '', '', '<p>S</p>\r\n', 'S'),
(145, 39, '', '', '<p>35</p>\r\n', 'B'),
(146, 39, '', '', '<p>15</p>\r\n', 'S'),
(147, 39, '', '', '<p>20</p>\r\n', 'S'),
(148, 39, '', '', '<p>25</p>\r\n', 'S'),
(149, 39, '', '', '<p>30</p>\r\n', 'S'),
(150, 40, '', '', 'Pernyataan kemerdekaan yang terperinci', 'B'),
(151, 40, '', '', 'Pernyataan tentang hak-hak asasi manusia', 'S'),
(152, 40, '', '', 'Deklarasi terbentuknya bangsa dan Negara', 'S'),
(153, 40, '', '', 'Peraturan perundang-undangan tertinggi', 'S'),
(154, 40, '', '', 'Semua jawaban benar', 'S'),
(155, 41, '', '', 'Sebagai pandangan hidup bangsa dan negara Indonesia serta akan menjiwai setiap warga negaranya.', 'B'),
(156, 41, '', '', 'Perwujudan dan pelaksanaan setiap warga negara dalam masyarakat, berbangsa dan bernegara', 'S'),
(157, 41, '', '', 'Menangkal budaya asing yang masuk ke Indonesia, baik secara langsung maupun melalui perantara teknologi', 'S'),
(158, 41, '', '', 'Memperdalam pengetahuan dan pengertian Pancasila sebagai dasar negera, pandangan hidup, dan ideologi bangsa.', 'S'),
(159, 41, '', '', 'Perwujudan dan pelaksanaan setiap warga negara dalam hukum', 'S'),
(160, 42, '', '', 'Sila-sila Pancasila itu harus kita lihat sebagai satu rangkaian kesatuan', 'S'),
(161, 42, '', '', 'Harus kita fahami sebagai totalitas', 'S'),
(162, 42, '', '', 'Susunan dan bentuknya hirearkis pyramidal', 'S'),
(163, 42, '', '', 'Banyak orang yang melanggarnya', 'S'),
(164, 42, '', '', 'Jawaban A, B, C benar', 'B'),
(165, 43, '', '', 'Kehidupan masyarakat penuh dinamika persaingan', 'S'),
(166, 43, '', '', 'Kepentingan orang lain menjadi batasan kepentingan individu', 'B'),
(167, 43, '', '', 'Pancasila sebagai dasar negara mencerminkan nilai kehidupan bangsa dan negara', 'S'),
(168, 43, '', '', 'Manusia tidak pernah lepas dari bantuan manusia lain', 'S'),
(169, 43, '', '', 'Kepentingan bangsa dan negara di atas kepentingan lainnya ', 'S'),
(170, 44, '', '', 'Sebagai pandangan hidup bangsa dan Negara Indonesia serta akan menjiwai setiap warga negaranya', 'B'),
(171, 44, '', '', 'Memperdalam pengetahuan dan pengertian Pancasila sebagai dasar Negara, pandangan hidup bangsa, dan ideology Negara', 'S'),
(172, 44, '', '', 'Menangkal budaya asing yang masuk ke Indonesia, baik secara langsung maupun melalui perantara teknologi', 'S'),
(173, 44, '', '', 'Perwujudan dan pelaksanaan setiap warga Negara dalam masyarakat, berbangsa, dan bernegara', 'S'),
(174, 44, '', '', 'Semua jawaban benar', 'S'),
(175, 45, '', '', 'Kolusi', 'S'),
(176, 45, '', '', 'Reposisi', 'S'),
(177, 45, '', '', 'Reaksi', 'B'),
(178, 45, '', '', 'Persamaan', 'S'),
(179, 45, '', '', 'Polisi', 'S'),
(180, 46, '', '', 'Penggambaan', 'B'),
(181, 46, '', '', 'Pemahaman cerita', 'S'),
(182, 46, '', '', 'Penulisan naskah', 'S'),
(183, 46, '', '', 'Penanaman saham', 'S'),
(184, 46, '', '', 'Pendadaran', 'S'),
(185, 47, '', '', 'Ekosistem', 'S'),
(186, 47, '', '', 'Kebersamaan', 'S'),
(187, 47, '', '', 'Kesamaan Martabat', 'S'),
(188, 47, '', '', 'Mata Rantai', 'S'),
(189, 47, '', '', 'Keseimbangan', 'B'),
(190, 48, '', '', 'Rajin', 'S'),
(191, 48, '', '', 'Pandai', 'S'),
(192, 48, '', '', 'Istimewa', 'S'),
(193, 48, '', '', 'Bebal', 'B'),
(194, 48, '', '', 'Parlente', 'S'),
(195, 49, '', '', 'Insvestasi', 'S'),
(196, 49, '', '', 'Estimasi', 'S'),
(197, 49, '', '', 'Dependen', 'B'),
(198, 49, '', '', 'Relasi', 'S'),
(199, 49, '', '', 'Konstruksi', 'S'),
(200, 50, '', '', 'Pena : Gambar ', 'S'),
(201, 50, '', '', 'Anggar:Tangan ', 'B'),
(202, 50, '', '', 'Pedang : Pisau ', 'S'),
(203, 50, '', '', 'Tongkat : Satpam ', 'S'),
(204, 50, '', '', 'Samurai : Ninja ', 'S'),
(205, 51, '', '', 'Newton : Apel', 'S'),
(206, 51, '', '', 'Binatang : Fabel', 'S'),
(207, 51, '', '', 'Penisitin : Fleming', 'B'),
(208, 51, '', '', 'Beckham : Inggris', 'S'),
(209, 51, '', '', 'Barcelona : Messi', 'S'),
(210, 52, '', '', 'Hadiah : Pengabdian ', 'B'),
(211, 52, '', '', 'Kepribadian : Jati diri', 'S'),
(212, 52, '', '', 'Karyawan : gaji ', 'S'),
(213, 52, '', '', 'Pimpinan : Atur', 'S'),
(214, 52, '', '', 'Hadiah : Gaji ', 'S'),
(215, 53, '', '', 'Telur : Kucing ', 'S'),
(216, 53, '', '', 'Air payau : Air tawar ', 'S'),
(217, 53, '', '', 'Kucing : Ikan ', 'S'),
(218, 53, '', '', 'Insang : Paru-paru ', 'B'),
(219, 53, '', '', 'Kera : Monyet ', 'S'),
(220, 54, '', '', 'Aktor : Penghargaan ', 'B'),
(221, 54, '', '', 'Gaji: Honor ', 'S'),
(222, 54, '', '', 'Upah : Hadiah ', 'S'),
(223, 54, '', '', 'Utang: Piutang ', 'S'),
(224, 54, '', '', 'Buih : Laut ', 'S'),
(225, 55, '', '', 'Ayahnya bekerja nonstop untuk membiayai kehidupan keluarganya. ', 'S'),
(226, 55, '', '', 'Jangan sembarangan minum anti biotika. ', 'B'),
(227, 55, '', '', 'Pemerintah daerah baru saja rapat soal penanganan pasca gempa. ', 'S'),
(228, 55, '', '', 'Pasukan anti Amerika bersiaga penuh di daerah perbatasan. ', 'S'),
(229, 55, '', '', 'Semua Jawaban Salah', 'S'),
(230, 56, '', '', 'Karena hujan terus-menerus, daerah itu banjir lagi. ', 'S'),
(231, 56, '', '', 'Jika kamu belajar, kamu akan sukses. ', 'S'),
(232, 56, '', '', 'Kepercayaan dari seseorang jangan kamu sia-siakan. ', 'B'),
(233, 56, '', '', 'Kita harus berhati-hati agar kita selamat di sana. ', 'S'),
(234, 56, '', '', 'Semua Jawaban Salah', 'S'),
(235, 57, '', '', 'berhari-hari, mondar-mandir, bercakap-cakap ', 'S'),
(236, 57, '', '', 'bercakap-cakap, tanya-menanya, pandang-memandang ', 'B'),
(237, 57, '', '', 'berhari-hari, bercakap-cakap, tertawa-tawa ', 'S'),
(238, 57, '', '', 'tertawa-tawa, tanya-menanya, lihat-melihat ', 'S'),
(239, 57, '', '', 'tanya-menanya, lihat-melihat, berhari-hari ', 'S'),
(240, 58, '', '', 'Rumah itu tidak begitu besar meskipun begitu nyaman digunakan untuk tempat beristirahat. ', 'S'),
(241, 58, '', '', 'Tanaman juga seperti manusia yang memerlukan makan dan minum untuk kelangsungan hidupnya. ', 'S'),
(242, 58, '', '', 'Tanaman itu bukan hanya sebagai hiasan, melainkan juga sebagai obat berbagai penyakit. ', 'B'),
(243, 58, '', '', 'Tidak boleh menyesali hidupmu sedemikian rupa, apalagi sampai menyesali orang tuamu sendiri. ', 'S'),
(244, 58, '', '', 'Sekali, dua kali bahkan sudah sering saya menasihatinya, namun dia tidak menunjukkan perubahan. ', 'S'),
(245, 59, '', '', 'Di Indonesia banyak lahan pertanian yang subur. ', 'S'),
(246, 59, '', '', 'Karena hujan terus-menerus, daerah itu banjir lagi. ', 'S'),
(247, 59, '', '', 'Jika kamu belajar, kamu akan sukses. ', 'B'),
(248, 59, '', '', 'Kepercayaan dari seseorang jangan kamu sia-siakan. ', 'S'),
(249, 59, '', '', 'Kita harus berhati-hati agar kita selamat di sana. ', 'S'),
(250, 60, '', '', 'Perang ideologi', 'S'),
(251, 60, '', '', 'Perlombaan persenjataan', 'S'),
(252, 60, '', '', 'Human right ', 'B'),
(253, 60, '', '', 'Ofensif politik', 'S'),
(254, 60, '', '', 'Semua Jawaban Salah', 'S'),
(255, 61, '', '', 'Jika orang bekerja keras, orang tersebut akan mendapat hasil maksimal. ', 'S'),
(256, 61, '', '', 'Petinju itu berlatih setiap hari sehingga ia menjadi juara. ', 'B'),
(257, 61, '', '', 'Orang itu berusaha keras maka is akan berhasil. ', 'S'),
(258, 61, '', '', 'Polisi menegakkan hukum supaya keadilan tercapai. ', 'S'),
(259, 61, '', '', 'Semua Jawaban Salah', 'S'),
(260, 62, '', '', 'saya tidak perlu repot-repot mencobanya ', 'S'),
(261, 62, '', '', 'saya khawatir jika mencobanya dan gagal ', 'S'),
(262, 62, '', '', 'saya berani mencobanya setelah mempertimbangkan risikonya ', 'B'),
(263, 62, '', '', 'saya minta pendapat isteri dan orang tua ', 'S'),
(264, 62, '', '', 'yang penting coba dulu', 'S'),
(265, 63, '', '', 'menerimanya dengan terpaksa ', 'S'),
(266, 63, '', '', 'menerimanya, meski tentu saja dengan sedikit kekecewaan ', 'S'),
(267, 63, '', '', 'menerimanya dengan lapang dada ', 'B'),
(268, 63, '', '', 'membenci diri saya sendiri ', 'S'),
(269, 63, '', '', 'meretapi diri sendiri ', 'S'),
(270, 64, '', '', 'Merasa sangat kecewa.', 'S'),
(271, 64, '', '', 'Mencoba mencari alternatif usulan lain yang lebih tepat untuk diajukan lagi.', 'B'),
(272, 64, '', '', 'Merasa kecewa tetapi berusaha melupakan penolakan tersebut.', 'S'),
(273, 64, '', '', 'Bersikeras memberikan alasan dan pembenaran atas usulan tersebut sampai dapat meyakinkan atasan saya.', 'S'),
(274, 64, '', '', 'Membiarkan saja.', 'S'),
(275, 65, '', '', 'Saya percaya dokter RS mampu menangani dengan baik', 'S'),
(276, 65, '', '', 'Saya akan menjenguknya ketika ada waktu yang benar-benar longgar', 'B'),
(277, 65, '', '', 'Saya akan menjenguknya', 'S'),
(278, 65, '', '', 'Saya menanyakan apakah kondisinya memang parah', 'S'),
(279, 65, '', '', 'Saya berharap semoga lekas sembuh', 'S'),
(280, 66, '', '', 'Pulang dengan diam diam, tanpa sepengetahuan pimpinan.', 'S'),
(281, 66, '', '', 'Berpura pura sakit agar dapat diizinkan untuk segera pulang.', 'S'),
(282, 66, '', '', 'Menghubungi anak saya menjelaskan agar naik taksi saja.', 'S'),
(283, 66, '', '', 'Bekerja lembur, karena yakin anak saya pasti memaklumi.', 'S'),
(284, 66, '', '', 'Meminta izin pimpinan mengantar anak saya kemudian kembali ke kantoruntukbekerja lembur.', 'B'),
(285, 67, '', '', 'Selamat atas kemenanganmu tadi malam ', 'S'),
(286, 67, '', '', 'Terima kasih atas hadiahnya ', 'S'),
(287, 67, '', '', 'Hati-hati di jalan ', 'S'),
(288, 67, '', '', 'Kapan kau mau datang', 'B'),
(289, 67, '', '', 'Aku jadi ke rumahmu nanti sore ', 'S'),
(290, 68, '', '', 'Ikan nila atau Oreochromis niloticus adalah komoditi utama perikanan di Jawa Barat ', 'B'),
(291, 68, '', '', 'Presiden akan berkunjung ke Negara Jepang pada akhir bulan ini', 'S'),
(292, 68, '', '', 'Sebagai negara mandiri kita tidak boleh tunduk kepada negara asing', 'S'),
(293, 68, '', '', 'Budaya kita banyak yang diakui oleh negara tetangga ', 'S'),
(294, 68, '', '', 'Persatuan Bangsa-Bangsa adalah organ isasi antarnegara yang sangat besar ', 'S'),
(295, 69, '', '', 'raden mas Onggosuwiryo', 'S'),
(296, 69, '', '', 'Arifin budiarto', 'S'),
(297, 69, '', '', 'Raden Ayu Mahayu Kusuma', 'B'),
(298, 69, '', '', 'haji imam sobirin', 'S'),
(299, 69, '', '', 'tika Widyasari', 'S'),
(300, 70, '', '', 'Very small  plants and animals', 'B'),
(301, 70, '', '', 'Big animals', 'S'),
(302, 70, '', '', 'The oceans mammals', 'S'),
(303, 70, '', '', 'The smallest animal', 'S'),
(304, 70, '', '', 'Big plants', 'S'),
(305, 71, '', '', 'The life of whales', 'B'),
(306, 71, '', '', 'The size of whales', 'B'),
(307, 71, '', '', 'The movement of whales', 'B'),
(308, 71, '', '', 'Three kinds of whales', 'B'),
(309, 71, '', '', 'The migration of whales', 'B'),
(310, 72, '', '', 'Saved', 'S'),
(311, 72, '', '', 'Having saved', 'B'),
(312, 72, '', '', 'Has saved', 'S'),
(313, 72, '', '', 'He have saved', 'S'),
(314, 72, '', '', 'Has saving', 'S'),
(315, 73, '', '', '10', 'S'),
(316, 73, '', '', '11', 'B'),
(317, 73, '', '', '12', 'S'),
(318, 73, '', '', '13', 'S'),
(319, 73, '', '', '14', 'S'),
(320, 74, '', '', '25    9', 'S'),
(321, 74, '', '', '23    6', 'S'),
(322, 74, '', '', '92   5', 'S'),
(323, 74, '', '', '33   6', 'S'),
(324, 74, '', '', '25   3', 'B'),
(325, 75, '', '', '26  8', 'B'),
(326, 75, '', '', '8   26', 'S'),
(327, 75, '', '', '7   27', 'S'),
(328, 75, '', '', '27   7', 'S'),
(329, 75, '', '', '6   25', 'S'),
(330, 76, '', '', '29  35', 'S'),
(331, 76, '', '', '30  36', 'B'),
(332, 76, '', '', '36  30', 'S'),
(333, 76, '', '', '31  37', 'S'),
(334, 76, '', '', '37  31', 'S'),
(335, 77, '', '', '30  20', 'B'),
(336, 77, '', '', '20  30', 'S'),
(337, 77, '', '', '21  31', 'S'),
(338, 77, '', '', '31  21', 'S'),
(339, 77, '', '', '20  31', 'S'),
(340, 78, '', '', '36', 'S'),
(341, 78, '', '', '40', 'B'),
(342, 78, '', '', '45', 'S'),
(343, 78, '', '', '50', 'S'),
(344, 78, '', '', '54', 'S'),
(345, 79, '', '', '48', 'S'),
(346, 79, '', '', '40', 'B'),
(347, 79, '', '', '54', 'S'),
(348, 79, '', '', '50', 'S'),
(349, 79, '', '', '56', 'S'),
(350, 80, '', '', '408576', 'B'),
(351, 80, '', '', '412.327', 'S'),
(352, 80, '', '', '524176', 'S'),
(353, 80, '', '', '40,8576', 'S'),
(354, 80, '', '', '52,4176', 'S'),
(355, 81, '', '', '15,00', 'S'),
(356, 81, '', '', '14,30', 'B'),
(357, 81, '', '', '14,00', 'S'),
(358, 81, '', '', '15,30', 'S'),
(359, 81, '', '', '13,00', 'S'),
(360, 82, '', '', '1/6', 'S'),
(361, 82, '', '', '1/4', 'S'),
(362, 82, '', '', '1/8', 'S'),
(363, 82, '', '', '2', 'S'),
(364, 82, '', '', '1/15', 'B'),
(695, 1, '', '', 'FLAMBOYAN = ', ''),
(696, 1, '', '', 'VETER =', ''),
(697, 1, '', '', 'YAYI = ', ''),
(698, 1, '', '', 'Nadir ><', ''),
(699, 1, '', '', 'Artika >< ', ''),
(700, 1, '', '', 'EFISIEN = …', ''),
(701, 1, '', '', 'PARTIKELLIR = …', ''),
(702, 1, '', '', 'SINDIKASI = ....   ', ''),
(703, 1, '', '', 'KONSIDERAN = …', ''),
(704, 1, '', '', 'ASKETIK = …', ''),
(705, 1, '', '', 'PROGRESIF >< …  ', ''),
(706, 1, '', '', 'ANTIPATI >< …', ''),
(707, 1, '', '', 'ORTODOKS >< ....   ', ''),
(708, 1, '', '', 'PARADOKSAL >< ....   ', ''),
(709, 1, '', '', 'MAKAR >< ....   ', ''),
(710, 163, '', '', 'Jenis tanah', 'S'),
(711, 163, '', '', 'Hidup Sederhana', 'S'),
(712, 163, '', '', 'Kebersamaan', 'S'),
(713, 163, '', '', 'Kedamaian', 'S'),
(714, 163, '', '', 'Serba mewah', 'B'),
(715, 164, '', '', 'Hewan', 'S'),
(716, 164, '', '', 'Artis', 'S'),
(717, 164, '', '', 'Urutan Abjad', 'S'),
(718, 164, '', '', 'Tali Sepatu', 'B'),
(719, 164, '', '', 'Angka', 'S'),
(720, 165, '', '', 'Kakak', 'S'),
(721, 165, '', '', 'Ratu', 'S'),
(722, 165, '', '', 'Adinda', 'B'),
(723, 165, '', '', 'Sepupu', 'S'),
(724, 165, '', '', 'Raja', 'S'),
(725, 166, '', '', 'Zenit', 'B'),
(726, 166, '', '', 'Ajal', 'S'),
(727, 166, '', '', 'Penentu', 'S'),
(728, 166, '', '', 'Darah', 'S'),
(729, 166, '', '', 'Vena', 'S'),
(730, 167, '', '', 'Kutub', 'S'),
(731, 167, '', '', 'Panas', 'S'),
(732, 167, '', '', 'Asia', 'B'),
(733, 167, '', '', 'Pulau', 'S'),
(734, 167, '', '', 'Antartika', 'S'),
(735, 168, '', '', 'Berhasil guna', 'S'),
(736, 168, '', '', 'Tepat guna', 'S'),
(737, 168, '', '', 'Berhemat', 'B'),
(738, 168, '', '', 'Penyederhanaan', 'S'),
(739, 168, '', '', 'Cepat ', 'S'),
(740, 169, '', '', 'Pedagang', 'S'),
(741, 169, '', '', 'Tuan Tanah', 'B'),
(742, 169, '', '', 'Penjajah', 'S'),
(743, 169, '', '', 'Penduduk Asli', 'S'),
(744, 169, '', '', 'Hiasan ', 'S'),
(745, 170, '', '', 'Kejahatan  ', 'S'),
(746, 170, '', '', 'Persekutuan  ', 'B'),
(747, 170, '', '', 'Dugaan   ', 'S'),
(748, 170, '', '', 'Persaingan ', 'S'),
(749, 170, '', '', 'Persekongkolan ', 'S'),
(750, 171, '', '', 'Pengantar ', 'S'),
(751, 171, '', '', 'Pendahuluan', 'S'),
(752, 171, '', '', 'Pertimbangan ', 'B'),
(753, 171, '', '', 'keputusan    ', 'S'),
(754, 171, '', '', 'Perbandingan ', 'S'),
(755, 172, '', '', 'Duniawiah', 'S'),
(756, 172, '', '', 'Rasional', 'S'),
(757, 172, '', '', 'Ritual', 'S'),
(758, 172, '', '', 'Keimanan', 'B'),
(759, 172, '', '', 'Klenik ', 'S'),
(760, 173, '', '', 'Stagnan', 'B'),
(761, 173, '', '', 'Berarti', 'S'),
(762, 173, '', '', 'Aktif', 'S'),
(763, 173, '', '', 'Pasif', 'S'),
(764, 173, '', '', 'Kemunduran ', 'S'),
(765, 174, '', '', 'Simpati', 'S'),
(766, 174, '', '', 'Acuh', 'S'),
(767, 174, '', '', 'Akrab ', 'S'),
(768, 174, '', '', 'Apatis', 'S'),
(769, 174, '', '', 'Peduli', 'B'),
(770, 175, '', '', 'Kuno ', 'S'),
(771, 175, '', '', 'Aliran  ', 'S'),
(772, 175, '', '', 'Paradoks ', 'S'),
(773, 175, '', '', 'Modern', 'B'),
(774, 175, '', '', 'Asli  ', 'S'),
(775, 176, '', '', 'Berseberangan ', 'S'),
(776, 176, '', '', 'Berlawanan  ', 'S'),
(777, 176, '', '', 'Berseberangan ', 'B'),
(778, 176, '', '', 'Bertentangan ', 'S'),
(779, 176, '', '', 'Sejalan', 'S'),
(780, 177, '', '', 'Pengacau  ', 'S'),
(781, 177, '', '', 'Pro', 'B'),
(782, 177, '', '', 'Teroris', 'S'),
(783, 177, '', '', 'Kontra', 'S'),
(784, 177, '', '', 'Baik ', 'S'),
(785, 178, '', '', 'Gempa Oceanik ', 'S'),
(786, 178, '', '', 'Gempa Vulkanik ', 'S'),
(787, 178, '', '', 'Gempa Mekanik', 'S'),
(788, 178, '', '', 'Gempa Tektonik ', 'B'),
(789, 178, '', '', 'Gempa Seismik ', 'S'),
(790, 179, '', '', 'UNESCO', 'B'),
(791, 179, '', '', 'ILO', 'S'),
(792, 179, '', '', 'WIPO', 'S'),
(793, 179, '', '', 'WHO', 'S'),
(794, 179, '', '', 'UNICEF', 'S'),
(795, 180, '', '', 'Sudan', 'S'),
(796, 180, '', '', 'Mali', 'S'),
(797, 180, '', '', 'Mesir', 'B'),
(798, 180, '', '', 'Angola', 'S'),
(799, 180, '', '', 'Nigeria', 'S'),
(800, 181, '', '', '3 orang', 'S'),
(801, 181, '', '', '4 orang', 'S'),
(802, 181, '', '', '5 orang', 'B'),
(803, 181, '', '', '6 orang', 'S'),
(804, 181, '', '', '7 orang', 'S'),
(805, 182, '', '', 'Salam Pembuka', 'S'),
(806, 182, '', '', 'Salam Penutup', 'S'),
(807, 182, '', '', 'Nomor Surat', 'B'),
(808, 182, '', '', 'Isi Surat', 'S'),
(809, 182, '', '', 'Tanda Tangan dan Nama Terang', 'S'),
(810, 183, '', '', 'Tanya', 'S'),
(811, 183, '', '', 'Berita', 'B'),
(812, 183, '', '', 'Majemuk Campuran', 'S'),
(813, 183, '', '', 'Saran', 'S'),
(814, 183, '', '', 'Perintah', 'S'),
(815, 184, '', '', 'Berupaya', 'S'),
(816, 184, '', '', 'Berpikir', 'B'),
(817, 184, '', '', 'Bekerja', 'S'),
(818, 184, '', '', 'Berkarya', 'S'),
(819, 184, '', '', 'Berjasa', 'S'),
(820, 185, '', '', 'Sri Hartati, SE', 'S'),
(821, 185, '', '', 'Tuti Herawati, S.H', 'S'),
(822, 185, '', '', 'Tyas Agung Febriwati, M.Ed', 'S'),
(823, 185, '', '', 'DR. Rekso Pranoto', 'B'),
(824, 185, '', '', 'I.R. Titik Widowati', 'S'),
(825, 186, '', '', 'Peningkatan Mutu Sumber daya manusia Di Indonesia ', 'S'),
(826, 186, '', '', 'Pengembangan Tes Prestasi Belajar Tingkat Sekolah Dasar ', 'B'),
(827, 186, '', '', 'Studi Tentang Kompetensi Guru Bidang Studi Matematika Tingkat SMA ', 'S'),
(828, 186, '', '', 'Kepemimpinan Nasional Yang Ideal Dalam Masa Reformasi ', 'S'),
(829, 186, '', '', 'Pengolahan Limbah Industri Di Negara Maju ', 'S'),
(830, 187, '', '', 'Dia menyesalkan perbuatan kami dan meminta agar kami berjanji tidak akan mengulangi kesalahan yang sama, yang merugikan nama baik keluarga. ', 'B'),
(831, 187, '', '', 'Dia menyesalkan perbuatan kami sehingga meminta kami tidak menguangi kesalahan nama baik keluarga.', 'S'),
(832, 187, '', '', 'Kesalahan yang merugikan nama baik keluarga disesalkannya sehingga dia meminta kami untuk tidak mengulangi kesalahan yang sama. ', 'S'),
(833, 187, '', '', 'Dia menyesalkan dan meminta kami berjanji tidak akan mengulangi kesalahan yang merugikan nama baik keluarga.', 'S'),
(834, 187, '', '', 'Dia meminta agar perbuatan kami tidak terulangi karena kesalahannya merugikan nama baik keluarga. ', 'S'),
(835, 188, '', '', 'is declining ', 'S'),
(836, 188, '', '', 'declined', 'S'),
(837, 188, '', '', 'has been declining', 'B'),
(838, 188, '', '', 'was declining', 'S'),
(839, 188, '', '', 'had declined', 'S'),
(840, 189, '', '', 'The turtles eat jellyfish', 'B'),
(841, 189, '', '', 'The turtles lay their eggs in the seabed', 'B'),
(842, 189, '', '', 'The sharks possess a powerful sting.', 'B'),
(843, 189, '', '', 'The Californian grey whales immigrate to the Arctic in winter. ', 'B'),
(844, 189, '', '', 'The humpback whale is the largest animal in the sea', 'B'),
(845, 190, '', '', 'Am', 'S'),
(846, 190, '', '', 'Was', 'B'),
(847, 190, '', '', 'Were', 'S'),
(848, 190, '', '', 'Be', 'S'),
(849, 190, '', '', 'Are', 'S'),
(850, 191, '', '', 'Can borrow', 'S'),
(851, 191, '', '', 'May Borrow', 'S'),
(852, 191, '', '', 'Ought to Borrow', 'S'),
(853, 191, '', '', 'Will Borrow', 'S'),
(854, 191, '', '', 'Might borrow', 'B'),
(855, 192, '', '', 'Has been playing', 'B'),
(856, 192, '', '', 'Plays', 'S'),
(857, 192, '', '', 'Is playing', 'S'),
(858, 192, '', '', 'Played', 'S'),
(859, 192, '', '', 'Plays', 'S'),
(860, 193, '', '', 'Was needing', 'S'),
(861, 193, '', '', 'Has needing', 'S'),
(862, 193, '', '', 'Am needing', 'S'),
(863, 193, '', '', 'Have needing', 'S'),
(864, 193, '', '', 'Had needing', 'B'),
(865, 194, '', '', 'He kept his job .', 'B'),
(866, 194, '', '', 'He would like to keep the job', 'B'),
(867, 194, '', '', 'He had to quit his job', 'B'),
(868, 194, '', '', 'He lost his job', 'B'),
(869, 194, '', '', 'He losing from job', 'B'),
(870, 195, '', '', 'Selang', 'S'),
(871, 195, '', '', 'Antara', 'S'),
(872, 195, '', '', 'Lewat', 'S'),
(873, 195, '', '', 'Sela ', 'S'),
(874, 195, '', '', 'Beda ', 'B'),
(875, 196, '', '', 'Sahabat', 'S'),
(876, 196, '', '', 'Saudara', 'B'),
(877, 196, '', '', 'Kawan', 'S'),
(878, 196, '', '', 'Teman', 'S'),
(879, 196, '', '', 'Handai', 'S'),
(880, 197, '', '', 'Pada Pokoknya', 'B'),
(881, 197, '', '', 'Memang Demikian', 'S'),
(882, 197, '', '', 'Sahaja', 'S'),
(883, 197, '', '', 'Sesungguhnya', 'S'),
(884, 197, '', '', 'Sebenarnya', 'S'),
(885, 198, '', '', 'Iri ', 'S'),
(886, 198, '', '', 'Cemburu', 'S'),
(887, 198, '', '', 'Dendam', 'B'),
(888, 198, '', '', 'Dengki', 'S'),
(889, 198, '', '', 'Sirik', 'S'),
(890, 199, '', '', 'Ken Arok', 'S'),
(891, 199, '', '', 'Anusapati', 'S'),
(892, 199, '', '', 'Tohjaya', 'S'),
(893, 199, '', '', 'Kertanegara', 'S'),
(894, 199, '', '', 'Kertajaya', 'B'),
(895, 200, '', '', '4,2', 'B'),
(896, 200, '', '', '4,8', 'S'),
(897, 200, '', '', '5,6', 'S'),
(898, 200, '', '', '6', 'S'),
(899, 200, '', '', '6,4', 'S'),
(900, 201, '', '', 'Kecamatan J', 'B'),
(901, 201, '', '', 'Kecamatan K', 'S'),
(902, 201, '', '', 'Kecamatan L', 'S'),
(903, 201, '', '', 'Kecamatan M', 'S'),
(904, 201, '', '', 'Kecamatan N', 'S'),
(905, 202, '', '', 'Kecamatan J dan K', 'S'),
(906, 202, '', '', 'Kecamatan L dan M', 'B'),
(907, 202, '', '', 'Kecamatan M dan J', 'S'),
(908, 202, '', '', 'Kecamatan N dan K', 'S'),
(909, 202, '', '', 'Kecamatan K dan L', 'S'),
(910, 203, '', '', 'J, K, N, L, M', 'B'),
(911, 203, '', '', 'K, J, L, N, M', 'S'),
(912, 203, '', '', 'K, M, L, J, N', 'S'),
(913, 203, '', '', 'L, K, J, N, M', 'S'),
(914, 203, '', '', 'M, K, N, J, L', 'S'),
(915, 204, '', '', '1, 2, dan 4', 'S'),
(916, 204, '', '', '2, 3, dan 6', 'S'),
(917, 204, '', '', '1, 2 dan 3', 'B'),
(918, 204, '', '', '2, 3 dan 4', 'S'),
(919, 204, '', '', '1, 2, dan 6', 'S'),
(920, 205, '', '', 'Bu Heni', 'S'),
(921, 205, '', '', 'Pak Mara', 'S'),
(922, 205, '', '', 'Pak Tasman', 'S'),
(923, 205, '', '', 'Bu Rati', 'S'),
(924, 205, '', '', 'Pak Dedi', 'B'),
(925, 206, '', '', '2', 'S'),
(926, 206, '', '', '6', 'S'),
(927, 206, '', '', '1', 'S'),
(928, 206, '', '', '3', 'D'),
(929, 206, '', '', '4', 'S'),
(930, 207, '', '', 'Pil anti malaria sangat dibutuhkan di daerah Irian Jaya. ', 'S'),
(931, 207, '', '', 'Ayahnya bekerja nonstop untuk membiayai kehidupan keluarganya. ', 'B'),
(932, 207, '', '', 'Jangan sembarangan minum anti biotika. ', 'S'),
(933, 207, '', '', 'Pemerintah daerah baru saja rapat soal penanganan pasca gempa. ', 'S'),
(934, 207, '', '', 'Pasukan anti Amerika bersiaga penuh di daerah perbatasan. ', 'S'),
(935, 208, '', '', 'Semua siswa-siswa kelas tiga SMU harus mengikuti ujian nasional. ', 'S'),
(936, 208, '', '', 'Banyak anak-anak zaman sekarang kurang menghargai orang yang lebih tua. ', 'S'),
(937, 208, '', '', 'Guru-guru merupakan ujung tombak dalam mencerdaskan kehidupan bangsa. ', 'B'),
(938, 208, '', '', 'Banyak soal-soal bahasa Indonesia yang tidak dapat dijawab siswa. ', 'S'),
(939, 208, '', '', 'Para buruh-buruh pabrik melakukan demonstrasi kenaikan gaji. ', 'S'),
(940, 209, '', '', 'Bekerja keras akan mendatangkan hasil maksimal. ', 'S'),
(941, 209, '', '', 'Jika orang bekerja keras, orang tersebut akan mendapat hasil maksimal. ', 'B'),
(942, 209, '', '', 'Petinju itu berlatih setiap hari sehingga ia menjadi juara. ', 'S'),
(943, 209, '', '', 'Orang itu berusaha keras maka is akan berhasil. ', 'S'),
(944, 209, '', '', 'Polisi menegakkan hukum supaya keadilan tercapai. ', 'S'),
(945, 210, '', '', 'Belakang parang pun jika diasah akan tajam. ', 'S'),
(946, 210, '', '', 'Bunga gugur putik pun gugur. ', 'S'),
(947, 210, '', '', 'Seperti api dalam sekam. ', 'S'),
(948, 210, '', '', 'Habis manis sepah dibuang. ', 'B'),
(949, 210, '', '', 'Pagar makan tanaman. ', 'S'),
(950, 211, '', '', 'Akhirnya, pemuda itu angkat bicara di tengah-tengah para demonstran. ', 'S'),
(951, 211, '', '', 'Ayah memberikan acungan jempol atas keberhasilan saya diterima di perguruan tinggi negeri yang berkualitas. ', 'B'),
(952, 211, '', '', 'Orang itu angkat muka sewaktu namanya dipanggil maju ke tengah sidang. ', 'S'),
(953, 211, '', '', 'Sudah pada tempatnya Anda unjuk gigi di hadapan pemuda yang kuceritakan itu. ', 'S'),
(954, 211, '', '', 'Para perusuh itu angkat tangan sewaktu dikepung para petugas keamanan yang berpatroli. ', 'S'),
(955, 212, '', '', 'Gubuk sederhana inilah hasil karya kami selama bertahun-tahun. ', 'B'),
(956, 212, '', '', 'Teman akrab ada kalanya merupakan musuh sejati. ', 'S'),
(957, 212, '', '', 'Hujan memandikan tanaman di halaman rumah. ', 'S'),
(958, 212, '', '', 'Kecantikannyalah justru yang mencelakakannya. ', 'S'),
(959, 212, '', '', 'Saya telah mencatat kejadian itu dengan tangan saya sendiri. ', 'S'),
(960, 213, '', '', 'Peringatan ke-57 HUT RI jatuh pada hari Sabtu. Karena kelelahan bekerja, setiba di rumah dia jatuh di halaman. ', 'B'),
(961, 213, '', '', 'Selamat pagi, Pak! sapa sekretaris itu kepada atasannya. \"Berapa pak dia membeli rokok?\" tanya pedagang itu kepada orang suruhannya. ', 'S'),
(962, 213, '', '', 'Pertandingan sepak bola antara Argentina dan Swedia berakhir seri (1-1). Cerita yang sedang dibaca Tomy adalah cerita Jaka Tingkir seri kedua. ', 'S'),
(963, 213, '', '', 'Sepeninggal ayah, ibu sakit-sakitan dan badannya terlihat mengurus. Istri yang pandai mengurus rumah tangga akan membahagiakan suaminya. ', 'S'),
(964, 213, '', '', 'Petugas yang telah ditunjuk itu mengukur jalan ke kiri dan ke kanan sepanjang enam meter. Ibu mengukur kelapa untuk membuat kolak makanan kesenangan ayah.', 'S'),
(965, 214, '', '', 'Ridwan menjadi pemimpin karena jujur dan dapat dipercaya', 'S'),
(966, 214, '', '', 'Ridwan menjadi orang Kepercayaan bawahannya', 'S'),
(967, 214, '', '', 'Ridwan adalah orang jujur dan dipercaya atasannya', 'S'),
(968, 214, '', '', 'Ridwan Harus Jujur dan dapat dipercaya', 'B'),
(969, 214, '', '', 'Ridwan adalah orang yang jujur sehingga dipercaya bawahannya', 'S'),
(970, 215, '', '', 'Jika guru Matematika, menambah jam pelajaran, nilai siswa banyak yang meningkat', 'S'),
(971, 215, '', '', 'Guru Matematika menambah jam pelajaran dihari Rabu jika siswa tidak mengikuti kegiatan ekstrakurikuler', 'S'),
(972, 215, '', '', 'Jika guru Matematika menambah jam pelajaran di hari Rabu, ekstrakurikuler ditiadakan', 'S'),
(973, 215, '', '', 'Siswa dapat mengikuti kegiatan ekstraurikuler jika Guru Matematika menambah jam pelajaran di hari Rabu', 'B'),
(974, 215, '', '', 'Nilai siswa banyak yang meningkat jika tidak mengikuti kegiatan ekstrakurikuler', 'S'),
(975, 216, '', '', 'Nilai tukar dolar AS melemah dibandingkan dengan rupiah karena Bank Sentral Amerika tidak menaikan tingkat suku bunga', 'S'),
(976, 216, '', '', 'Nilai tukar dolar AS menguat dibandingkan dengan rupiah tetapi Bank Sentral Amerika tidak menaikan tingkat suku bunga', 'S'),
(977, 216, '', '', 'Nilai tukar dolar AS melemah dibandingkan dengan rupiah walaupun Bank Sentral Amerika tidak menaikan tingkat suku bunga', 'S'),
(978, 216, '', '', 'Nilai Tukar dolar AS melemah dibandingkan dengan rupiah atau Bank Sentral Amerika tidak menaikan tingkat suku bunga', 'B'),
(979, 216, '', '', 'Nilai tukar dolar AS melemah dibandingkan dengan rupiah dan Bank Sentral Amerika tidak menaikan tingkat suku bunga', 'S'),
(980, 217, '', '', 'Ada dokter yang berjas putih dan bekerja di puskesmas ', 'S'),
(981, 217, '', '', 'Tidak ada orang yang bekerja di puskesmas yang merupakan dokter berjas putih', 'B'),
(982, 217, '', '', 'Ada dokter yang tidak berjas putih dan bekerja di puskesmas', 'S'),
(983, 217, '', '', 'Ada dokter yang tidak bekerja di puskesmas dan memakai jas putih', 'S'),
(984, 217, '', '', 'Tidak ada dokter yang bekerja di puskesmas yang tidak berjas putih', 'S'),
(985, 218, '', '', 'Beberapa orang yang mengeluh bukan orang sukses ', 'S'),
(986, 218, '', '', 'Beberapa orang yang takut mencoba bukanlah orang sukses', 'S'),
(987, 218, '', '', 'Semua orang takut mencoba selalu mengeluh ', 'S'),
(988, 218, '', '', 'Semua orang sukses tidak pernah mengeluh dan berani mencoba', 'B'),
(989, 218, '', '', 'Beberapa orang yang sukses takut mencoba', 'S'),
(990, 219, '', '', 'Semut : Gula', 'S'),
(991, 219, '', '', 'Harimau : Hutan', 'S'),
(992, 219, '', '', 'Tentara : Perang', 'S'),
(993, 219, '', '', 'Kerbau : Kandang', 'B'),
(994, 219, '', '', 'Burung : Langit', 'S'),
(995, 220, '', '', 'Nada : Gitar', 'S'),
(996, 220, '', '', 'Botol : Plastik', 'S'),
(997, 220, '', '', 'Lokomotif : Kereta', 'B'),
(998, 220, '', '', 'Kabel : Listrik ', 'S'),
(999, 220, '', '', 'Lambung : Usus', 'S'),
(1000, 221, '', '', 'Buku : Pendinginan', 'B'),
(1001, 221, '', '', 'Badai : Topan', 'S'),
(1002, 221, '', '', 'Terang : Gelap', 'S'),
(1003, 221, '', '', 'Samar : Terang', 'S'),
(1004, 221, '', '', 'Lampu : Penerangan', 'S'),
(1005, 222, '', '', 'Bahasa Indonesia', 'S'),
(1006, 222, '', '', 'Bahasa Inggris ', 'B'),
(1007, 222, '', '', 'Matematika', 'S'),
(1008, 222, '', '', 'Kesenian', 'S'),
(1009, 222, '', '', 'IPS', 'S'),
(1010, 223, '', '', 'Ani', 'S'),
(1011, 223, '', '', 'Ari', 'S'),
(1012, 223, '', '', 'Ati', 'S'),
(1013, 223, '', '', 'Ita', 'S'),
(1014, 223, '', '', 'Ami', 'B'),
(1015, 224, '', '', 'Kiki, Deni, Cepi, Ani, Seno', 'S'),
(1016, 224, '', '', 'Kiki, Deni, Ani, Cepi, Seno', 'B'),
(1017, 224, '', '', 'Kiki, Ani, Cepi, Deni, Seno', 'S'),
(1018, 224, '', '', 'Ani, Kiki, Deni, Cepi, Seno', 'S'),
(1019, 224, '', '', 'Ani, Seno, Kiki, Deni, Cepi', 'S'),
(1020, 225, '', '', '62', 'S'),
(1021, 225, '', '', '74', 'S'),
(1022, 225, '', '', '86', 'S'),
(1023, 225, '', '', '98', 'B'),
(1024, 225, '', '', '100', 'S'),
(1025, 226, '', '', '31', 'S'),
(1026, 226, '', '', '34', 'B'),
(1027, 226, '', '', '38', 'S'),
(1028, 226, '', '', '61', 'S'),
(1029, 226, '', '', '64', 'S'),
(1030, 227, '', '', '9', 'S'),
(1031, 227, '', '', '12', 'S'),
(1032, 227, '', '', '18', 'B'),
(1033, 227, '', '', '39', 'S'),
(1034, 227, '', '', '108', 'S'),
(1035, 228, '', '', '12', 'S'),
(1036, 228, '', '', '16', 'B'),
(1037, 228, '', '', '22', 'S'),
(1038, 228, '', '', '24', 'S'),
(1039, 228, '', '', '48', 'S'),
(1040, 229, '', '', '<p>The reason why women are better parents than men&nbsp;</p>\r\n', 'B'),
(1041, 229, '', '', '<p>The importance of men and women in child rearing.</p>\r\n', 'S'),
(1042, 229, '', '', '<p>The reluctance of men to play the role of parents&nbsp;</p>\r\n', 'S'),
(1043, 229, '', '', '<p>The superior role of women in a family&nbsp;</p>\r\n', 'S'),
(1044, 229, '', '', '<p>The different attitudes of men and women as parents&nbsp;</p>\r\n', 'S'),
(1045, 230, '', '', '<p>Experienced in raising children&nbsp;</p>\r\n', 'B'),
(1046, 230, '', '', '<p>Not aggressive at all&nbsp;</p>\r\n', 'S'),
(1047, 230, '', '', '<p>Good communicators&nbsp;</p>\r\n', 'S'),
(1048, 230, '', '', '<p>Superior human beings&nbsp;</p>\r\n', 'S'),
(1049, 230, '', '', '<p>Capable of solving problems&nbsp;</p>\r\n', 'S'),
(1050, 231, '', '', '<p>Have known the role of career since childhood</p>\r\n', 'B'),
(1051, 231, '', '', '<p>Have brothers and sisters with whom they play&nbsp;</p>\r\n', 'S'),
(1052, 231, '', '', '<p>Had to learn about nurturing when they were children&nbsp;</p>\r\n', 'S'),
(1053, 231, '', '', '<p>Are not to learn about competitive roles&nbsp;</p>\r\n', 'S'),
(1054, 231, '', '', '<p>Have never dramed of adventures like boys&nbsp;</p>\r\n', 'S'),
(1055, 232, '', '', '<p>Irresponsible</p>\r\n', 'B'),
(1056, 232, '', '', '<p>Aggressive</p>\r\n', 'B'),
(1057, 232, '', '', '<p>Adventurous</p>\r\n', 'S'),
(1058, 232, '', '', '<p>Impatient</p>\r\n', 'S'),
(1059, 232, '', '', '<p>Competitive&nbsp;</p>\r\n', 'S'),
(1060, 233, '', '', 'Setiap orang boleh berpindah dan berganti agama setiap saat bila dikehendaki.', 'S'),
(1061, 234, '', '', 'Bhineka Tunggal Ika', 'S'),
(1062, 235, '', '', 'mengembangkan sikap saling mencintai dengan penuh kesadaran', 'S'),
(1063, 236, '', '', 'Penjabaran seluruh konsepsi tentang Negara yang terkandung dalam pembukaan', 'S'),
(1064, 237, '', '', 'Mentri', 'S'),
(1065, 238, '', '', 'Kepala Dua', 'S'),
(1066, 239, '', '', 'kemenakan laki-laki', 'S'),
(1067, 240, '', '', 'tempat menyimpan mobil', 'S'),
(1068, 241, '', '', 'energi', 'S'),
(1069, 242, '', '', 'Panas', 'S'),
(1070, 243, '', '', 'Bervariasi', 'S'),
(1071, 244, '', '', 'Pilot : Pesawat', 'B'),
(1072, 245, '', '', 'laut : sungai', 'S'),
(1073, 246, '', '', 'penulis : kuas : cat', 'S'),
(1074, 247, '', '', 'ngantuk : tidur :mimpi', 'S'),
(1075, 248, '', '', 'Demikian berita kami. Semoga Saudara maklum hendaknya', 'S'),
(1076, 249, '', '', 'Empat ekor', 'S'),
(1077, 250, '', '', 'sangat manis', 'S'),
(1078, 251, '', '', 'Susi dan adiknya duduk berbisik bisik dalam ruang tunggu dokter itu menunggu giliran diperiksa', 'S'),
(1079, 252, '', '', 'interferensi', 'S'),
(1080, 253, '', '', 'sekarang sudah memasuki abad ke-XX', 'S'),
(1081, 254, '', '', 'Ibu memasak', 'S'),
(1082, 255, '', '', 'Termotivasi untuk belajar terus mengikuti jejak negara-negara tersebut', 'B'),
(1083, 256, '', '', 'Chris John wajar kalah karena sudah lama menyandang gelar juara tinju dunia kelas bulu WBA', 'S'),
(1084, 257, '', '', 'Te Hasani pasti akan memperoleh nilai tertinggi lagi karena dari awal prestasi ini sudah terbukti', 'S'),
(1085, 258, '', '', 'Menyapa', 'B'),
(1086, 259, '', '', 'Memaksa anak didik agar bisa berbicara lebih dewasa seperti orang tua', 'S'),
(1087, 260, '', '', 'Selamat atas kelulusan kamu di UNG ', 'S'),
(1088, 261, '', '', 'Ikan nila atau Oreochromis niloticus adalah komoditi utama perikanan di Jawa Barat ', 'B'),
(1089, 262, '', '', 'raden ajeng Kartini', 'S'),
(1090, 263, '', '', 'sleeping the baby', 'S'),
(1091, 264, '', '', 'say', 'S'),
(1092, 265, '', '', 'I didn t have a car yet I drove to Puncak very often', 'S'),
(1093, 233, '', '', ' Setiap agama memiliki tujuan dan cara ibadah yang sama kepada Tuhan', 'S'),
(1094, 234, '', '', ' Undang-Undang Dasar', 'S'),
(1095, 235, '', '', ' menyesuaikan diri terhadap tuntutan perkembangan zaman', 'B'),
(1096, 236, '', '', ' Penjabaran lebih rinci dari pokok pokok pikiran yang terkandung dalam pembukaan', 'B'),
(1097, 237, '', '', ' Ketua DPR', 'S'),
(1098, 238, '', '', ' Dua Kekuatan', 'S'),
(1099, 239, '', '', ' sesepuh', 'S'),
(1100, 240, '', '', ' pengampunan hukuman dari presiden', 'B'),
(1101, 241, '', '', ' gigi', 'S'),
(1102, 242, '', '', ' Pasti', 'B'),
(1103, 243, '', '', ' Berubah-ubah', 'B'),
(1104, 244, '', '', ' Baju : Kancing', 'S'),
(1105, 245, '', '', ' orang : pemuda', 'B'),
(1106, 246, '', '', ' dokter : obat : stetoskop', 'S'),
(1107, 247, '', '', ' kecil : sedang : besar', 'S'),
(1108, 248, '', '', ' Atas perhatian saudara kami ucapkan terima kasih', 'S'),
(1109, 249, '', '', ' Ekor', 'S'),
(1110, 250, '', '', ' akan terbang', 'S'),
(1111, 251, '', '', ' suami istri itu hidup berbahagia dengan keempat orang anaknya walaupun mereka tidak tergolong keluarga kaya', 'B'),
(1112, 252, '', '', ' integrasi', 'S'),
(1113, 253, '', '', ' tua-muda, besar-kecil berkumpul di ruang itu', 'S'),
(1114, 254, '', '', ' Ayah membaca', 'S'),
(1115, 255, '', '', ' Merasa kasihan terhadap bangsa Indonesia dengan kemajuan IPTEK yang rendah', 'S'),
(1116, 256, '', '', ' Indonesia harus tetap bangga karena mempunyai petinju sehebat Chris John', 'S'),
(1117, 257, '', '', ' Te Hasani harus menurunkan prestasinya untuk memberi kesempatan kepada teman-teman nya.', 'S'),
(1118, 258, '', '', ' Cemberut', 'S'),
(1119, 259, '', '', ' Selalu menasehati anak didiknya agar menghormati orang tua', 'S'),
(1120, 260, '', '', ' Terima kasih atas perhatian kamu ', 'S'),
(1121, 261, '', '', ' Presiden akan berkunjung ke Negara Jepang pada akhir bulan ini', 'S'),
(1122, 262, '', '', ' Oemar Said Tjokroaminoto', 'B'),
(1123, 263, '', '', ' the baby slept', 'S'),
(1124, 264, '', '', ' to say', 'S'),
(1125, 265, '', '', ' I didn t have a car, so I didn t drive to Puncak very often', 'S'),
(1126, 233, '', '', 'Tersedianya tempat beribadah bagi semua umat beragama dan berdekatan.', 'S'),
(1127, 234, '', '', 'Peraturan Presiden', 'B'),
(1128, 235, '', '', 'mewujudkan demokrasi dalam kehidupan politik, ekonomi, dan sosial', 'S'),
(1129, 236, '', '', 'Dua dokumen historis dalam kehidupan berbangsa dan bernegara bagi bangsa indonesia', 'S'),
(1130, 237, '', '', 'DPR', 'S'),
(1131, 238, '', '', 'Dua Kaki', 'S'),
(1132, 239, '', '', 'paman', 'B'),
(1133, 240, '', '', 'tidak usah bayar', 'S'),
(1134, 241, '', '', 'perut', 'S'),
(1135, 242, '', '', 'Perkiraan', 'S'),
(1136, 243, '', '', 'Berbeda-beda', 'S'),
(1137, 244, '', '', 'Wartawan : Berita', 'S'),
(1138, 245, '', '', 'kapal : perahu', 'S'),
(1139, 246, '', '', 'penari : topi : selendang', 'S'),
(1140, 247, '', '', 'sore : siang : pagi', 'S'),
(1141, 248, '', '', 'Demikian agar instruksi ini dilaksanakan dengan penuh rasa tanggung jawab', 'B'),
(1142, 249, '', '', 'Ikan', 'B'),
(1143, 250, '', '', 'perahu layar', 'S'),
(1144, 251, '', '', 'seluruh rakyat indonesia ikut berjuang dengan kemampuan masing masing untuk mempertahankan kemerdekaan', 'S'),
(1145, 252, '', '', 'infeksi', 'S'),
(1146, 253, '', '', 'tamu-tamu diperbolehkan masuk', 'B'),
(1147, 254, '', '', 'Adik ke sekolah', 'B'),
(1148, 255, '', '', 'Mencari terobosan baru dengan penemuan yang lebih tinggi yang bisa mengalahkan negara-negara tersebut', 'S'),
(1149, 256, '', '', 'Chris John kalah karena pihak promotor salah memilih lawan tanding', 'S'),
(1150, 257, '', '', 'Te Hasani harus tetap berusaha mempertahankan prestasinya dengan belajar lebih keras', 'B'),
(1151, 258, '', '', 'Tersenyum dan menyentuh', 'S'),
(1152, 259, '', '', 'Sebagai orang tua santai saja saat mengajar', 'S'),
(1153, 260, '', '', 'Dilarang parkir ', 'S'),
(1154, 261, '', '', 'Sebagai negara mandiri kita tidak boleh tunduk kepada negara asing', 'S'),
(1155, 262, '', '', 'SBYudhoyono', 'S'),
(1156, 263, '', '', 'the sleeping baby', 'B'),
(1157, 264, '', '', 'said', 'S'),
(1158, 265, '', '', 'I don t have a car, so I don t drive very often', 'B'),
(1159, 233, '', '', 'Persebaran agama dapat dilakukan kepada siapa saja dan dimana saja.', 'S'),
(1160, 234, '', '', 'Negara Kesatuan Republik Indonesia', 'S'),
(1161, 235, '', '', 'mewujudkan kehidupan yang berkeadilan dan keberadaban', 'S'),
(1162, 236, '', '', 'Norma norma dasar dalam kehidupan bernegara bagi bangsa Indonesia', 'S'),
(1163, 237, '', '', 'MPR', 'B'),
(1164, 238, '', '', 'Dibagi Dua', 'B'),
(1165, 239, '', '', 'saudara', 'S'),
(1166, 240, '', '', 'alat untuk memotong kayu', 'S'),
(1167, 241, '', '', 'tubuh', 'B'),
(1168, 242, '', '', 'Dingin', 'S'),
(1169, 243, '', '', 'Situasional', 'S'),
(1170, 244, '', '', 'Guru : Murid', 'S'),
(1171, 245, '', '', 'gaji : uang', 'S'),
(1172, 246, '', '', 'guru : kapur : topi', 'S'),
(1173, 247, '', '', 'bayi : kanak kanak : remaja', 'B'),
(1174, 248, '', '', 'Mudah mudahan bahan pertimbangan yang kami kemukakan diatas bermanfaat bagi Saudara', 'S'),
(1175, 249, '', '', 'Hiu raksasa', 'S'),
(1176, 250, '', '', 'sayur asam', 'B'),
(1177, 251, '', '', 'lebih baik kita berangkat agak awal agar tidak kemalaman di perjalanan', 'S'),
(1178, 252, '', '', 'intervensi', 'S'),
(1179, 253, '', '', 'Drs.M.P. Sinaga. S.H. sudah datang', 'S'),
(1180, 254, '', '', 'Sangat tinggi menara itu', 'S'),
(1181, 255, '', '', 'Merasa bangga negara lain sudah mencipatakan teknologi tinggi', 'S'),
(1182, 256, '', '', 'Gelar juara tinju dunia kelas bulu WBA harus kembali direbut Chris John', 'B'),
(1183, 257, '', '', 'Te Hasani sudah bisa santai belajar karena dengan sendirinya prestasinya akan tetap dipertahankan', 'S'),
(1184, 258, '', '', 'Bertindak', 'S'),
(1185, 259, '', '', 'Mengajar dengan gaya komunikasi yang disukai remaja', 'B'),
(1186, 260, '', '', 'Bagaimana pendapat kamu tentang kejadian tadi', 'B'),
(1187, 261, '', '', 'Budaya kita banyak yang diakui oleh negara tetangga ', 'S'),
(1188, 262, '', '', 'hajah Mama dedeh', 'S'),
(1189, 263, '', '', 'the baby was sleeping', 'S'),
(1190, 264, '', '', 'says', 'S'),
(1191, 265, '', '', 'I don t have a car, yet I drove to Puncak very often', 'S'),
(1192, 233, '', '', 'Menghargai aktivitas inter dan antar umat beragama serta pemerintah.', 'B'),
(1193, 234, '', '', 'Pancasila', 'S'),
(1194, 235, '', '', 'membangkitkan kesadaran hidup dalam berbangsa dan bernegara', 'S'),
(1195, 236, '', '', 'Tidak ada jawaban benar', 'S'),
(1196, 237, '', '', 'Ketua MPR', 'S'),
(1197, 238, '', '', 'Dua Fungsi', 'S'),
(1198, 239, '', '', 'ibu', 'S'),
(1199, 240, '', '', 'percetakan', 'S'),
(1200, 241, '', '', 'mulut', 'S'),
(1201, 242, '', '', 'Perulangan', 'S'),
(1202, 243, '', '', 'Konsisten', 'S'),
(1203, 244, '', '', 'Raja : Mahkota', 'S'),
(1204, 245, '', '', 'binatang : lawan', 'S'),
(1205, 246, '', '', 'montir : obeng : tang', 'B'),
(1206, 247, '', '', 'anak : ayah : kakek', 'S'),
(1207, 248, '', '', 'Harapan kami semoga kerja sama yang telah kita bina dapat ditingkatkan terus', 'S'),
(1208, 249, '', '', 'Ikan raksasa', 'S'),
(1209, 250, '', '', 'ikan mas', 'S'),
(1210, 251, '', '', 'D.Budi berlari menyusul kakaknya yang sudah berangkat lebih dahulu', 'S'),
(1211, 252, '', '', 'invasi', 'B'),
(1212, 253, '', '', 'jalan itu rusak dari Km.4-Km-9', 'S'),
(1213, 254, '', '', 'Saya mencubitnya', 'S'),
(1214, 255, '', '', 'Merasa jiwa nasionalisme tercoreng karena tidak mampu seperti negara-negara tersebut', 'S'),
(1215, 256, '', '', 'Saya memfavoritkan Simpiwe Vetyeka karena lebih hebat dari Chris John', 'S'),
(1216, 257, '', '', 'Te Hasani sebaiknya diberikan beasiswa oleh pihak kampus untuk melanjutkan ke jenjang lebih tinggi', 'S'),
(1217, 258, '', '', 'Diam saja', 'S'),
(1218, 259, '', '', 'Sebagai guru senior apa yang diketahui harus disampaikan secara detail dan menyeluruh kepada anak didiknya', 'S'),
(1219, 260, '', '', 'Masih ada yang menganggap bumi itu datar', 'S'),
(1220, 261, '', '', 'Persatuan Bangsa-Bangsa adalah organisasi antarnegara yang sangat besar ', 'S'),
(1221, 262, '', '', 'tika Puspitasari', 'S'),
(1222, 263, '', '', 'the baby sleeping', 'S'),
(1223, 264, '', '', 'saying', 'B'),
(1224, 265, '', '', 'I had a car so I could drive to Puncak very often', 'S'),
(1225, 266, '', '', ' 22  25', 'S'),
(1226, 267, '', '', ' 54  57', 'S'),
(1227, 268, '', '', ' 34 dan 40', 'S'),
(1228, 269, '', '', ' X dan Y tidak bisa ditentukan', 'S'),
(1229, 270, '', '', '14', 'S'),
(1230, 271, '', '', '3.66', 'S'),
(1231, 272, '', '', '28', 'B'),
(1232, 273, '', '', ' 250 km/jam', 'S'),
(1233, 274, '', '', ' Rp 450.000', 'S'),
(1234, 275, '', '', ' 20 jam', 'S'),
(1235, 276, '', '', ' hanya cincin X dan Y yang berbeda kilaunya', 'S'),
(1236, 277, '', '', ' Barang plastik hanya dijual di toko Kurnia ', 'S'),
(1237, 278, '', '', ' I', 'S'),
(1238, 279, '', '', ' Nida, Badu, Anto, Sari, Intan', 'S'),
(1239, 280, '', '', ' Noda pada pakaian akan tampak jelas.', 'S'),
(1240, 266, '', '', ' 22   26', 'S'),
(1241, 267, '', '', ' 55   60', 'S'),
(1242, 268, '', '', ' 40 dan 36', 'S'),
(1243, 269, '', '', ' XY < Y', 'S'),
(1244, 270, '', '', '18', 'S'),
(1245, 271, '', '', '4.66', 'B'),
(1246, 272, '', '', '48', 'S'),
(1247, 273, '', '', ' 450 km/jam', 'S'),
(1248, 274, '', '', ' Rp 462.000', 'B'),
(1249, 275, '', '', ' 24 jam', 'S'),
(1250, 276, '', '', ' Cincin berlian X dan Y mempunyai kilau yang berbeda.', 'B'),
(1251, 277, '', '', ' Semua barang di toko Kurnia bahan dasarnya plastik', 'S'),
(1252, 278, '', '', ' II', 'S'),
(1253, 279, '', '', ' Badu, Nida, Sari, Intan, Anto', 'S'),
(1254, 280, '', '', ' Tidak mungkin segumpal detergen dapat menghilangkan noda.', 'S'),
(1255, 266, '', '', ' 23   26', 'S'),
(1256, 267, '', '', ' 56   61', 'B'),
(1257, 268, '', '', ' 41 dan 30', 'S'),
(1258, 269, '', '', ' X < Y', 'S'),
(1259, 270, '', '', '21', 'S'),
(1260, 271, '', '', '0.03', 'S'),
(1261, 272, '', '', '54', 'S'),
(1262, 273, '', '', ' 900 km/jam', 'B'),
(1263, 274, '', '', ' Rp 472.000', 'S'),
(1264, 275, '', '', ' 44 jam', 'B'),
(1265, 276, '', '', ' Semua cincin berlian memiliki kilau yang berbeda dengan cincin X dan Y.', 'S'),
(1266, 277, '', '', ' Tidak ada tekstil yang dijual di toko kurnia. ', 'B'),
(1267, 278, '', '', ' III', 'S'),
(1268, 279, '', '', ' Intan, Badu, Sari, Anto, Nida', 'S'),
(1269, 280, '', '', ' Detergen membuat noda putih tampak tidak jelas pada pakaian hitam bernoda putih.', 'B'),
(1270, 266, '', '', ' 24   27', 'B'),
(1271, 267, '', '', ' 60   55', 'S'),
(1272, 268, '', '', ' 36 dan 41', 'B'),
(1273, 269, '', '', ' X = Y', 'S'),
(1274, 270, '', '', '24', 'B'),
(1275, 271, '', '', ' 3,66%', 'S'),
(1276, 272, '', '', '50', 'S'),
(1277, 273, '', '', ' 15 km/jam', 'S'),
(1278, 274, '', '', ' Rp 500.000', 'S'),
(1279, 275, '', '', ' 50 jam', 'S'),
(1280, 276, '', '', ' Ada cincin berlian selain cincin X dan Y yang memiliki kilau. ', 'S'),
(1281, 277, '', '', ' Toko kurnia menjual barang dari kapas dan plastik.', 'S'),
(1282, 278, '', '', ' IV', 'S'),
(1283, 279, '', '', ' Nida, Badu, Sari, Anto, Intan', 'B'),
(1284, 280, '', '', ' Tidak mungkin hitam menjadi putih', 'S'),
(1285, 266, '', '', ' 24   29', 'S'),
(1286, 267, '', '', ' 61   56', 'S'),
(1287, 268, '', '', ' 36 dan 40', 'S'),
(1288, 269, '', '', ' X > Y', 'B'),
(1289, 270, '', '', '36', 'S'),
(1290, 271, '', '', '0.04', 'S'),
(1291, 272, '', '', '26', 'S'),
(1292, 273, '', '', ' 300 m/detik', 'S'),
(1293, 274, '', '', ' Rp 520.000', 'S'),
(1294, 275, '', '', ' 54 jam', 'S'),
(1295, 276, '', '', ' Semua perhiasan yang sama bentuk dengan cincin X dan Y pasti berkilau.', 'S'),
(1296, 277, '', '', ' Di toko Kurnia terdapat segalam macam barang.', 'S'),
(1297, 278, '', '', ' V', 'B'),
(1298, 279, '', '', ' Intan, Nida, Sari, Badu, Anto', 'S'),
(1299, 280, '', '', ' Deterjen menimbulkan noda.', 'S'),
(1300, 281, '', '', ' Lembaga Negara', 'S'),
(1301, 282, '', '', ' Sila-sila Pancasila itu harus kita lihat sebagai satu rangkaian kesatuan', 'S'),
(1302, 283, '', '', ' Memperkaya budaya bangsa Indonesia dengan keanekaragaman perbedaan', 'S'),
(1303, 284, '', '', ' Lima falsafah negara Indonesia', 'S'),
(1304, 285, '', '', ' Pernyataan kemerdekaan yang terperinci', 'B'),
(1305, 286, '', '', ' Penggambaran', 'B'),
(1306, 287, '', '', ' agitasi', 'S'),
(1307, 288, '', '', ' Rajin', 'S'),
(1308, 289, '', '', ' melebar', 'S'),
(1309, 290, '', '', ' palsu', 'S'),
(1310, 291, '', '', ' Newton : Apel', 'S'),
(1311, 292, '', '', ' Akademi : Taruna', 'S'),
(1312, 293, '', '', ' Telur : Kucing ', 'S'),
(1313, 294, '', '', ' nahkoda : kapal : mualim', 'B'),
(1314, 295, '', '', ' santai', 'S'),
(1315, 296, '', '', ' berhari-hari, mondar-mandir, bercakap-cakap ', 'S'),
(1316, 297, '', '', ' tang - tank', 'S'),
(1317, 298, '', '', ' Di Indonesia banyak lahan pertanian yang subur. ', 'S'),
(1318, 299, '', '', ' kondisi itu tidak akan menambah perkembangan industri ternak', 'S'),
(1319, 300, '', '', ' Pegawai mendapat gaji per satu Mei', 'S'),
(1320, 301, '', '', ' Bekerja keras akan mendatangkan hasil maksimal. ', 'S'),
(1321, 302, '', '', ' Revolusi akan berlangsung dalam dua tahap.', 'S'),
(1322, 303, '', '', ' Membandingkan dengan prestasi yang pernah saya lakukan', 'B'),
(1323, 304, '', '', ' menerimanya dengan terpaksa ', 'S'),
(1324, 305, '', '', ' Mendekatkan diri dengan teman kampus yang lebih sesuai dengan kepribadiannya', 'S'),
(1325, 306, '', '', ' Saya percaya dokter RS mampu menangani dengan baik', 'S'),
(1326, 307, '', '', ' Memperhatikan sekeliling jalan', 'S'),
(1327, 308, '', '', ' Apa tim kamu meraih kemenangan tadi malam ', 'S'),
(1328, 309, '', '', ' Ikan nila atau Oreochromis niloticus adalah komoditi utama perikanan di Jawa Barat ', 'B'),
(1329, 310, '', '', ' Budianto , SH.', 'S'),
(1330, 311, '', '', ' had seen', 'S'),
(1331, 312, '', '', ' cry', 'S'),
(1332, 313, '', '', ' must', 'S'),
(1333, 281, '', '', ' Kekuasaan belaka', 'B'),
(1334, 282, '', '', ' Harus kita fahami sebagai totalitas', 'S'),
(1335, 283, '', '', ' Pemersatu perbedaan keanekaragaman di Indonesia', 'B'),
(1336, 284, '', '', ' Lima pandangan hidup negara Indonesia', 'S'),
(1337, 285, '', '', ' Pernyataan tentang hak-hak asasi manusia', 'S'),
(1338, 286, '', '', ' Pemahaman cerita', 'S'),
(1339, 287, '', '', ' tekanan', 'S'),
(1340, 288, '', '', ' Pandai', 'S'),
(1341, 289, '', '', ' memisah', 'B'),
(1342, 290, '', '', ' tiruan', 'S'),
(1343, 291, '', '', ' Binatang : Fabel', 'S'),
(1344, 292, '', '', ' Rumah sakit : Pasien', 'S');
INSERT INTO `t_option_soal` (`id_jawaban`, `id_soal`, `file`, `tipe_file`, `jawaban`, `benar`) VALUES
(1345, 293, '', '', ' Air payau : Air tawar ', 'S'),
(1346, 294, '', '', ' dokter : obat : stetoskop', 'S'),
(1347, 295, '', '', ' senang', 'S'),
(1348, 296, '', '', ' bercakap-cakap, tanya-menanya, pandang-memandang ', 'B'),
(1349, 297, '', '', ' bunga bank - bang Amat', 'S'),
(1350, 298, '', '', ' Karena hujan terus-menerus, daerah itu banjir lagi. ', 'S'),
(1351, 299, '', '', ' Hampir semua negara tidak peduli akan pajak hasil ternak', 'B'),
(1352, 300, '', '', ' Mereka masuk satu per satu', 'B'),
(1353, 301, '', '', ' Jika orang bekerja keras, orang tersebut akan mendapat hasil maksimal. ', 'B'),
(1354, 302, '', '', ' Kita juga harus belajar dari masa lampau.', 'S'),
(1355, 303, '', '', ' Merasa iba dan terharu', 'S'),
(1356, 304, '', '', ' menerimanya, meski tentu saja dengan sedikit kekecewaan ', 'S'),
(1357, 305, '', '', ' Tetap menjaga integritas dengan mempertahankan kebiasaan suaranya yang keras', 'S'),
(1358, 306, '', '', ' Saya akan menjenguknya ketika ada waktu yang benar-benar longgar', 'B'),
(1359, 307, '', '', ' Berhenti karena takut polisi', 'S'),
(1360, 308, '', '', ' Bagaimana hadiahnya ', 'S'),
(1361, 309, '', '', ' Presiden akan berkunjung ke Negara Jepang pada akhir bulan ini', 'S'),
(1362, 310, '', '', ' Budianto. SH.', 'S'),
(1363, 311, '', '', ' see', 'S'),
(1364, 312, '', '', ' cries', 'S'),
(1365, 313, '', '', ' can', 'S'),
(1366, 281, '', '', ' Kedaulatan rakyat', 'S'),
(1367, 282, '', '', ' Susunan dan bentuknya hirearkis pyramidal', 'S'),
(1368, 283, '', '', ' Memupuk jiwa semangat membangun bangsa dengan semboyan yang disepakati bersama', 'S'),
(1369, 284, '', '', ' Lima aturan negara Indonesia', 'S'),
(1370, 285, '', '', ' Deklarasi terbentuknya bangsa dan Negara', 'S'),
(1371, 286, '', '', ' Penulisan naskah', 'S'),
(1372, 287, '', '', ' ancaman', 'B'),
(1373, 288, '', '', ' Istimewa', 'S'),
(1374, 289, '', '', ' mengurai', 'S'),
(1375, 290, '', '', ' plagiat', 'B'),
(1376, 291, '', '', ' Penicillin : Fleming', 'B'),
(1377, 292, '', '', ' Konservator : Seniman', 'S'),
(1378, 293, '', '', '  Kucing : Ikan ', 'S'),
(1379, 294, '', '', ' penari : topi : selendang', 'S'),
(1380, 295, '', '', ' membaca', 'B'),
(1381, 296, '', '', ' berhari-hari, bercakap-cakap, tertawa-tawa ', 'S'),
(1382, 297, '', '', ' kemeja - ke meja', 'S'),
(1383, 298, '', '', ' Jika kamu belajar, kamu akan sukses. ', 'B'),
(1384, 299, '', '', ' pemeritah tidak akan mengadakan penjadwalan kembali hutang luar negeri.', 'S'),
(1385, 300, '', '', ' Harga jeruk itu Rp. 500 perbuah', 'S'),
(1386, 301, '', '', ' Petinju itu berlatih setiap hari sehingga ia menjadi juara. ', 'S'),
(1387, 302, '', '', ' Tyson akhirnya kalah di tangan Douglas.', 'B'),
(1388, 303, '', '', ' Salut dengan perjuangannya', 'S'),
(1389, 304, '', '', ' menerimanya dengan lapang dada ', 'B'),
(1390, 305, '', '', ' Menunjukkan identitas sukunya dalam lingkungan kampus yang baru untuk menunjukkan kekayaan budaya Indonesia', 'S'),
(1391, 306, '', '', ' Saya akan menjenguknya', 'S'),
(1392, 307, '', '', ' Berhenti sampai lampu hijau menyala', 'B'),
(1393, 308, '', '', ' Hati-hati di jalan ', 'B'),
(1394, 309, '', '', ' Sebagai negara mandiri kita tidak boleh tunduk kepada negara asing', 'S'),
(1395, 310, '', '', ' Budianto, S.H', 'S'),
(1396, 311, '', '', ' to see', 'S'),
(1397, 312, '', '', ' crying', 'B'),
(1398, 313, '', '', ' may', 'B'),
(1399, 281, '', '', ' Majelis Permusyawaratan Rakyat', 'S'),
(1400, 282, '', '', ' Banyak orang yang melanggarnya', 'S'),
(1401, 283, '', '', ' Memupuk jiwa nasionalisame', 'S'),
(1402, 284, '', '', ' Lima dasar negara Indonesia', 'B'),
(1403, 285, '', '', ' Peraturan perundang-undangan tertinggi', 'S'),
(1404, 286, '', '', ' Penanaman saham', 'S'),
(1405, 287, '', '', ' dorongan', 'S'),
(1406, 288, '', '', ' Bebal', 'B'),
(1407, 289, '', '', ' membagi', 'S'),
(1408, 290, '', '', ' imitasi', 'S'),
(1409, 291, '', '', ' Beckham : Inggris', 'S'),
(1410, 292, '', '', ' Perpustakaan : Peneliti', 'B'),
(1411, 293, '', '', ' Insang : Paru-paru ', 'B'),
(1412, 294, '', '', ' guru : sekolah : kapur', 'S'),
(1413, 295, '', '', ' musik', 'S'),
(1414, 296, '', '', ' tertawa-tawa, tanya-menanya, lihat-melihat ', 'S'),
(1415, 297, '', '', ' pejabat teras - teras rumah', 'B'),
(1416, 298, '', '', ' Kepercayaan dari seseorang jangan kamu sia-siakan. ', 'S'),
(1417, 299, '', '', ' usaha ternak akan terhambat kalau dikenakan PPN 10 %', 'S'),
(1418, 300, '', '', ' Harga benda itu Rp. 200 perbiji', 'S'),
(1419, 301, '', '', ' Orang itu berusaha keras maka ia akan berhasil. ', 'S'),
(1420, 302, '', '', ' Tulisan itu belum sempat kubaca.', 'S'),
(1421, 303, '', '', ' Biasa saja karena prestasi tersebut karena latihan dan bakat', 'S'),
(1422, 304, '', '', ' membenci diri saya sendiri ', 'S'),
(1423, 305, '', '', ' Berusaha banyak diam karena kepribadiannya sangat berbeda dengan lingkungannya yang baru', 'S'),
(1424, 306, '', '', ' Saya menanyakan apakah kondisinya memang parah', 'S'),
(1425, 307, '', '', ' Jalan terus dengan santai', 'S'),
(1426, 308, '', '', ' Kapan kau mau datang', 'S'),
(1427, 309, '', '', ' Budaya kita banyak yang diakui oleh negara tetangga ', 'S'),
(1428, 310, '', '', ' Budianto , SH', 'S'),
(1429, 311, '', '', ' seeing', 'B'),
(1430, 312, '', '', ' cried', 'S'),
(1431, 313, '', '', ' had better', 'S'),
(1432, 281, '', '', ' Adat istiadat', 'S'),
(1433, 282, '', '', ' Jawaban A, B, C benar', 'B'),
(1434, 283, '', '', ' Memberikan kenyamanan bagi setiap agama menjalankan kepercayaannya', 'S'),
(1435, 284, '', '', ' Lima kesepakatan bernegara di Indonesia', 'S'),
(1436, 285, '', '', ' Semua jawaban benar', 'S'),
(1437, 286, '', '', ' Pendadaran', 'S'),
(1438, 287, '', '', ' provokasi', 'S'),
(1439, 288, '', '', ' Parlente', 'S'),
(1440, 289, '', '', ' menyebar', 'S'),
(1441, 290, '', '', ' jiplakan', 'S'),
(1442, 291, '', '', ' Barcelona : Messi', 'S'),
(1443, 292, '', '', ' Kera : Monyet ', 'S'),
(1444, 293, '', '', ' Kera : Monyet ', 'S'),
(1445, 294, '', '', ' montir : obeng : tang', 'S'),
(1446, 295, '', '', ' kata-kata', 'S'),
(1447, 296, '', '', ' tanya-menanya, lihat-melihat, berhari-hari ', 'S'),
(1448, 297, '', '', ' merasa sangsi - ada sanksinya', 'S'),
(1449, 298, '', '', ' Kita harus berhati-hati agar kita selamat di sana. ', 'S'),
(1450, 299, '', '', 'D.peranan industri makanan ternak cukup strategis', 'S'),
(1451, 300, '', '', ' Ayah memberikan dua bungkus perorang', 'S'),
(1452, 301, '', '', ' Polisi menegakkan hukum supaya keadilan tercapai. ', 'S'),
(1453, 302, '', '', ' Perusahaan itu hampir saja bangkrut.', 'S'),
(1454, 303, '', '', ' Termotivasi menyumbang untuk organisasi amal', 'S'),
(1455, 304, '', '', ' meretapi diri sendiri ', 'S'),
(1456, 305, '', '', ' Belajar berbicara lembut seperti teman-teman sekampusnya', 'B'),
(1457, 306, '', '', ' Saya berharap semoga lekas sembuh', 'S'),
(1458, 307, '', '', ' Jalan terus dengan menaikkan kecepatan', 'S'),
(1459, 308, '', '', ' Aku jadi ke rumahmu nanti sore ', 'S'),
(1460, 309, '', '', ' Persatuan Bangsa-Bangsa adalah organ isasi antarnegara yang sangat besar ', 'S'),
(1461, 310, '', '', ' Budianto, S.H.', 'B'),
(1462, 311, '', '', ' will see', 'S'),
(1463, 312, '', '', ' to cry', 'S'),
(1464, 313, '', '', ' should', 'S'),
(1465, 314, '', '', ' 22  25', 'S'),
(1466, 315, '', '', ' 26  8', 'B'),
(1467, 316, '', '', ' 34 dan 40', 'S'),
(1468, 317, '', '', ' X dan Y tidak bisa ditentukan', 'S'),
(1469, 318, '', '', '10', 'S'),
(1470, 319, '', '', '3.66', 'S'),
(1471, 320, '', '', ' Nida, Badu, Anto, Sari, Intan', 'S'),
(1472, 321, '', '', '15', 'S'),
(1473, 322, '', '', ' Rp 450.000', 'S'),
(1474, 323, '', '', ' 20 jam', 'S'),
(1475, 324, '', '', ' Memiliki cara berkembang biak yang tidak sama dengan unggas. ', 'S'),
(1476, 325, '', '', ' Barang plastik hanya dijual di toko Kurnia ', 'S'),
(1477, 326, '', '', ' Sebagian calon mahasiswa yang tidak memiliki skor TOEFL menempuh tes Matematik  ', 'B'),
(1478, 327, '', '', ' Noda pada pakaian akan tampak jelas.', 'S'),
(1479, 328, '', '', ' Tidak mungkin ada anggota kelompok A yang tidak merupakan anggota kelompok C.', 'S'),
(1480, 314, '', '', ' 22   26', 'S'),
(1481, 315, '', '', ' 8   26', 'S'),
(1482, 316, '', '', ' 40 dan 36', 'S'),
(1483, 317, '', '', ' XY < Y', 'S'),
(1484, 318, '', '', '11', 'B'),
(1485, 319, '', '', '4.66', 'B'),
(1486, 320, '', '', ' Badu, Nida, Sari, Intan, Anto', 'S'),
(1487, 321, '', '', '14.3', 'B'),
(1488, 322, '', '', ' Rp 462.000', 'B'),
(1489, 323, '', '', ' 24 jam', 'S'),
(1490, 324, '', '', ' Memiliki ciri-ciri yang sama dengan unggas. ', 'S'),
(1491, 325, '', '', ' Semua barang di toko Kurnia bahan dasarnya plastik', 'S'),
(1492, 326, '', '', ' Semua calon mahasiswa yang memiliki skor TOEFL, tidak menempuh tes Matematika ', 'S'),
(1493, 327, '', '', ' Tidak mungkin segumpal detergen dapat menghilangkan noda.', 'S'),
(1494, 328, '', '', ' Mungkin ada anggota kelompok A yang tidak merupakan anggota C.', 'S'),
(1495, 314, '', '', ' 23   26', 'S'),
(1496, 315, '', '', ' 7   27', 'S'),
(1497, 316, '', '', ' 41 dan 30', 'S'),
(1498, 317, '', '', ' X < Y', 'S'),
(1499, 318, '', '', '12', 'S'),
(1500, 319, '', '', '0.03', 'S'),
(1501, 320, '', '', ' Intan, Badu, Sari, Anto, Nida', 'S'),
(1502, 321, '', '', '14', 'S'),
(1503, 322, '', '', ' Rp 472.000', 'S'),
(1504, 323, '', '', ' 44 jam', 'B'),
(1505, 324, '', '', ' Memiliki cara berkembang biak yang sama dengan unggas.', 'B'),
(1506, 325, '', '', ' Tidak ada tekstil yang dijual di toko kurnia. ', 'B'),
(1507, 326, '', '', ' Semua calon mahasiswa yang tidak memiliki skor TOEFL, tidak menempuh tes Matematika. ', 'S'),
(1508, 327, '', '', ' Detergen membuat noda putih tampak tidak jelas pada pakaian hitam bernoda putih.', 'B'),
(1509, 328, '', '', ' Tidak mungkin ada anggota kelompok C yang merupakan anggota A.', 'S'),
(1510, 314, '', '', ' 24   27', 'B'),
(1511, 315, '', '', ' 27   7', 'S'),
(1512, 316, '', '', ' 36 dan 41', 'B'),
(1513, 317, '', '', ' X = Y', 'S'),
(1514, 318, '', '', '13', 'S'),
(1515, 319, '', '', ' 3,66%', 'S'),
(1516, 320, '', '', ' Nida, Badu, Sari, Anto, Intan', 'B'),
(1517, 321, '', '', '15.3', 'S'),
(1518, 322, '', '', ' Rp 500.000', 'S'),
(1519, 323, '', '', ' 50 jam', 'S'),
(1520, 324, '', '', ' Memiliki telur yang sama dengan golongan unggas.', 'S'),
(1521, 325, '', '', ' Toko kurnia menjual barang dari kapas dan plastik.', 'S'),
(1522, 326, '', '', ' Sebagian calon mahasiswa yang memiliki skor TOEFL tidak menempuh tes Matematika. ', 'S'),
(1523, 327, '', '', ' Tidak mungkin hitam menjadi putih', 'S'),
(1524, 328, '', '', ' Tidak mungkin ada anggota kelompok C yang merupakan anggota A.', 'S'),
(1525, 314, '', '', ' 24   29', 'S'),
(1526, 315, '', '', ' 6   25', 'S'),
(1527, 316, '', '', ' 36 dan 40', 'S'),
(1528, 317, '', '', ' X > Y', 'B'),
(1529, 318, '', '', '14', 'S'),
(1530, 319, '', '', '0.04', 'S'),
(1531, 320, '', '', ' Intan, Nida, Sari, Badu, Anto', 'S'),
(1532, 321, '', '', '13', 'S'),
(1533, 322, '', '', ' Rp 520.000', 'S'),
(1534, 323, '', '', ' 54 jam', 'S'),
(1535, 324, '', '', ' Memiliki cara bertelur yang sama dengan unggas.', 'S'),
(1536, 325, '', '', ' Di toko Kurnia terdapat segalam macam barang.', 'S'),
(1537, 326, '', '', ' Sebagian calon mahasiswa yang memiliki skor TOEFL tidak menempuh tes Matematika. ', 'S'),
(1538, 327, '', '', ' Deterjen menimbulkan noda.', 'S'),
(1539, 328, '', '', ' Mungkin ada anggota kelompok C yang tidak merupakan anggota A.', 'B');

-- --------------------------------------------------------

--
-- Table structure for table `t_peserta`
--

CREATE TABLE `t_peserta` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `nama_peserta` varchar(150) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `id_pilihan_peserta` int(11) NOT NULL,
  `id_gelombang` int(11) NOT NULL,
  `level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_petugas_jenis_soal`
--

CREATE TABLE `t_petugas_jenis_soal` (
  `id` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_jenis_soal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_petugas_jenis_soal`
--

INSERT INTO `t_petugas_jenis_soal` (`id`, `id_petugas`, `id_jenis_soal`) VALUES
(1, 1, 1),
(2, 2, 1),
(7, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_petugas_soal`
--

CREATE TABLE `t_petugas_soal` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_petugas_soal`
--

INSERT INTO `t_petugas_soal` (`id`, `nama`) VALUES
(1, 'Ebi'),
(2, 'Rahmad'),
(3, 'Gilberto');

-- --------------------------------------------------------

--
-- Table structure for table `t_pilihan_peserta`
--

CREATE TABLE `t_pilihan_peserta` (
  `id_pilihan_peserta` int(11) NOT NULL,
  `pilihan_peserta` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pilihan_peserta`
--

INSERT INTO `t_pilihan_peserta` (`id_pilihan_peserta`, `pilihan_peserta`) VALUES
(1, 'Saintek'),
(2, 'Soshum'),
(3, 'Campuran');

-- --------------------------------------------------------

--
-- Table structure for table `t_ruangan`
--

CREATE TABLE `t_ruangan` (
  `id` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `ruangan` varchar(55) NOT NULL,
  `token_ujian` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_ruangan`
--

INSERT INTO `t_ruangan` (`id`, `id_ruangan`, `ruangan`, `token_ujian`) VALUES
(1, 1, 'Lab. Bah. Ingrris Lt. 3', 'aa1234'),
(2, 2, 'Lab. Fak. Hukum Lt. 2', 'bb123'),
(3, 3, 'Lab. FIP Lt. 2', 'cc123'),
(4, 4, 'Lab. Matematika Lt. 1', 'dd123'),
(5, 5, 'Lab. MIPA Lt. 2', 'ee123'),
(6, 6, 'Lab. Multimedia FSB Lt. 1', 'ff123'),
(7, 7, 'Lab. Informatika 1', 'gg123'),
(8, 8, 'Lab. Informatika 2', 'hh123'),
(9, 9, 'Lab. Informatika 3', 'ii123'),
(10, 10, 'Laboratorium Industri Lt. 2', 'jj123'),
(11, 11, 'Laboratorium Komputer 1', 'kk123'),
(12, 12, 'Laboratorium Komputer 2', 'll123'),
(13, 13, 'Lab A', 'mm123'),
(14, 14, 'Lab B', 'nn123'),
(15, 15, 'Lab. Agribisnis', 'pp123');

-- --------------------------------------------------------

--
-- Table structure for table `t_soal`
--

CREATE TABLE `t_soal` (
  `id` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_jenis_soal` int(11) NOT NULL,
  `bobot` int(3) NOT NULL,
  `file` varchar(150) NOT NULL,
  `tipe_file` varchar(50) NOT NULL,
  `soal` longtext NOT NULL,
  `opsi_a` longtext NOT NULL,
  `opsi_b` longtext NOT NULL,
  `opsi_c` longtext NOT NULL,
  `opsi_d` longtext NOT NULL,
  `opsi_e` longtext NOT NULL,
  `jawaban` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_soal`
--

INSERT INTO `t_soal` (`id`, `id_petugas`, `id_jenis_soal`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `jawaban`) VALUES
(26, 1, 1, 1, 'gambar_soal_26.png', 'image/png', '<p>Gambar 1</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(27, 1, 1, 1, 'gambar_soal_27.png', 'image/png', '<p>Gambar 2</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(28, 1, 1, 1, 'gambar_soal_28.png', 'image/png', '<p>Gambar 3</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(29, 1, 1, 1, 'gambar_soal_29.png', 'image/png', '<p>Gambar 4</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(30, 1, 1, 1, 'gambar_soal_30.png', 'image/png', '<p>Gambar 5</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(31, 1, 1, 1, 'gambar_soal_31.png', 'image/png', '<p>Gambar 6</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(32, 1, 1, 1, 'gambar_soal_32.png', 'image/png', '<p>Gambar 7</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(33, 1, 1, 1, 'gambar_soal_33.png', 'image/png', '<p>Gambar 8</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(34, 1, 1, 1, 'gambar_soal_34.png', 'image/png', '<p>Gambar 9</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(35, 1, 1, 1, 'gambar_soal_35.png', 'image/png', '<p>Gambar 10</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(36, 1, 1, 1, '', '', '<p>Berikut adalah table rentang waktu yang diperlukan oleh 5 orang anak untuk berjalan menempuh jarak tertentu.</p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Nama Anak</td>\r\n			<td>Waktu (WIB)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>David</td>\r\n			<td>09.00 - 09.25</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Dimas</td>\r\n			<td>09.27 - 09.51</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sigit</td>\r\n			<td>09.52 - 10.09</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Candra</td>\r\n			<td>10.10 - 10.26</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Andi</td>\r\n			<td>10.27 - 10.50</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(37, 1, 1, 1, '', '', '<p>Tabel di bawah ini menjelaskan jumlah lulusan (dalam ribu) dari tiga sekolah (A, B, C) yang bekerja di lima buah pabrik.</p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan=\"1\" rowspan=\"2\">Pabrik</td>\r\n			<td colspan=\"3\" rowspan=\"1\">Sekolah</td>\r\n		</tr>\r\n		<tr>\r\n			<td>A</td>\r\n			<td>B</td>\r\n			<td>C</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Baja</td>\r\n			<td>4</td>\r\n			<td>5</td>\r\n			<td>4</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Keramik</td>\r\n			<td>3</td>\r\n			<td>5</td>\r\n			<td>5</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Kaca</td>\r\n			<td>3</td>\r\n			<td>5</td>\r\n			<td>3</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Kayu</td>\r\n			<td>4</td>\r\n			<td>6</td>\r\n			<td>4</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Tanah</td>\r\n			<td>5</td>\r\n			<td>6</td>\r\n			<td>5</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(38, 1, 1, 1, '', '', '<p>Tabel berikut adalah harga gula &nbsp;pasir ( per kg) dan susu cair (per liter) di beberapa took.</p>\r\n\r\n<table border=\"1\" cellspacing=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:52.25pt\">\r\n			<p>Toko</p>\r\n			</td>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Jenis Barang</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Harga</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Diskon</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan=\"2\" style=\"width:52.25pt\">\r\n			<p>P</p>\r\n			</td>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Gula Pasir</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 28.000,00</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 3.000,00</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Susu Cair</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 15.000,00</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 2.000,00</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan=\"2\" style=\"width:52.25pt\">\r\n			<p>Q</p>\r\n			</td>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Gula Pasir</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 30.000,00</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 2.000,00</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Susu Cair</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 13.000,00</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 1.000,00</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan=\"2\" style=\"width:52.25pt\">\r\n			<p>R</p>\r\n			</td>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Gula Pasir</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 35.000,00</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 1.000,00</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Susu Cair</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 15.000,00</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 2.000,00</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan=\"2\" style=\"width:52.25pt\">\r\n			<p>S</p>\r\n			</td>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Gula Pasir</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 30.000,00</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 3.000,00</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Susu Cair</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 15.000,00</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 1.000,00</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan=\"2\" style=\"width:52.25pt\">\r\n			<p>T</p>\r\n			</td>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Gula Pasir</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 26.000,00</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 3.000,00</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:75.0pt\">\r\n			<p>Susu Cair</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 15.000,00</p>\r\n			</td>\r\n			<td style=\"width:85.05pt\">\r\n			<p>Rp. 4.000,00</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Seseorang yang hanya memiliki uang sebesar Rp.35.000,00 dapat membeli 1 kg gula pasir dan 1 liter susu cair di toko...&nbsp;</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(39, 1, 1, 1, '', '', '<p>Berikut adalah daftar stok baju yang dijual online oleh Toko &nbsp;Anda.</p>\r\n\r\n<table border=\"1\" cellspacing=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan=\"2\" style=\"width:240.75pt\">\r\n			<p>Stok Baju</p>\r\n			</td>\r\n			<td style=\"width:52.6pt\">\r\n			<p>Jumlah Stok Awal</p>\r\n			</td>\r\n			<td style=\"width:48.65pt\">\r\n			<p>Jumlah Stok Akhir</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan=\"2\" style=\"width:100.95pt\">\r\n			<p>Atasan</p>\r\n			</td>\r\n			<td style=\"width:139.8pt\">\r\n			<p>Kaos</p>\r\n			</td>\r\n			<td style=\"width:52.6pt\">\r\n			<p>50</p>\r\n			</td>\r\n			<td style=\"width:48.65pt\">\r\n			<p>20</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:139.8pt\">\r\n			<p>Kemeja</p>\r\n			</td>\r\n			<td style=\"width:52.6pt\">\r\n			<p>40</p>\r\n			</td>\r\n			<td style=\"width:48.65pt\">\r\n			<p>10</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan=\"3\" style=\"width:100.95pt\">\r\n			<p>Bawahan</p>\r\n			</td>\r\n			<td style=\"width:139.8pt\">\r\n			<p>Rok</p>\r\n			</td>\r\n			<td style=\"width:52.6pt\">\r\n			<p>?</p>\r\n			</td>\r\n			<td style=\"width:48.65pt\">\r\n			<p>10</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:139.8pt\">\r\n			<p>Celana Panjang</p>\r\n			</td>\r\n			<td style=\"width:52.6pt\">\r\n			<p>30</p>\r\n			</td>\r\n			<td style=\"width:48.65pt\">\r\n			<p>15</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:139.8pt\">\r\n			<p>Celana Pendek</p>\r\n			</td>\r\n			<td style=\"width:52.6pt\">\r\n			<p>45</p>\r\n			</td>\r\n			<td style=\"width:48.65pt\">\r\n			<p>15</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Jika setiap baju atasan memberikan keuntungan Rp.15.000,00 dan baju bawahan memberikan keuntungan Rp.10.000,00, berapa jumlah stok awal Rok yang dimiliki Toko Anda jika total keuntungannya Rp. 1.600.000,00?&nbsp;</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(40, 1, 1, 1, '', '', 'Pembukaan UUD 1945 bagi bangsa dan negara Republik Indonesia antara lain mempunyai makna sebagai…', '#####', '#####', '#####', '#####', '#####', ''),
(41, 1, 1, 1, '', '', 'Filsafat Pancasila memiliki beragam manfaat bagi setiap bidang kehidupan bangsa dan negara Indonesia, salah satunya adalah...', '#####', '#####', '#####', '#####', '#####', ''),
(42, 1, 1, 1, '', '', 'Pancasila dikatakan sebagai suatu sistem falsafah bila…', '#####', '#####', '#####', '#####', '#####', ''),
(43, 1, 1, 1, '', '', 'Prinsip dasar yang menjiwai arti kebebasan dan batasan kebebasan yang bertanggung jawab adalah...', '#####', '#####', '#####', '#####', '#####', ''),
(44, 1, 1, 1, '', '', 'Manfaat filsafat Pancasila bagi setiap bidang kehidupan bangsa dan Negara Indonesia adalah…', '#####', '#####', '#####', '#####', '#####', ''),
(45, 1, 1, 1, '', '', 'PROPOSISI ><', '#####', '#####', '#####', '#####', '#####', ''),
(46, 1, 1, 1, '', '', 'DESKRIPSI =', '#####', '#####', '#####', '#####', '#####', ''),
(47, 1, 1, 1, '', '', 'EKUILIBRIUM = ', '#####', '#####', '#####', '#####', '#####', ''),
(48, 1, 1, 1, '', '', 'CERDAS >< ', '#####', '#####', '#####', '#####', '#####', ''),
(49, 1, 1, 1, '', '', 'MANDIRI >< ', '#####', '#####', '#####', '#####', '#####', ''),
(50, 1, 1, 1, '', '', 'KARATE : KAKI =', '#####', '#####', '#####', '#####', '#####', ''),
(51, 1, 1, 1, '', '', 'BAROMETER : TORICELU =', '#####', '#####', '#####', '#####', '#####', ''),
(52, 1, 1, 1, '', '', 'INSENTIF: PRESTASI = ', '#####', '#####', '#####', '#####', '#####', ''),
(53, 1, 1, 1, '', '', 'IKAN MAS : IKAN PAUS = ', '#####', '#####', '#####', '#####', '#####', ''),
(54, 1, 1, 1, '', '', 'PEKERJA: BONUS = ', '#####', '#####', '#####', '#####', '#####', ''),
(55, 1, 1, 1, '', '', 'Pil anti malaria sangat dibutuhkan di daerah Irian Jaya. ', '#####', '#####', '#####', '#####', '#####', ''),
(56, 1, 1, 1, '', '', 'Di Indonesia banyak lahan pertanian yang subur. ', '#####', '#####', '#####', '#####', '#####', ''),
(57, 1, 1, 1, '', '', 'Berhari-hari lamanya dia berjalan mondar-mandir keluar-masuk kantor. Dilihatnya banyak orang berdiri di depan loket. Ada yang sedang bercakap-cakap, tetapi tidak seorang pun yang tertawa-tawa, tanya-menanya, atau pandang-memandang. Kata ulang yang menyatakan berbalasan dalam paragraf di atas adalah ........ ', '#####', '#####', '#####', '#####', '#####', ''),
(58, 1, 1, 1, '', '', 'Kalimat yang menggunakan kata penghubung korelatif adalah ........', '#####', '#####', '#####', '#####', '#####', ''),
(59, 1, 1, 1, '', '', 'Kata penghubung antarklausa terdapat dalam kalimat ........', '#####', '#####', '#####', '#####', '#####', ''),
(60, 1, 1, 1, '', '', 'Persaingan ekonomi global', '#####', '#####', '#####', '#####', '#####', ''),
(61, 1, 1, 1, '', '', 'Bekerja keras akan mendatangkan hasil maksimal. ', '#####', '#####', '#####', '#####', '#####', ''),
(62, 1, 1, 1, '', '', 'Jika suatu rencana kerja terlihat rumit, maka', '#####', '#####', '#####', '#####', '#####', ''),
(63, 1, 1, 1, '', '', 'Saya sudah berusaha untuk memperbaiki kelemahan diri, tetapi belum juga menampakkan hasilnya. Sehingga saya... ', '#####', '#####', '#####', '#####', '#####', ''),
(64, 1, 1, 1, '', '', 'Saya mengajukan suatu usulan untuk atasan saya namun usulan tersebut menurut atasan saya kurang tepat. Sikap saya adalah...', '#####', '#####', '#####', '#####', '#####', ''),
(65, 1, 1, 1, '', '', 'Ayah sahabat anda mengalami serangan jantung dan masuk Rumah Sakit.', '#####', '#####', '#####', '#####', '#####', ''),
(66, 1, 1, 1, '', '', 'Saya diminta untuk lembur kerja sedangkan saya sudah berjanji kepada anaksayauntuk mengantarnya ke pesta ulang tahun sahabatnya. Sikap saya.', '#####', '#####', '#####', '#####', '#####', ''),
(67, 1, 1, 1, '', '', 'Penggunaan tanda tanya yang tepat dapat digunakan pada kalimat', '#####', '#####', '#####', '#####', '#####', ''),
(68, 1, 1, 1, '', '', 'Penggunaan huruf miring untuk ungkapan asing yang benar adalah ', '#####', '#####', '#####', '#####', '#####', ''),
(69, 1, 1, 1, '', '', 'Penulisan nama yang benar adalah ', '#####', '#####', '#####', '#####', '#####', ''),
(70, 1, 1, 1, '', '', '........ are food for huge numbers of small  animals that also live near the surface.', '#####', '#####', '#####', '#####', '#####', ''),
(71, 1, 1, 1, '', '', 'Paragraph three tells us about........', '#####', '#####', '#####', '#####', '#####', ''),
(72, 1, 1, 1, '', '', '..... a few million rupiahs, he went on a tour to Australia. ', '#####', '#####', '#####', '#####', '#####', ''),
(73, 1, 1, 1, '', '', '3  5  7  9……..', '#####', '#####', '#####', '#####', '#####', ''),
(74, 1, 1, 1, '', '', '1 48 4 24 9 12 16 6 ', '#####', '#####', '#####', '#####', '#####', ''),
(75, 1, 1, 1, '', '', '12 36 12 35 10 33 10 30 8 ', '#####', '#####', '#####', '#####', '#####', ''),
(76, 1, 1, 1, '', '', '21 40 22 39 24 37 27 34 31.... ', '#####', '#####', '#####', '#####', '#####', ''),
(77, 1, 1, 1, '', '', '17 20 18 21 18 23 19 26 19.... ', '#####', '#####', '#####', '#####', '#####', ''),
(78, 1, 1, 1, '', '', 'Sebuah tabung berisi air telah diisi sebanyak 3/8 dan daya tampungnya. Jika diisi sebanyak 5 liter lagi volume tabung menjadi setengahnya. Kapasitas volume tabung adalah .... ---------liter. ', '#####', '#####', '#####', '#####', '#####', ''),
(79, 1, 1, 1, '', '', 'Herman mengikuti tes kerja. Pada ujian tersebut Herman mendapatkan skor 87,5%. Jika Herman tidak dapat menjawab 5 buah pertanyaan, jumlah soal pada tes ujian adalah', '#####', '#####', '#####', '#####', '#####', ''),
(80, 1, 1, 1, '', '', 'Sebuah bak mandi berukuran panjang 84 cm, lebar 64 cm, dan tinggi 80 cm. Bak mandi tersebut diisi air hingga mencapai 4 cm dan atas. Volume air pada bak mandi adalah ... liter.', '#####', '#####', '#####', '#####', '#####', ''),
(81, 1, 1, 1, '', '', 'Dengan mobil, Chika akan berangkat dan kota A menuju kota B pada pukul 10.30 dengan kecepatan rata-rata 60 km/jam. Pada saat yang sama, Anggun juga mengendarai sebuah mobil dan kota B ke kota A dengan kecepatan rata-rata 80 km/jam. Jika jarak kedua kota 560 km, mereka akan bertemu pada pukul', '#####', '#####', '#####', '#####', '#####', ''),
(82, 1, 1, 1, '', '', 'Diketahui 1/a + 1/b  = 8 dan 1/a - 1/b =2  Nilai a x b adalah', '#####', '#####', '#####', '#####', '#####', ''),
(163, 1, 1, 1, '', '', 'FLAMBOYAN = ', '#####', '#####', '#####', '#####', '#####', ''),
(164, 1, 1, 1, '', '', 'VETER =', '#####', '#####', '#####', '#####', '#####', ''),
(165, 1, 1, 1, '', '', 'YAYI = ', '#####', '#####', '#####', '#####', '#####', ''),
(166, 1, 1, 1, '', '', 'Nadir ><', '#####', '#####', '#####', '#####', '#####', ''),
(167, 1, 1, 1, '', '', 'Artika >< ', '#####', '#####', '#####', '#####', '#####', ''),
(168, 1, 1, 1, '', '', 'EFISIEN = …', '#####', '#####', '#####', '#####', '#####', ''),
(169, 1, 1, 1, '', '', 'PARTIKELLIR = …', '#####', '#####', '#####', '#####', '#####', ''),
(170, 1, 1, 1, '', '', 'SINDIKASI = ....   ', '#####', '#####', '#####', '#####', '#####', ''),
(171, 1, 1, 1, '', '', 'KONSIDERAN = …', '#####', '#####', '#####', '#####', '#####', ''),
(172, 1, 1, 1, '', '', 'ASKETIK = …', '#####', '#####', '#####', '#####', '#####', ''),
(173, 1, 1, 1, '', '', 'PROGRESIF >< …  ', '#####', '#####', '#####', '#####', '#####', ''),
(174, 1, 1, 1, '', '', 'ANTIPATI >< …', '#####', '#####', '#####', '#####', '#####', ''),
(175, 1, 1, 1, '', '', 'ORTODOKS >< ....   ', '#####', '#####', '#####', '#####', '#####', ''),
(176, 1, 1, 1, '', '', 'PARADOKSAL >< ....   ', '#####', '#####', '#####', '#####', '#####', ''),
(177, 1, 1, 1, '', '', 'MAKAR >< ....   ', '#####', '#####', '#####', '#####', '#####', ''),
(178, 1, 1, 1, '', '', 'Tragedi gelombang tsunami yang terjadi di Aceh adalah akibat dari ', '#####', '#####', '#####', '#####', '#####', ''),
(179, 1, 1, 1, '', '', 'Organisasi PBB yang mengurusi permasalahan yang berkaitan dengan anak-anak adalah ', '#####', '#####', '#####', '#####', '#####', ''),
(180, 1, 1, 1, '', '', 'Negara yang merupakan negara terluas di Benua Afrika adalah', '#####', '#####', '#####', '#####', '#####', ''),
(181, 1, 1, 1, '', '', 'Jumlah pemain pada permainan olahraga futsal adalah', '#####', '#####', '#####', '#####', '#####', ''),
(182, 1, 1, 1, '', '', 'Bagian-bagian yang terdapat dalam surat pribadi adalah, kecuali ', '#####', '#####', '#####', '#####', '#####', ''),
(183, 1, 1, 1, '', '', 'Ayo, cepat! Kerjakan pekerjaan kalian. Kalimat tersebut merupakan bentuk kalimat ', '#####', '#####', '#####', '#####', '#####', ''),
(184, 1, 1, 1, '', '', 'Jika kedua orang tua bekerja dan tidak bisa mengambil cuti bersama dengan liburan anak-anak, mereka  harus putar otak untuk mengisi waktu luang anak agar tidak mubazir.  Apa makna putar otak dalam kalimat tersebut? ', '#####', '#####', '#####', '#####', '#####', ''),
(185, 1, 1, 1, '', '', 'Penulisan nama orang dengan gelar pendidikan yang tepat adalah ', '#####', '#####', '#####', '#####', '#####', ''),
(186, 1, 1, 1, '', '', 'Tentukan penulisan judul karangan yang sesuai dengan pedoman umum EYD! ', '#####', '#####', '#####', '#####', '#####', ''),
(187, 1, 1, 1, '', '', '(1) Dia menyesalkan perbuatan kami. (2) Dia meminta kami. (3) kami berjanji tidak akan mengulangi kesalahan yang sama. (4) kesalahan merugikan nama baik keluarga.  Kalimat-kalimat tunggal di atas akan menjadi kalimat majemuk yang tepat jika disusun menjadi', '#####', '#####', '#####', '#####', '#####', ''),
(188, 1, 1, 1, '', '', '‘Several hotels in this region are closing down.’  ‘That’s because tourism itself — -----------since last year.’', '#####', '#####', '#####', '#####', '#####', ''),
(189, 1, 1, 1, '', '', 'Which statement is True according to the text?', '#####', '#####', '#####', '#####', '#####', ''),
(190, 1, 1, 1, '', '', 'If I ..... you, I would go home', '#####', '#####', '#####', '#####', '#####', ''),
(191, 1, 1, 1, '', '', 'He asked me whether he ... my motorcycle to go to the party. ', '#####', '#####', '#####', '#####', '#####', ''),
(192, 1, 1, 1, '', '', 'Raja, the national foot ball champion, ... foot ball since he was nine.', '#####', '#####', '#####', '#####', '#####', ''),
(193, 1, 1, 1, '', '', 'When did you realize you had lost your purse\nWhen I ..., one to pay the conductor ', '#####', '#####', '#####', '#####', '#####', ''),
(194, 1, 1, 1, '', '', 'If he had not been promoted, had not have quit his job. The underline words mean...', '#####', '#####', '#####', '#####', '#####', ''),
(195, 1, 1, 1, '', '', 'Pilihlah satu kata yang tidak termasuk kelompok atau tidak mempunyai persamaan tertentu dibandingkan kata-kata lainnya. ', '#####', '#####', '#####', '#####', '#####', ''),
(196, 1, 1, 1, '', '', 'Pilihlah satu kata yang tidak termasuk kelompok atau tidak mempunyai persamaan tertentu dibandingkan kata-kata lainnya. ', '#####', '#####', '#####', '#####', '#####', ''),
(197, 1, 1, 1, '', '', 'Pilihlah satu kata yang tidak termasuk kelompok atau tidak mempunyai persamaan tertentu dibandingkan kata-kata lainnya. ', '#####', '#####', '#####', '#####', '#####', ''),
(198, 1, 1, 1, '', '', 'Pilihlah satu kata yang tidak termasuk kelompok atau tidak mempunyai persamaan tertentu dibandingkan kata-kata lainnya.', '#####', '#####', '#####', '#####', '#####', ''),
(199, 1, 1, 1, '', '', 'Pilihlah satu kata yang tidak termasuk kelompok atau tidak mempunyai persamaan tertentu dibandingkan kata-kata lainnya. ', '#####', '#####', '#####', '#####', '#####', ''),
(200, 1, 1, 1, '', '', 'Rata-rata dan 50 bilangan adalah 8,4. Jika bilangan tersebut dikalikan setengah, rata-ratanya menjadi', '#####', '#####', '#####', '#####', '#####', ''),
(201, 1, 1, 1, '', '', 'Seorang calon legislatif melakukan kampanye pada lima kecamatan yaitu J, K, L M, dan N dengan ketentuan sebagai berikut: (1) Ia dapat berkunjung ke kecamatan Mjika telah ke L dan N. (2) Ia tidak bisa mengunjungi kecamatan N sebelum mengunjungi kecamatan J. (3) Kecamatan kedua yang harus dikunjungi adalah K. Kecamatan yang pertama harus dikunjungi adalah  ', '#####', '#####', '#####', '#####', '#####', ''),
(202, 1, 1, 1, '', '', 'Seorang calon legislatif melakukan kampanye pada lima kecamatan yaitu J, K, L M, dan N dengan ketentuan sebagai berikut (1) Ia dapat berkunjung ke kecamatan Mjika telah ke L dan N. (2) Ia tidak bisa mengunjungi kecamatan N sebelum mengunjungi kecamatan J. (3) Kecamatan kedua yang harus dikunjungi adalah K. Dua kecamatan dapat dikunjungi setelah kecamatan N adalah kecamatan', '#####', '#####', '#####', '#####', '#####', ''),
(203, 1, 1, 1, '', '', 'Seorang calon legislatif melakukan kampanye pada lima kecamatan yaitu J, K, L M, dan N dengan ketentuan sebagai berikut:  (1) Ia dapat berkunjung ke kecamatan Mjika telah ke L dan N. (2) Ia tidak bisa mengunjungi kecamatan N sebelum mengunjungi kecamatan J. (3) Kecamatan kedua yang harus dikunjungi adalah K. Rencana kunjungan yang sebaiknya dipilih agar lima kecamatan dapat dikunjungi adalah', '#####', '#####', '#####', '#####', '#####', ''),
(204, 1, 1, 1, '', '', 'Seorang karyawan mengatur 6 ruang kerja untuk 6 staf dengan urutan nomor ruang 1 sampai 6 dengan aturan sebagai berikut: (1) Bu Rati sering bercakap-cakap yang suaranya terdengar keras ke ruang sebelahnya. (2) Pak Mara dan Pak Bono ingin berdekatan agar dapat berkoordinasi. (3) Bu Heni meminta ruang nomor 5 yang berjendela lebar. (4) Pak Dedi tidak suka pekerjaannya terganggu oleh suara-suara. (5) Pak Tasman, Pak Mara, dan Pak Dedi adalah perokok. (6) Bu Heni alergi dengan asap rokok. Tiga karyawan perokok seharusnya ditempatkan di ruang ', '#####', '#####', '#####', '#####', '#####', ''),
(205, 1, 1, 1, '', '', 'Seorang karyawan mengatur 6 ruang kerja untuk 6 staf dengan urutan nomor ruang 1 sampai 6 dengan aturan sebagai berikut: (1) Ru Rati sering bercakap-cakap yang suaranya terdengar keras ke ruang sebelahnya. (2) Pak Mara dan Pak Bono ingin berdekatan agar dapat berkoordinasi. (3) Bu Heni meminta ruang nomor 5 yang berjendela lebar. (4) Pak Dedi tidak suka pekerjaannya terganggu oleh suara-suara. (5) Pak Tasman, Pak Mara, dan Pak Dedi adalah perokok. (6) Bu Heni alergi dengan asap rokok. Ruang kerja yang paling jauh dan ruang kerja Pak Bono adalah ruang kerja', '#####', '#####', '#####', '#####', '#####', ''),
(206, 1, 1, 1, '', '', 'Seorang karyawan mengatur 6 ruang kerja untuk 6 staf dengan urutan nomor ruang 1 sampai 6 dengan aturan sebagai berikut: (1) Bu Rati sering bercakap-cakap yang suaranya terdengar keras ke ruang sebelahnya. (2) Pak Mara dan Pak Bono ingin berdekatan agar dapat berkoordinasi. (3) Bu Heni meminta ruang nomor 5 yang berjendela lebar. (4) Pak Dedi tidak suka pekerjaannya terganggu oleh suara-suara. (5) Pak Tasman, Pak Mara, dan Pak Dedi adalah perokok. (6) Bu Heni alergi dengan asap rokok. Ruang kerja yang paling cocok untuk Pak Mara adalah ruang nomor', '#####', '#####', '#####', '#####', '#####', ''),
(207, 1, 1, 1, '', '', 'Penulisan gabungan kata yang tepat terdapat dalam kalimat ........', '#####', '#####', '#####', '#####', '#####', ''),
(208, 1, 1, 1, '', '', 'Kalimat yang menggunakan kata ulang yang tepat adalah ........', '#####', '#####', '#####', '#####', '#####', ''),
(209, 1, 1, 1, '', '', 'Kata penghubung syarat hasil terdapat pada kalimat ........', '#####', '#####', '#####', '#####', '#####', ''),
(210, 1, 1, 1, '', '', 'Selagi jaya pejabat itu dikerumuni orang-orang yang mengaku teman atau saudara. Sekarang ia sudah kehilangan jabatannya. Hilanglah pula orang-orang yang mengaku teman dan saudaranya. Peribahasa yang sesuai dengan ilustrasi di atas adalah ........ ', '#####', '#####', '#####', '#####', '#####', ''),
(211, 1, 1, 1, '', '', 'Saya angkat topi atas prestasi yang telah kauraih untuk memajukan sekolah kita. Makna ungkapan dalam kalimat tersebut sama dengan makna ungkapan yang tersaji dalam kalimat ........ ', '#####', '#####', '#####', '#####', '#####', ''),
(212, 1, 1, 1, '', '', 'Kalimat yang menggunakan majas litotes adalah ........', '#####', '#####', '#####', '#####', '#####', ''),
(213, 1, 1, 1, '', '', 'Pasangan kalimat yang menggunakan kata berpolisemi dengan tepat terdapat pada ........ ', '#####', '#####', '#####', '#####', '#####', ''),
(214, 1, 1, 1, '', '', 'Jika menjadi seorang pemimpin, Ridwan harus jujur \ndan dapat dipercaya. Saat Ini Ridwan adalah seorang pemimpin. Simpulan yang tepat adalah…', '#####', '#####', '#####', '#####', '#####', ''),
(215, 1, 1, 1, '', '', 'Jika guru Matematika menambah jam pelajaran di hari\nRabu, nilai siswa banyak yang meningkat. Jika nilai siswa banyak yang meningkat, siswa dapat mengikuti kegiatan ekstrakurikuler. Simpulan yang tepat adalah…', '#####', '#####', '#####', '#####', '#####', ''),
(216, 1, 1, 1, '', '', 'Jika nilai tukar dollar AS menguat dibandingkan Rupiah,komoditas asal Indonesia membanjiri pasar dengan harga yang kompetitif. Jika Bank Sentral Amerika menaikan tingkat suku bunga, Indonesia berpeluang meningkatkan nilai ekspor ke Amerika. Saat ini komoditas asal Indonesia tidak membanjiri pasar  dengan harga kompetitif atau Indonesia tidak berpeluang meningkatkan nilai ekspor ke Amerika. Simpulan yang tepat adalah...', '#####', '#####', '#####', '#####', '#####', ''),
(217, 1, 1, 1, '', '', 'Semua dokter berjas putih membawa stetoskop. Tidak ada orang yang membawa stetoskop bekerja di puskesmas. Berdasarkan dua pernyataan di atas, simpulan …', '#####', '#####', '#####', '#####', '#####', ''),
(218, 1, 1, 1, '', '', 'Tidak ada orang sukses yang selalu mengeluh. Beberapa orang yang mengeluh adalah orang yang takut mencoba. Berdasarkan dua pernyataan…', '#####', '#####', '#####', '#####', '#####', ''),
(219, 1, 1, 1, '', '', 'Narapidana : Penjara =… ', '#####', '#####', '#####', '#####', '#####', ''),
(220, 1, 1, 1, '', '', 'Mata : Wajah =…', '#####', '#####', '#####', '#####', '#####', ''),
(221, 1, 1, 1, '', '', 'Memar : Benturan =…', '#####', '#####', '#####', '#####', '#####', ''),
(222, 1, 1, 1, '', '', 'Di suatu sekolah dasar, Amir dan Beni mengajar Bahasa Indonesiadan Bahasa Inggris. Cita dan Dika mengajar Bahasa Inggris dan IPS. Dika mengajar Matematika dan Bahasa Indonesia. Eni dan Beni mengajar Kesenian. Mata pelajaran apa yang diajar oleh lebih dari tiga guru?', '#####', '#####', '#####', '#####', '#####', ''),
(223, 1, 1, 1, '', '', 'Di sebuah asrama, satu kamar diisi delapan siswa. Tempat tidu Ari, Ani, Ati, dan Ami berturut-turut di sisi yang sama. Di sisi yang lain berturut-turut adalah tempat tidur Ina, Ira, Ita, dan Ida. Tempat Tidur Ari dan Ina berhadapan.\nJika Ida ingin bertukar dengan Ati, dan Ami bertukar dengan Ita, tempat tidur siapakah yang sekarang terletak di hadapan ida?', '#####', '#####', '#####', '#####', '#####', ''),
(224, 1, 1, 1, '', '', 'Seno lebih muda dari Deni dan Ani, sedangkan Ani lebih muda dari Deni. Kiki lebih tua dari Deni dan Cepi. Cepi Lebih muda dari Ani dan Lebih tua dari Seno. \nUrutkan kelima anak tersebut dari yang tertua adalah…', '#####', '#####', '#####', '#####', '#####', ''),
(225, 1, 1, 1, '', '', '5, 8, 14, 26, 50, …', '#####', '#####', '#####', '#####', '#####', ''),
(226, 1, 1, 1, '', '', '4, 7, 14, 17, …, 37, 74 ', '#####', '#####', '#####', '#####', '#####', ''),
(227, 1, 1, 1, '', '', '6, 3, 6, 18, 9, 12, 36, …', '#####', '#####', '#####', '#####', '#####', ''),
(228, 1, 1, 1, '', '', '4, 2, 8, 32, …, 22, 88, 44', '#####', '#####', '#####', '#####', '#####', ''),
(229, 1, 1, 1, '', '', '<p>The view that women are better parents than men has shown it self to be true throughout history. This is not to stay that men are not of importance in child-rearing: indeed, they are most necessary if children are to appreciate fully the roles of both sexes. But women have proven themselves superior parents as a result of their conditioning, their less aggressive nature and their generally better communication skills.<br />\r\n<br />\r\nFrom the time they are little girls, females learn about nurturing. First with dolls and later perhaps with younger brother and sisters, girls are given the role of career. Girls see their mothers in the same roles and so it natural that they identify this as a female activity. Boys, in contrast, learn competition far removed from what it means to nurture. Whike boys may dream of adventures, girl&rsquo;s conditioning means they tend to see the future in terms of raising families. Girls also appear to be less aggressive than boys. In adulthood, it is men, not women, who prove to be aggressors in crime and in war. Obviously, in raising children, a more patient, gentle manner is preferable to a more aggressive one. Although there certainly exist gentle men and aggressive women, by and large, female are less likely to resort to violence in attempting to solve problem.</p>\r\n\r\n<p>Finally, women tend to be better communicators than men. This is shown intelligence tests, where females, on average, do better in verbal communication than males. Of course, communication is of it most importance in rearing children, as children tend to learn from the adopt the communication styles of the parents. Thus, while it is all very well to suggest a greater role for men am raising children let us not forget that women are generally better suited to the parenting role.&nbsp;<br />\r\n<br />\r\nThe main information of the text is about :</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(230, 1, 1, 1, '', '', '<p>The view that women are better parents than men has shown it self to be true throughout history. This is not to stay that men are not of importance in child-rearing: indeed, they are most necessary if children are to appreciate fully the roles of both sexes. But women have proven themselves superior parents as a result of their conditioning, their less aggressive nature and their generally better communication skills.</p>\r\n\r\n<p>From the time they are little girls, females learn about nurturing. First with dolls and later perhaps with younger brother and sisters, girls are given the role of career. Girls see their mothers in the same roles and so it natural that they identify this as a female activity. Boys, in contrast, learn competition far removed from what it means to nurture. Whike boys may dream of adventures, girl&rsquo;s conditioning means they tend to see the future in terms of raising families. Girls also appear to be less aggressive than boys. In adulthood, it is men, not women, who prove to be aggressors in crime and in war. Obviously, in raising children, a more patient, gentle manner is preferable to a more aggressive one. Although there certainly exist gentle men and aggressive women, by and large, female are less likely to resort to violence in attempting to solve problem.</p>\r\n\r\n<p>Finally, women tend to be better communicators than men. This is shown intelligence tests, where females, on average, do better in verbal communication than males. Of course, communication is of it most importance in rearing children, as children tend to learn from the adopt the communication styles of the parents. Thus, while it is all very well to suggest a greater role for men am raising children let us not forget that women are generally better suited to the parenting role.&nbsp;<br />\r\n<br />\r\nAs parents, women in general play a more important role than men because they are :</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(231, 1, 1, 1, '', '', '<p>The view that women are better parents than men has shown it self to be true throughout history. This is not to stay that men are not of importance in child-rearing: indeed, they are most necessary if children are to appreciate fully the roles of both sexes. But women have proven themselves superior parents as a result of their conditioning, their less aggressive nature and their generally better communication skills.</p>\r\n\r\n<p>From the time they are little girls, females learn about nurturing. First with dolls and later perhaps with younger brother and sisters, girls are given the role of career. Girls see their mothers in the same roles and so it natural that they identify this as a female activity. Boys, in contrast, learn competition far removed from what it means to nurture. Whike boys may dream of adventures, girl&rsquo;s conditioning means they tend to see the future in terms of raising families. Girls also appear to be less aggressive than boys. In adulthood, it is men, not women, who prove to be aggressors in crime and in war. Obviously, in raising children, a more patient, gentle manner is preferable to a more aggressive one. Although there certainly exist gentle men and aggressive women, by and large, female are less likely to resort to violence in attempting to solve problem.</p>\r\n\r\n<p>Finally, women tend to be better communicators than men. This is shown intelligence tests, where females, on average, do better in verbal communication than males. Of course, communication is of it most importance in rearing children, as children tend to learn from the adopt the communication styles of the parents. Thus, while it is all very well to suggest a greater role for men am raising children let us not forget that women are generally better suited to the parenting role.&nbsp;<br />\r\n<br />\r\nMost women are good mothers because they :&nbsp;</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(232, 1, 1, 1, '', '', '<p>The view that women are better parents than men has shown it self to be true throughout history. This is not to stay that men are not of importance in child-rearing: indeed, they are most necessary if children are to appreciate fully the roles of both sexes. But women have proven themselves superior parents as a result of their conditioning, their less aggressive nature and their generally better communication skills.</p>\r\n\r\n<p>From the time they are little girls, females learn about nurturing. First with dolls and later perhaps with younger brother and sisters, girls are given the role of career. Girls see their mothers in the same roles and so it natural that they identify this as a female activity. Boys, in contrast, learn competition far removed from what it means to nurture. Whike boys may dream of adventures, girl&rsquo;s conditioning means they tend to see the future in terms of raising families. Girls also appear to be less aggressive than boys. In adulthood, it is men, not women, who prove to be aggressors in crime and in war. Obviously, in raising children, a more patient, gentle manner is preferable to a more aggressive one. Although there certainly exist gentle men and aggressive women, by and large, female are less likely to resort to violence in attempting to solve problem.</p>\r\n\r\n<p>Finally, women tend to be better communicators than men. This is shown intelligence tests, where females, on average, do better in verbal communication than males. Of course, communication is of it most importance in rearing children, as children tend to learn from the adopt the communication styles of the parents. Thus, while it is all very well to suggest a greater role for men am raising children let us not forget that women are generally better suited to the parenting role.&nbsp;<br />\r\n<br />\r\n<br />\r\n<br />\r\nThe following are the general characteristics of men, except :</p>\r\n', '#####', '#####', '#####', '#####', '#####', ''),
(233, 1, 1, 1, '', '', 'Toleransi dalam kehidupan antar umat beragama berarti…', '#####opsi A.1', '#####opsi B.1', '#####opsi C.1', '#####opsi D.1', '#####opsi E.1', 'A'),
(234, 1, 1, 1, '', '', 'Berikut ini termasuk Empat pilar kebangsaan kecuali :', '#####opsi A.1', '#####opsi B.1', '#####opsi C.1', '#####opsi D.1', '#####opsi E.1', 'A'),
(235, 1, 1, 1, '', '', 'Pancasila sebagai ideologi terbuka haruslah bersifat dinamis, hal ini mengandung pengertian bahwa Pancasila mampu …', '#####', '#####', '#####', '#####', '#####', ''),
(236, 1, 1, 1, '', '', 'Batang tubuh UUD 1945, dalam hubungannya dengan pembukaan, seperti ditegaskan dalam penjelasan pada hakikatnya merupakan……', '#####', '#####', '#####', '#####', '#####', ''),
(237, 1, 1, 1, '', '', 'Presiden ialah penyelenggara pemerintah Negara yang tertinggi dibawahnya…..', '#####', '#####', '#####', '#####', '#####', ''),
(238, 1, 1, 1, '', '', 'DIKOTOMI =', '#####', '#####', '#####', '#####', '#####', ''),
(239, 1, 1, 1, '', '', 'Anak perempuan terhadap ayah seperti kemenakan perempuan terhadap….', '#####', '#####', '#####', '#####', '#####', ''),
(240, 1, 1, 1, '', '', 'GRASI =', '#####', '#####', '#####', '#####', '#####', ''),
(241, 1, 1, 1, '', '', '“bensin terhadap mobil seperti makanan terhadap ….”', '#####', '#####', '#####', '#####', '#####', ''),
(242, 1, 1, 1, '', '', 'ESTIMASI ><', '#####', '#####', '#####', '#####', '#####', ''),
(243, 1, 1, 1, '', '', 'KONSTAN ><', '#####', '#####', '#####', '#####', '#####', ''),
(244, 1, 1, 1, '', '', 'PETANI : CANGKUL = ', '#####', '#####', '#####', '#####', '#####', ''),
(245, 1, 1, 1, '', '', 'KENDARAAN : MOBIL = ', '#####', '#####', '#####', '#####', '#####', ''),
(246, 1, 1, 1, '', '', 'TUKANG : GERGAJI : PALU = ', '#####', '#####', '#####', '#####', '#####', ''),
(247, 1, 1, 1, '', '', 'ULAT : KEPOMPONG : KUPU KUPU =', '#####', '#####', '#####', '#####', '#####', ''),
(248, 1, 1, 1, '', '', 'Paragraf penutup pada surat berikut ini sesuai dengan kaidah kalimat baku bahasa indonesia, Kecuali…..', '#####', '#####', '#####', '#####', '#####', ''),
(249, 1, 1, 1, '', '', 'Inti frase “empat ekor ikan hiu raksasa\" adalah', '#####', '#####', '#####', '#####', '#####', ''),
(250, 1, 1, 1, '', '', 'Dari deretan kata di bawah ini yang sepola dengan kelompok kata ikan asin adalah …..', '#####', '#####', '#####', '#####', '#####', ''),
(251, 1, 1, 1, '', '', 'Semua kata berawalan ber – dalam kalimat dibawah ini menyatakan makna melakukan tindakan, kecuali….', '#####', '#####', '#####', '#####', '#####', ''),
(252, 1, 1, 1, '', '', 'Penyerbuan tentara Rusia ke Afganistan dikecam negara negara Arab. Pernyataan tersebut dapat digambarkan dengan kata :', '#####', '#####', '#####', '#####', '#####', ''),
(253, 1, 1, 1, '', '', 'Pemakaian tanda baca yang sesuai dengan EYD adalah…..', '#####', '#####', '#####', '#####', '#####', ''),
(254, 1, 1, 1, '', '', 'Diantara kalimat dibawah ini yang termasuk kalimat tak sempurna adalah….', '#####', '#####', '#####', '#####', '#####', ''),
(255, 1, 1, 1, '', '', 'Sejarah mencatat negara Rusia, Amerika Serikat dan China sudah pernah “mendarat” ke bulan karena penemuan teknologi yang maju, sementara Indonesia masih jauh tertinggal dari kemajuan teknologi. Sebagai warga negara sikap anda…..', '#####', '#####', '#####', '#####', '#####', ''),
(256, 1, 1, 1, '', '', 'Petinju kebanggaan Indonesia Chris John gagal mempertahankan gelar juara dunia kelas bulu WBA setelah kalah TKO saat menghadapi juara dunia kelas IBO asal Afrika Selatan Simpiwe Vetyeka, Jumat (6/12/2013). Sebagai warga negara Indonesia tanggapan anda adalah....', '#####', '#####', '#####', '#####', '#####', ''),
(257, 1, 1, 1, '', '', 'Te Hasani adalah mahasiswa berprestasi di kampusnya. Setiap ujian semester dia memperoleh nilai tertinggi mengalahkan mahasiswa lainnya. Minggu depan Ujian akhir semester terakhir bagi Joko Tingkir yang kemudian akan menamatkan perkuliahannya dari Universitas Ilmu Sakti. Untuk menghadapi ujian semester minggu depan, tanggapan anda:……', '#####', '#####', '#####', '#####', '#####', ''),
(258, 1, 1, 1, '', '', 'Jika anda seorang perawat dalam sebuah rumah sakit. Saat menerima pasien baru yang pertama anda lakukan adalah….', '#####', '#####', '#####', '#####', '#####', ''),
(259, 1, 1, 1, '', '', 'Pak Herlambang adalah seorang guru senior dengan umur 70 tahun masih mengajar disebuah Sekolah Menegah Pertama (SMP) swasta dimana anak didiknya adalah para remaja. Dalam mengajar sebaiknya Pak Herlambang…..', '#####', '#####', '#####', '#####', '#####', ''),
(260, 1, 1, 1, '', '', 'Penggunaan tanda tanya yang tepat dapat digunakan pada kalimat', '#####', '#####', '#####', '#####', '#####', ''),
(261, 1, 1, 1, '', '', 'Penggunaan huruf miring untuk ungkapan asing yang benar adalah ', '#####', '#####', '#####', '#####', '#####', ''),
(262, 1, 1, 1, '', '', 'Penulisan nama yang benar adalah ', '#####', '#####', '#####', '#####', '#####', ''),
(263, 1, 1, 1, '', '', 'My Mother opened the door quietly in order not to wake up……..', '#####', '#####', '#####', '#####', '#####', ''),
(264, 1, 1, 1, '', '', 'He left this town without……any words.', '#####', '#####', '#####', '#####', '#####', ''),
(265, 1, 1, 1, '', '', 'It is possible that he will come soon. We can also say. He ……. come soon.', '#####', '#####', '#####', '#####', '#####', ''),
(266, 1, 1, 1, '', '', '3, 6, 9, 12, 15, 18, 21, …., ….. ', '#####opsi A.1', '#####opsi B.1', '#####opsi C.1', '#####opsi D.1', '#####opsi E.1', 'A'),
(267, 1, 1, 1, '', '', '50 55 52 57 54 59 … ... ', '#####', '#####', '#####', '#####', '#####', ''),
(268, 1, 1, 1, '', '', '26 27 29 32 … … 40 38 35 31 26', '#####', '#####', '#####', '#####', '#####', ''),
(269, 1, 1, 1, '', '', '2X = 64 dan 3Y = 81, maka :', '#####', '#####', '#####', '#####', '#####', ''),
(270, 1, 1, 1, '', '', '5 x a x 14 = 6 x 7 x 4. Nilai a yang tepat adalah ……', '#####', '#####', '#####', '#####', '#####', ''),
(271, 1, 1, 1, '', '', 'Tujuh ratus adalah berapa persen dari 150 ? ', '#####', '#####', '#####', '#####', '#####', ''),
(272, 1, 1, 1, '', '', 'Ali adalah kakak Hasan, 4 tahun lebih tua. Sinta adalah kakak Ali dan berbeda 3 tahun. Berapakah usia Sinta, jika saat ini Hasan baru saja merayakan ulang tahun yang ke-21 ?', '#####', '#####', '#####', '#####', '#####', ''),
(273, 1, 1, 1, '', '', 'Sebuah pesawat terbang dapat menempuh jarah 10 km dalam tempo 40 detik. Kecepatan pesawat terbang tersebut adalah …….', '#####', '#####', '#####', '#####', '#####', ''),
(274, 1, 1, 1, '', '', 'Seorang pedagang menjual jambu dengan harga Rp 15.000/kg. Di dalam tokonya terdapat 6 dus dan di setiap dus berisi 5 kg jambu. Dua bekas tempat jambu itu masih bisa dijual lagi dengan harga Rp 2.000/dus. Berapakah uang hasil penjualan seluruh jambu dan dus tersebut ?', '#####', '#####', '#####', '#####', '#####', ''),
(275, 1, 1, 1, '', '', 'Harga sewa mobil di sebuah persewaan mobil adalah Rp 375.000/24 jam pertama, sedangkan kelebihan dua jam berikutnya dikenai denda Rp 25.000. Jika suatu hari, Abdul menyewa sebuah mobil dan diharuskan membayar Rp 625.000, berapa lama Abdul menyewa mobil tersebut ?', '#####', '#####', '#####', '#####', '#####', ''),
(276, 1, 1, 1, '', '', 'Tidak ada dua perhiasan berlian yang mempunyai kilau yang sama. Cincin X dan Y terbuat dari berlian. Simpulan yang tepat tentang pernyataan-pernyataan di atas adalah …….', '#####', '#####', '#####', '#####', '#####', ''),
(277, 1, 1, 1, '', '', 'Hanya barang-barang dari plastik yang dijual di toko Kurnia. Tekstil terbuat dari bahan dasar kapas. Kesimpulan :', '#####', '#####', '#####', '#####', '#####', ''),
(278, 1, 1, 1, '', '', 'Lima orang pedagang asongan menghitung hasil penjualan dalam satu hari. Pedagang III lebih banyak menjual dari pedagang IV, tetapi tidak melebihi pedagang I. Penjualan pedagang II tidak melebihi pedagang V dan melebihi pedagang I. Pedagang mana yang hasil penjualannya paling banyak ?', '#####', '#####', '#####', '#####', '#####', ''),
(279, 1, 1, 1, '', '', 'Intan sekarang berusia 12 tahun. Sedangkan umur Anto dua kali lebih tua daripada umur Badu. Umur Badu tiga tahun lebih tua daripada umur Nida. Jika umur Intan lima tahun lebih tua daripada umur Sari yang setahun lebih muda daripada Anto, maka urutan umur mereka dari yang termuda ke yang tertua adalah ... ', '#####', '#####', '#####', '#####', '#####', ''),
(280, 1, 1, 1, '', '', 'Noda hitam akan tampak jelas pada pakaian putih. Noda putih akan tampak jelas pada pakaian hitam. Deterjen dapat digunakan untuk menghilangkan noda. Simpulan yang tepat adalah …….', '#####', '#####', '#####', '#####', '#####', ''),
(281, 1, 1, 1, '', '', 'Negara Indonesia berdasar atas hukum (rectssataat) tidak berdasar pada…….', '#####opsi A.1', '#####opsi B.1', '#####opsi C.1', '#####opsi D.1', '#####opsi E.1', 'A'),
(282, 1, 1, 1, '', '', 'Pancasila dikatakan sebagai suatu sistem falsafah bila…', '#####', '#####', '#####', '#####', '#####', ''),
(283, 1, 1, 1, '', '', 'Makna semboyan Bhineka Tunggal Ika bagi bangsa Indonesia yang paling mendasar adalah ……..', '#####', '#####', '#####', '#####', '#####', ''),
(284, 1, 1, 1, '', '', 'Bagi bangsa Indonesia Pancasila secara singkat dari asal kata diartikan sebagai …….', '#####', '#####', '#####', '#####', '#####', ''),
(285, 1, 1, 1, '', '', 'Pembukaan UUD 1945 bagi bangsa dan negara Republik Indonesia antara lain mempunyai makna sebagai…', '#####', '#####', '#####', '#####', '#####', ''),
(286, 1, 1, 1, '', '', 'DESKRIPSI =', '#####', '#####', '#####', '#####', '#####', ''),
(287, 1, 1, 1, '', '', 'INTIMIDASI =', '#####', '#####', '#####', '#####', '#####', ''),
(288, 1, 1, 1, '', '', 'CERDAS >< ', '#####', '#####', '#####', '#####', '#####', ''),
(289, 1, 1, 1, '', '', 'MEMBAUR >< ', '#####', '#####', '#####', '#####', '#####', ''),
(290, 1, 1, 1, '', '', 'ORISINIL ><', '#####', '#####', '#####', '#####', '#####', ''),
(291, 1, 1, 1, '', '', 'BAROMETER : TORRICELLI =', '#####', '#####', '#####', '#####', '#####', ''),
(292, 1, 1, 1, '', '', 'SEMINAR : SARJANA = ', '#####', '#####', '#####', '#####', '#####', ''),
(293, 1, 1, 1, '', '', 'IKAN MAS : IKAN PAUS = ', '#####', '#####', '#####', '#####', '#####', ''),
(294, 1, 1, 1, '', '', 'PILOT : PESAWAT : PRAMUGARI =', '#####', '#####', '#####', '#####', '#####', ''),
(295, 1, 1, 1, '', '', 'Terompet terhadap bermain sama dengan buku terhadap….', '#####', '#####', '#####', '#####', '#####', ''),
(296, 1, 1, 1, '', '', 'Berhari-hari lamanya dia berjalan mondar-mandir keluar-masuk kantor. Dilihatnya banyak orang berdiri di depan loket. Ada yang sedang bercakap-cakap, tetapi tidak seorang pun yang tertawa-tawa, tanya-menanya, atau pandang-memandang. Kata ulang yang menyatakan berbalasan dalam paragraf di atas adalah ........ ', '#####', '#####', '#####', '#####', '#####', ''),
(297, 1, 1, 1, '', '', 'Pasangan di bawah ini bila diucapkan secara benar merupakan homofon, kecuali …..', '#####', '#####', '#####', '#####', '#####', ''),
(298, 1, 1, 1, '', '', 'Kata penghubung antarklausa terdapat dalam kalimat ........', '#####', '#####', '#####', '#####', '#####', ''),
(299, 1, 1, 1, '', '', 'Pola kalimat Masyarakat tidak perlu khawatir akan persediaan sembilan bahan pokok sama dengan pola pada kalimat …….', '#####', '#####', '#####', '#####', '#####', ''),
(300, 1, 1, 1, '', '', 'Penulisan partikel “per” yang benar adalah ……..', '#####', '#####', '#####', '#####', '#####', ''),
(301, 1, 1, 1, '', '', 'Kata penghubung syarat hasil terdapat pada kalimat ……', '#####', '#####', '#####', '#####', '#####', ''),
(302, 1, 1, 1, '', '', 'Di Polandia, Partai Komunis yang membawa panji Marxsisme Leninisme akhirnya dibubarkan. Pola kalimat tersebut sama dengan pola kalimat…….', '#####', '#####', '#####', '#####', '#####', ''),
(303, 1, 1, 1, '', '', 'Lena Maria Klingvall (1968) adalah gadis penyandang cacat dari Swedia. Dia dilahirkan tanpa tangan, dan hanya mempunyai satu kaki. Lena belajar berenang sejak usia 3 tahun, dan pada usianya yang kedelapan belas ia menjuarai kejuaraan renang nasional di Swedia, meraih 3 medali emas dan menorehkan 2 rekor nasional. Prestasi gemilangnya berlanjut pada paralympic games di Seoul 1988, ia memperoleh medali emas internasional. Tanggapan anda atas biografi singkat ini adalah…...', '#####', '#####', '#####', '#####', '#####', ''),
(304, 1, 1, 1, '', '', 'Saya sudah berusaha untuk memperbaiki kelemahan diri, tetapi belum juga menampakkan hasilnya. Sehingga saya... ', '#####', '#####', '#####', '#####', '#####', ''),
(305, 1, 1, 1, '', '', 'Rukun Sinaga adalah seorang mahasiswa bersuku Batak yang dikenal dengan suaranya yang lantang. Karena ikut dengan orangtuanya yang dipindah tugaskan dari Kota Medan ke Kota Solo dimana kampus barunya kebanyakan mahasiswa dikenal dengan tutur bahasanya yang lembut. Di lingkungan kampus yang baru sebaiknya Rukun ...…', '#####', '#####', '#####', '#####', '#####', ''),
(306, 1, 1, 1, '', '', 'Ayah sahabat anda mengalami serangan jantung dan masuk Rumah Sakit.', '#####', '#####', '#####', '#####', '#####', ''),
(307, 1, 1, 1, '', '', 'Saat pulang dari kampus anda membawa kendaraan dan di persimpangan jalan di dekat pos polisi ada Traffic Light yang menunjukkan lampu merah. Situasi saat itu terlihat sepi tanpa ada kendaraan lain yang akan melintas dari arah lain. Maka tindakan anda :', '#####', '#####', '#####', '#####', '#####', ''),
(308, 1, 1, 1, '', '', 'Penggunaan tanda seru yang tepat dapat digunakan pada kalimat', '#####', '#####', '#####', '#####', '#####', ''),
(309, 1, 1, 1, '', '', 'Penggunaan huruf miring untuk ungkapan asing yang benar adalah ', '#####', '#####', '#####', '#####', '#####', ''),
(310, 1, 1, 1, '', '', 'Penulisan yang benar diantara pernyataan – pernyataan dibawah ini :', '#####', '#####', '#####', '#####', '#####', ''),
(311, 1, 1, 1, '', '', 'He always waits for ……. his favorite singer.', '#####', '#####', '#####', '#####', '#####', ''),
(312, 1, 1, 1, '', '', 'He kept on _____ although I had tried to make him understand about my feeling.', '#####', '#####', '#####', '#####', '#####', ''),
(313, 1, 1, 1, '', '', 'It is possible that he will come soon. We can also say. He ……. come soon.', '#####', '#####', '#####', '#####', '#####', ''),
(314, 1, 1, 1, '', '', '3, 6, 9, 12, 15, 18, 21, …., ….. ', '#####opsi A.1', '#####opsi B.1', '#####opsi C.1', '#####opsi D.1', '#####opsi E.1', 'A'),
(315, 1, 1, 1, '', '', '12 36 12 35 10 33 10 30 8 ….  ….', '#####', '#####', '#####', '#####', '#####', '');
INSERT INTO `t_soal` (`id`, `id_petugas`, `id_jenis_soal`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `jawaban`) VALUES
(316, 1, 1, 1, '', '', '26 27 29 32 … … 40 38 35 31 26', '#####', '#####', '#####', '#####', '#####', ''),
(317, 1, 1, 1, '', '', '2X = 64 dan 3Y = 81, maka :', '#####', '#####', '#####', '#####', '#####', ''),
(318, 1, 1, 1, '', '', '3  5  7  9 …', '#####', '#####', '#####', '#####', '#####', ''),
(319, 1, 1, 1, '', '', 'Tujuh ratus adalah berapa persen dari 150 ? ', '#####', '#####', '#####', '#####', '#####', ''),
(320, 1, 1, 1, '', '', 'Intan sekarang berusia 12 tahun. Sedangkan umur Anto dua kali lebih tua daripada umur Badu. Umur Badu tiga tahun lebih tua daripada umur Nida. Jika umur Intan lima tahun lebih tua daripada umur Sari yang setahun lebih muda daripada Anto, maka urutan umur mereka dari yang termuda ke yang tertua adalah ... ', '#####', '#####', '#####', '#####', '#####', ''),
(321, 1, 1, 1, '', '', 'Dengan mobil, Chika akan berangkat dan kota A menuju kota B pada pukul 10.30 dengan kecepatan rata-rata 60 km/jam. Pada saat yang sama, Anggun juga mengendarai sebuah mobil dan kota B ke kota A dengan kecepatan rata-rata 80 km/jam. Jika jarak kedua kota 560 km, mereka akan bertemu pada pukul ......', '#####', '#####', '#####', '#####', '#####', ''),
(322, 1, 1, 1, '', '', 'Seorang pedagang menjual jambu dengan harga Rp 15.000/kg. Di dalam tokonya terdapat 6 dus dan di setiap dus berisi 5 kg jambu. Dua bekas tempat jambu itu masih bisa dijual lagi dengan harga Rp 2.000/dus. Berapakah uang hasil penjualan seluruh jambu dan dus tersebut ?', '#####', '#####', '#####', '#####', '#####', ''),
(323, 1, 1, 1, '', '', 'Harga sewa mobil di sebuah persewaan mobil adalah Rp 375.000/24 jam pertama, sedangkan kelebihan dua jam berikutnya dikenai denda Rp 25.000. Jika suatu hari, Abdul menyewa sebuah mobil dan diharuskan membayar Rp 625.000, berapa lama Abdul menyewa mobil tersebut ?', '#####', '#####', '#####', '#####', '#####', ''),
(324, 1, 1, 1, '', '', 'Hewan golongan unggas berkembang biak dengan cara bertelur. Buaya hewan bukan unggas yang berkembang biak dengan cara bertelur. Simpulan yang tepat tentang buaya adalah ', '#####', '#####', '#####', '#####', '#####', ''),
(325, 1, 1, 1, '', '', 'Hanya barang-barang dari plastik yang dijual di toko Kurnia. Tekstil terbuat dari bahan dasar kapas. Kesimpulan :', '#####', '#####', '#####', '#####', '#####', ''),
(326, 1, 1, 1, '', '', 'Semua calon mahasiswa menempuh tes Matematika. Sebagian calon mahasiswa memiliki skor TOEFL di atas 450. Simpulan yang tepat adalah', '#####', '#####', '#####', '#####', '#####', ''),
(327, 1, 1, 1, '', '', 'Noda hitam akan tampak jelas pada pakaian putih. Noda putih akan tampak jelas pada pakaian hitam. Deterjen dapat digunakan untuk menghilangkan noda. Simpulan yang tepat adalah …….', '#####', '#####', '#####', '#####', '#####', ''),
(328, 1, 1, 1, '', '', 'Setiap anggota kelompok A adalah anggota kelompok B. Setiap anggota kelompok B adalah anggota kelompok C. Simpulan yang tepat tentang kemungkinan keanggotaan A, B, dan C adalah', '#####', '#####', '#####', '#####', '#####', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_soals`
--

CREATE TABLE `t_soals` (
  `id` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_jenis_soal` int(11) NOT NULL,
  `bobot` int(3) NOT NULL,
  `gambar` varchar(150) NOT NULL,
  `soal` longtext NOT NULL,
  `opsi_a` longtext NOT NULL,
  `opsi_b` longtext NOT NULL,
  `opsi_c` longtext NOT NULL,
  `opsi_d` longtext NOT NULL,
  `opsi_e` longtext NOT NULL,
  `jawaban` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_soals`
--

INSERT INTO `t_soals` (`id`, `id_petugas`, `id_jenis_soal`, `bobot`, `gambar`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `jawaban`) VALUES
(1, 1, 1, 1, '', 'Siapakah Nabi Kedua', 'Isa AS', 'Muhammad SAW', 'Idris AS', 'Adam AS', 'Yusuf AS', 'C'),
(3, 1, 1, 1, '1.jpg', 'Tempat Ibadah Umat ', 'Kristen', 'Hindu', 'Islam', 'Budha', 'Yahudi', 'C'),
(4, 1, 1, 1, '', 'Juventus Klub Berasal Dari Negara ?', 'Inggris', 'Indonesia', 'Belanda', 'Jerman', 'Italia', 'E'),
(5, 1, 1, 1, '', 'Siapakah Walikota Kota Gorontalo Saat Ini', 'Irwan Karim', 'Marten Taha', 'Rusli Habibie', 'Idris Rahim', 'Adhan Dambea', 'B'),
(6, 1, 1, 1, '', 'Owner Microsoft ?', 'Bill Gates', 'Steve Jobs', 'Irwan Karim', 'Donald Trrump', 'Zakir Naik', 'A'),
(7, 1, 1, 1, '', 'Siapakah Pahlawan Dari Gorontalo', 'Sultan Hasanuddin', 'Nani Wartabone', 'Pangeran Hidayat', 'Soekarno', 'Pangeran Diponegoro', 'B'),
(8, 1, 1, 1, '', 'Makanan Khas Gorontalo ?\r\n', 'Milu Siram', 'Nasi Kuning', 'Lalapan', 'Coto Makassar', 'Tinu Tuan', 'A'),
(10, 1, 1, 1, '', 'Otak Komputer ?', 'VGA', 'Processor', 'RAM', 'Harddisk', 'Chipset', 'B');

-- --------------------------------------------------------

--
-- Table structure for table `t_soal_peserta`
--

CREATE TABLE `t_soal_peserta` (
  `id` int(11) NOT NULL,
  `kd_peserta` varchar(25) NOT NULL,
  `id_jenis_soal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_test_soal`
--

CREATE TABLE `t_test_soal` (
  `id` int(11) NOT NULL,
  `id_jenis_soal` int(11) NOT NULL,
  `jumlah_soal` int(6) NOT NULL,
  `waktu` int(6) NOT NULL,
  `jenis` enum('acak','set') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_test_soal`
--

INSERT INTO `t_test_soal` (`id`, `id_jenis_soal`, `jumlah_soal`, `waktu`, `jenis`) VALUES
(1, 1, 8, 1, 'acak');

-- --------------------------------------------------------

--
-- Table structure for table `t_token_ujian`
--

CREATE TABLE `t_token_ujian` (
  `id_token_ujian` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `token_ujian` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_waktu_ruangan`
--

CREATE TABLE `t_waktu_ruangan` (
  `id` int(11) NOT NULL,
  `id_sesi` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `id_gelombang` int(11) NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `terlambat` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_waktu_ruangan`
--

INSERT INTO `t_waktu_ruangan` (`id`, `id_sesi`, `id_ruangan`, `id_gelombang`, `tgl_mulai`, `terlambat`) VALUES
(1, 1, 15, 1, '2018-07-17 08:15:00', 200),
(2, 1, 14, 1, '2018-07-17 08:15:00', 20),
(3, 1, 13, 1, '2018-07-17 08:15:00', 20),
(4, 1, 12, 1, '2018-07-17 08:15:00', 200),
(5, 1, 11, 1, '2018-07-17 08:15:00', 20),
(6, 1, 10, 1, '2018-07-17 08:15:00', 20),
(7, 1, 9, 1, '2018-07-17 08:15:00', 20),
(8, 1, 8, 1, '2018-07-17 08:15:00', 20),
(9, 1, 7, 1, '2018-07-17 08:15:00', 20),
(10, 1, 6, 1, '2018-07-17 08:15:00', 200),
(11, 1, 5, 1, '2018-07-17 08:15:00', 20),
(12, 1, 4, 1, '2018-07-17 08:15:00', 20),
(13, 1, 3, 1, '2018-07-17 08:15:00', 200),
(14, 1, 2, 1, '2018-07-17 08:15:00', 20),
(15, 1, 1, 1, '2018-07-17 08:15:00', 200),
(16, 1, 15, 2, '2018-07-17 10:15:00', 20),
(17, 1, 14, 2, '2018-07-17 10:15:00', 20),
(18, 1, 13, 2, '2018-07-17 10:15:00', 20),
(19, 1, 12, 2, '2018-07-17 10:15:00', 20),
(20, 1, 11, 2, '2018-07-17 10:15:00', 20),
(21, 1, 10, 2, '2018-07-17 10:15:00', 20),
(22, 1, 9, 2, '2018-07-17 10:15:00', 20),
(23, 1, 8, 2, '2018-07-17 10:15:00', 20),
(24, 1, 7, 2, '2018-07-17 10:15:00', 20),
(25, 1, 6, 2, '2018-07-17 10:15:00', 20),
(26, 1, 5, 2, '2018-07-17 10:15:00', 20),
(27, 1, 4, 2, '2018-07-17 10:15:00', 20),
(28, 1, 3, 2, '2018-07-17 10:15:00', 60),
(29, 1, 2, 2, '2018-07-17 10:15:00', 60),
(30, 1, 1, 2, '2018-07-17 10:15:00', 60),
(31, 1, 15, 3, '2018-07-17 12:45:00', 20),
(32, 1, 14, 3, '2018-07-17 12:45:00', 20),
(33, 1, 13, 3, '2018-07-17 12:45:00', 20),
(34, 1, 12, 3, '2018-07-17 12:45:00', 20),
(35, 1, 11, 3, '2018-07-17 12:45:00', 20),
(36, 1, 10, 3, '2018-07-17 12:45:00', 20),
(37, 1, 9, 3, '2018-07-17 12:45:00', 20),
(38, 1, 8, 3, '2018-07-17 12:45:00', 20),
(39, 1, 7, 3, '2018-07-17 12:45:00', 20),
(40, 1, 6, 3, '2018-07-17 12:45:00', 20),
(41, 1, 5, 3, '2018-07-17 12:45:00', 20),
(42, 1, 4, 3, '2018-07-17 12:45:00', 20),
(43, 1, 3, 3, '2018-07-17 12:45:00', 20),
(44, 1, 2, 3, '2018-07-17 12:45:00', 20),
(45, 1, 1, 3, '2018-07-17 12:45:00', 20),
(46, 1, 1, 4, '2018-07-17 07:16:00', 20);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_masuk`
--
ALTER TABLE `log_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_guru_tes`
--
ALTER TABLE `tr_guru_tes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_mapel` (`id_jenis_soal`);

--
-- Indexes for table `t_admin`
--
ALTER TABLE `t_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_gelombang`
--
ALTER TABLE `t_gelombang`
  ADD PRIMARY KEY (`id_gelombang`);

--
-- Indexes for table `t_jenis_soal`
--
ALTER TABLE `t_jenis_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_list_option_soal`
--
ALTER TABLE `t_list_option_soal`
  ADD PRIMARY KEY (`kd_option_soal`);

--
-- Indexes for table `t_log_soal`
--
ALTER TABLE `t_log_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_option_soal`
--
ALTER TABLE `t_option_soal`
  ADD PRIMARY KEY (`id_jawaban`);

--
-- Indexes for table `t_peserta`
--
ALTER TABLE `t_peserta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_petugas_jenis_soal`
--
ALTER TABLE `t_petugas_jenis_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_petugas_soal`
--
ALTER TABLE `t_petugas_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pilihan_peserta`
--
ALTER TABLE `t_pilihan_peserta`
  ADD PRIMARY KEY (`id_pilihan_peserta`);

--
-- Indexes for table `t_ruangan`
--
ALTER TABLE `t_ruangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_soal`
--
ALTER TABLE `t_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_soals`
--
ALTER TABLE `t_soals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_soal_peserta`
--
ALTER TABLE `t_soal_peserta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_test_soal`
--
ALTER TABLE `t_test_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_token_ujian`
--
ALTER TABLE `t_token_ujian`
  ADD PRIMARY KEY (`id_token_ujian`);

--
-- Indexes for table `t_waktu_ruangan`
--
ALTER TABLE `t_waktu_ruangan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log_masuk`
--
ALTER TABLE `log_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=967;
--
-- AUTO_INCREMENT for table `tr_guru_tes`
--
ALTER TABLE `tr_guru_tes`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_admin`
--
ALTER TABLE `t_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_gelombang`
--
ALTER TABLE `t_gelombang`
  MODIFY `id_gelombang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_jenis_soal`
--
ALTER TABLE `t_jenis_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_log_soal`
--
ALTER TABLE `t_log_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=719;
--
-- AUTO_INCREMENT for table `t_option_soal`
--
ALTER TABLE `t_option_soal`
  MODIFY `id_jawaban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1540;
--
-- AUTO_INCREMENT for table `t_peserta`
--
ALTER TABLE `t_peserta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1935;
--
-- AUTO_INCREMENT for table `t_petugas_jenis_soal`
--
ALTER TABLE `t_petugas_jenis_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t_petugas_soal`
--
ALTER TABLE `t_petugas_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_ruangan`
--
ALTER TABLE `t_ruangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `t_soal`
--
ALTER TABLE `t_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=329;
--
-- AUTO_INCREMENT for table `t_soals`
--
ALTER TABLE `t_soals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `t_soal_peserta`
--
ALTER TABLE `t_soal_peserta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1841;
--
-- AUTO_INCREMENT for table `t_test_soal`
--
ALTER TABLE `t_test_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_waktu_ruangan`
--
ALTER TABLE `t_waktu_ruangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
