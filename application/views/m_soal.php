<?php 
$uri4 = $this->uri->segment(4);
?>

<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading"><b>Data</b>
      <div class="tombol-kanan">
        <a class="btn btn-success btn-sm" href="<?php echo base_url(); ?>adm/m_soal/edit/0"><i class="glyphicon glyphicon-plus" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Tambah Soal</a>        
        <a class="btn btn-danger btn-sm tombol-kanan" href="<?php echo base_url(); ?>upload/format_soal_download.xlsx" ><i class="glyphicon glyphicon-download"></i> &nbsp;&nbsp;Format Import Soal</a>
        <a class="btn btn-info btn-sm tombol-kanan" href="<?php echo base_url(); ?>adm/m_soal/import" ><i class="glyphicon glyphicon-upload"></i> &nbsp;&nbsp;Import Soal</a>
        <a class="btn btn-success btn-sm" href="<?php echo base_url(); ?>adm/m_soal/edit_jwb/0"><i class="glyphicon glyphicon-plus" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Tambah Jawaban</a>
        <a class="btn btn-danger btn-sm tombol-kanan" href="<?php echo base_url(); ?>upload/format_jawaban_download.xlsx" ><i class="glyphicon glyphicon-download"></i> &nbsp;&nbsp;Format Import Jawaban</a>
        <a class="btn btn-info btn-sm tombol-kanan" href="<?php echo base_url(); ?>adm/m_soal/imports" ><i class="glyphicon glyphicon-upload"></i> &nbsp;&nbsp;Import Jawaban</a>
        <a href='<?php echo base_url(); ?>adm/m_soal/cetak/<?php echo $uri4; ?>' class='btn btn-warning btn-sm' target='_blank'><i class='glyphicon glyphicon-print'></i> Cetak</a>
      </div>
    </div>
    <div class="panel-body">
        
        <?php echo $this->session->flashdata('k'); ?>
        
        <table class="table table-bordered" id="datatabel">
          <thead>
            <tr>
              <td width="5%">No</td>
              <td width="5%">ID Soal</td>
              <td width="50%">Soal</td>
              <td width="10%">Jenis Soal/Petugas</td>
              <td width="35%">Aksi</td>
            </tr>
          </thead>

          <tbody>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
