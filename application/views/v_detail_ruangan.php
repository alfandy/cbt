<div class="row col-md-12">
  <div class="panel panel-info">
    <div class="panel-heading">Data Peserta </div>
    <div class="panel-body">


      <table class="table table-bordered">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="10%">Kode</th>
            <th width="25%">Nama</th>
          </tr>
        </thead>

        <tbody>
          <?php 
            if (!empty($detail_ruangan)) {
              $no = 1;
              foreach ($detail_ruangan as $d) {
                echo '<tr>
                      <td class="ctr">'.$no.'</td>
                      <td>'.$d->kode.'</td>
                      <td>'.$d->nama_peserta.'</td>
                      </tr>
                      ';
              $no++;
              }
            } else {
              echo '<tr><td colspan="5">Belum ada data</td></tr>';
            }
          ?>
        </tbody>
      </table>
    
      </div>
    </div>
  </div>
</div>
                    




<div class="modal fade" id="m_siswa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myModalLabel">Data Siswa</h4>
      </div>
      <div class="modal-body">
          <form name="f_siswa" id="f_siswa" onsubmit="return m_siswa_s();">
            <input type="hidden" name="id" id="id" value="0"> 
              <table class="table table-form">
                <tr><td style="width: 25%">Kode Peserta</td><td style="width: 75%"><input type="text" class="form-control" name="kode" id="kode" required></td></tr>
                <tr><td style="width: 25%">Nama</td><td style="width: 75%"><input type="text" class="form-control" name="nama_peserta" id="nama_peserta" required></td></tr>
                <tr>
                <td style="width: 25%">Pilihan</td><td style="width: 75%">
                  <select name="id_pilihan_peserta" id="id_pilihan_peserta" class="form-control">
                    <?php 
                        foreach ($pilihan as $pil) {
                    ?>
                        <option value="<?php echo $pil->id_pilihan_peserta ?>"><?php echo $pil->pilihan_peserta ?></option>
                    <?php 
                        }
                     ?>
                  </select>  
                </td>
                </tr>
                <tr>
                <td style="width: 25%">Ruangan</td>
                <td style="width: 75%">
                  <select name="id_ruangan" id="id_ruangan" class="form-control">
                    <?php 
                        foreach ($ruangan as $rg) {
                    ?>
                        <option value="<?php echo $rg->id_ruangan ?>"><?php echo $rg->ruangan ?></option>
                    <?php
                        }
                     ?>
                  </select>
                </td>
                </tr>
              </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary">Simpan</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
      </div>
        </form>
    </div>
  </div>
</div>