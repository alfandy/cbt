<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=laporan.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<table border="1" width="100%">

	<thead>

		<tr>

			<th>No</th>
			<th>Kode Peserta</th>
			<th>Nama</th>
			<th>Gelombang</th>
			<th>Ruangan</th>
			<th>Jumlah Benar</th>
			<th>Jumlah Salah</th>
			<th>Nilai</th>

		</tr>

	</thead>

	<tbody>


			<?php if (!empty($data)): ?>
				<?php foreach ($data as $key => $value): ?>
					<tr>

						<td><?php echo $key+1 ?></td>

						<td style="mso-number-format:\@;"><?php echo "$value->kode_peserta" ?></td>
						<td><?php echo $value->nama_peserta ?></td>
						<td><?php echo $value->gelombang ?></td>
						<td><?php echo $value->ruangan ?></td>
						<td style="mso-number-format:0;"><?php echo $value->jml_benar ?></td>
						<td style="mso-number-format:0;"><?php echo $value->jml_salah ?></td>
						<td style="mso-number-format:0;"><?php echo $value->nilai_bobot ?></td>


					</tr>

				<?php endforeach ?>
			<?php endif ?>

		</tbody>

	</table>