<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Input Soal</div>
    <div class="panel-body">
      <?php echo form_open_multipart(base_url()."adm/m_soal/simpan_jwb", "class='form-horizontal'"); ?>
          <input type="hidden" name="id" id="id" value="<?php echo $d['id_jawaban']; ?>">
          <input type="hidden" name="mode" id="mode" value="<?php echo $d['mode']; ?>">
          <input type="hidden" name="id_soal_back" id="id_soal_back" value="<?php echo $d['id_soal']; ?>">
          <div id="konfirmasi"></div>

            <div class="form-group fgsoal">
              <div class="col-md-2"><label>Soal</label></div>
              <div class="col-md-10">
             <!--  <select name="id_soal" id="id_soal" class="form-control select2" required>
                <option value="">--Pilih Soal--</option>
                <?php // foreach ($p_soal as $ps) {
                ?>
                  <option value="<?php //echo $ps->id ?>"><?php //echo $ps->soal ?></option>
                <?php 
                //} ?>  
              </select> -->
              <?php 
              	if (!empty($p_soal)) {
              		foreach ($p_soal as $ps) {
              			$soalPilihan[''] = '--Pilih Soal--';
              			$soalPilihan[$ps->id] = $ps->soal;
              		}
              	}else{
          			$soalPilihan[''] = 'Maaf Data Soal Masih Kosong';
          		}
               ?>
              <?php echo form_dropdown('id_soal', $soalPilihan, $d['id_soal'] , 'class="form-control select2" required ');?>
            </div>
            </div>

            <div class="form-group fgsoal">
              <div class="col-md-2"><label>Jawaban</label></div>
              <div class="col-md-3">
                <input type="file" name="gambar_soal" id="gambar_soal" class="btn btn-info upload"><br>
                <?php 
                if (is_file('./upload/gambar_opsi/'.$d['file'])) {
                  echo tampil_media('./upload/gambar_opsi/'.$d['file'],"100%");
                }
                ?>
              </div>
              <div class="col-md-7">
                <textarea class="form-control"  id="editornya" style="height: 50px;"  name="jawaban"><?php echo $d['jawaban'] ?></textarea>
              </div>
            </div>
  
            <div class="form-group fgsoal">
              <div class="col-md-2"><label>Status</label></div>
              <div class="col-md-3">
              <?php echo form_dropdown('benar', $options, $d['benar'] , 'class="form-control" required'); ?>
              <!-- <select name="benar" class="form-control" required>
                <option value="<?php //echo $d['benar'] ?>"><?php //echo $d['benar'] ?></option>
                <option value="B">B</option>
                <option value="S">S</option>
              </select> -->
            </div>
            </div>
            <div class="form-group" style="margin-top: 20px">
              <div class="col-md-12">
                <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Simpan</button>
                <a href="<?php echo base_url(); ?>adm/m_soal/jawaban/<?php echo $d['id_soal']; ?>" class="btn btn-default"><i class="fa fa-minus-circle"></i> Kembali</a>
              </div>
            </div>
      </form>
    </div><!-- panel body-->
  </div>
</div>
</div>
