<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Makassar");
class Ser extends CI_Controller {
	function __construct() {
	    parent::__construct();
	    
	}

    public function dataPeserta( $starta = 0 , $limita = 10 ){
        $start = intval($starta);
        $limit = intval($limita);
        if ((is_int($start) && is_int($limit)) && $limit > 0) {
            $datas['limit'] = $limit;
            $datas['start'] = $start;
            /*$datas['totalRows'] = $data = $this->db->query("SELECT count(a.kode) as total
                FROM t_peserta as a 
                left join t_ruangan as b 
                    on a.id_ruangan = b.id_ruangan
                left join t_gelombang as c 
                    on a.id_gelombang = c.id_gelombang 
                left join t_log_soal as d 
                    on a.kode  = d.kd_peserta")->row()->total;*/
            $datas['totalRows'] = $this->db->select("count(a.kode) as total")
                                            ->from("t_peserta as a")
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->get()
                                            ->row()
                                            ->total;
             /*$datas['data'] = $data = $this->db->query("SELECT a.kode , a.nama_peserta , 
                b.ruangan , b.token_ujian as passwordRuangan , 
                c.gelombang,
                d.list_soal , d.list_jawaban, d.jml_benar, d.jml_salah , d.nilai,d.nilai_bobot, d.waktu_mulai, d.waktu_selesai , d.waktu , d.status
                FROM t_peserta as a 
                left join t_ruangan as b 
                    on a.id_ruangan = b.id_ruangan
                left join t_gelombang as c 
                    on a.id_gelombang = c.id_gelombang 
                left join t_log_soal as d 
                    on a.kode  = d.kd_peserta
                limit $start , $limit  ")->result();
            $datas['rows'] = count($data);*/
            $datas['data'] = $this->db->select("a.kode as kode_peserta , a.nama_peserta , 
                                                    b.ruangan , b.token_ujian as passwordRuangan , 
                                                    c.gelombang,
                                                    d.list_soal , d.list_jawaban, d.jml_benar, d.jml_salah , d.nilai,d.nilai_bobot, d.waktu_mulai, d.waktu_selesai , d.waktu , d.status")
                                            ->from("t_peserta as a")
                                            ->join('t_ruangan as b', 'a.id_ruangan = b.id_ruangan' , 'left')
                                            ->join('t_gelombang as c' , 'a.id_gelombang = c.id_gelombang', 'left')
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->limit($limit ,$start)
                                            ->get()
                                            ->result();
        }else{
            $datas['data'] = NULL;
        }
        echo json_encode($datas);
        
    }

    public function dataPesertaByKodePeserta($kodePeserta){
        
       
        $datas['data'] = $this->db->select("a.kode as kode_peserta, a.nama_peserta , 
                                                b.ruangan , b.token_ujian as passwordRuangan , 
                                                c.gelombang,
                                                d.list_soal , d.list_jawaban, d.jml_benar, d.jml_salah , d.nilai,d.nilai_bobot, d.waktu_mulai, d.waktu_selesai , d.waktu , d.status")
                                        ->from("t_peserta as a")
                                        ->join('t_ruangan as b', 'a.id_ruangan = b.id_ruangan' , 'left')
                                        ->join('t_gelombang as c' , 'a.id_gelombang = c.id_gelombang', 'left')
                                        ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                        ->where('a.kode' , $kodePeserta)
                                        ->get()
                                        ->row();
        echo json_encode($datas);
    }

    public function dataPesertaNilaiMax( $starta = 0 , $limita = 10 ){
        $start = intval($starta);
        $limit = intval($limita);
        if ((is_int($start) && is_int($limit)) && $limit > 0) {
            $datas['limit'] = $limit;
            $datas['start'] = $start;
            /*$datas['totalRows'] = $data = $this->db->query("SELECT count(a.kode) as total
                FROM t_peserta as a 
                left join t_ruangan as b 
                    on a.id_ruangan = b.id_ruangan
                left join t_gelombang as c 
                    on a.id_gelombang = c.id_gelombang 
                left join t_log_soal as d 
                    on a.kode  = d.kd_peserta")->row()->total;*/
            $datas['totalRows'] = $this->db->select("count(a.kode) as total")
                                            ->from("t_peserta as a")
                                            ->join('t_ruangan as b', 'a.id_ruangan = b.id_ruangan' , 'left')
                                            ->join('t_gelombang as c' , 'a.id_gelombang = c.id_gelombang', 'left')
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->where(array('d.kd_peserta IS NOT NULL' => NULL))
                                            ->get()
                                            ->row()
                                            ->total;
            $datas['data'] = $this->db->select("a.kode as kode_peserta, a.nama_peserta , 
                                                    b.ruangan , b.token_ujian as passwordRuangan , 
                                                    c.gelombang,
                                                    d.list_soal , d.list_jawaban, d.jml_benar, d.jml_salah , d.nilai,d.nilai_bobot, d.waktu_mulai, d.waktu_selesai , d.waktu , d.status")
                                            ->from("t_peserta as a")
                                            ->join('t_ruangan as b', 'a.id_ruangan = b.id_ruangan' , 'left')
                                            ->join('t_gelombang as c' , 'a.id_gelombang = c.id_gelombang', 'left')
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->where(array('d.kd_peserta IS NOT NULL' => NULL))
                                            ->order_by('d.nilai' , 'desc')
                                            ->limit($limit ,$start)
                                            ->get()
                                            ->result();
        }else{
            $datas['data'] = NULL;
        }
        echo json_encode($datas);
    }

    public function dataPesertaNilaiMin( $starta = 0 , $limita = 10 ){
        $start = intval($starta);
        $limit = intval($limita);
        if ((is_int($start) && is_int($limit)) && $limit > 0) {
            $datas['limit'] = $limit;
            $datas['start'] = $start;

            $datas['totalRows'] = $this->db->select("count(a.kode) as total")
                                            ->from("t_peserta as a")
                                            ->join('t_ruangan as b', 'a.id_ruangan = b.id_ruangan' , 'left')
                                            ->join('t_gelombang as c' , 'a.id_gelombang = c.id_gelombang', 'left')
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->where(array('d.kd_peserta IS NOT NULL' => NULL))
                                            ->get()
                                            ->row()
                                            ->total;

            $datas['data'] = $this->db->select("a.kode as kode_peserta, a.nama_peserta , 
                                                    b.ruangan , b.token_ujian as passwordRuangan , 
                                                    c.gelombang,
                                                    d.list_soal , d.list_jawaban, d.jml_benar, d.jml_salah , d.nilai,d.nilai_bobot, d.waktu_mulai, d.waktu_selesai , d.waktu , d.status")
                                            ->from("t_peserta as a")
                                            ->join('t_ruangan as b', 'a.id_ruangan = b.id_ruangan' , 'left')
                                            ->join('t_gelombang as c' , 'a.id_gelombang = c.id_gelombang', 'left')
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->where(array('d.kd_peserta IS NOT NULL' => NULL))
                                            ->order_by('d.nilai' , 'asc')
                                            ->limit($limit ,$start)
                                            ->get()
                                            ->result();
        }
        echo json_encode($datas);
    }

    public function dataPesertTidakUjian( $starta = 0 , $limita= 10 ){
        $start = intval($starta);
        $limit = intval($limita);
        if ((is_int($start) && is_int($limit)) && $limit > 0) {
            $datas['limit'] = $limit;
            $datas['start'] = $start;
            
            $datas['totalRows'] = $this->db->select("count(a.kode) as total")
                                            ->from("t_peserta as a")
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->where(array('d.kd_peserta IS NULL' => NULL))
                                            ->get()
                                            ->row()
                                            ->total;
           
             
            $datas['data'] = $this->db->select("a.kode as kode_peserta, a.nama_peserta , 
                                                    b.ruangan , b.token_ujian as passwordRuangan , 
                                                    c.gelombang,
                                                    d.list_soal , d.list_jawaban, d.jml_benar, d.jml_salah , d.nilai,d.nilai_bobot, d.waktu_mulai, d.waktu_selesai , d.waktu , d.status")
                                            ->from("t_peserta as a")
                                            ->join('t_ruangan as b', 'a.id_ruangan = b.id_ruangan' , 'left')
                                            ->join('t_gelombang as c' , 'a.id_gelombang = c.id_gelombang', 'left')
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->where(array('d.kd_peserta IS NULL' => NULL))
                                            ->limit($limit ,$start)
                                            ->get()
                                            ->result();
        }else{
            $datas['data'] = NULL;
        }
        echo json_encode($datas);
        
        
    }

    public function dataPesertIkutUjian( $starta = 0 , $limita = 10 ){
        $start = intval($starta);
        $limit = intval($limita);
        if ((is_int($start) && is_int($limit)) && $limit > 0) {
            $datas['limit'] = $limit;
            $datas['start'] = $start;
            
            $datas['totalRows'] = $this->db->select("count(a.kode) as total")
                                            ->from("t_peserta as a")
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->where(array('d.kd_peserta IS NOT NULL' => NULL))
                                            ->get()
                                            ->row()
                                            ->total;
           
             
            $datas['data'] = $this->db->select("a.kode as kode_peserta, a.nama_peserta , 
                                                    b.ruangan , b.token_ujian as passwordRuangan , 
                                                    c.gelombang,
                                                    d.list_soal , d.list_jawaban, d.jml_benar, d.jml_salah , d.nilai,d.nilai_bobot, d.waktu_mulai, d.waktu_selesai , d.waktu , d.status")
                                            ->from("t_peserta as a")
                                            ->join('t_ruangan as b', 'a.id_ruangan = b.id_ruangan' , 'left')
                                            ->join('t_gelombang as c' , 'a.id_gelombang = c.id_gelombang', 'left')
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->where(array('d.kd_peserta IS NOT NULL' => NULL))
                                            ->limit($limit ,$start)
                                            ->get()
                                            ->result();
        }else{
            $datas['data'] = NULL;
        }
        echo json_encode($datas);
        
        
    }
	
	
}
