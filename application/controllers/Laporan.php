<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Makassar");
class Laporan extends CI_Controller {
	function __construct() {
	    parent::__construct();
	    
	}

    public function exel( $starta = 0 , $limita = 10 ){
        $start = intval($starta);
        $limit = intval($limita);
        if ((is_int($start) && is_int($limit)) && $limit > 0) {
            $datas['limit'] = $limit;
            $datas['start'] = $start;
            
            $datas['totalRows'] = $this->db->select("count(a.kode) as total")
                                            ->from("t_peserta as a")
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->get()
                                            ->row()
                                            ->total;
    
            $datas['data'] = $this->db->select("a.kode as kode_peserta , a.nama_peserta , 
                                                    b.ruangan , b.token_ujian as passwordRuangan , 
                                                    c.gelombang,
                                                    d.list_soal , d.list_jawaban, d.jml_benar, d.jml_salah , d.nilai,d.nilai_bobot, d.waktu_mulai, d.waktu_selesai , d.waktu , d.status")
                                            ->from("t_peserta as a")
                                            ->join('t_ruangan as b', 'a.id_ruangan = b.id_ruangan' , 'left')
                                            ->join('t_gelombang as c' , 'a.id_gelombang = c.id_gelombang', 'left')
                                            ->join('t_log_soal as d' , 'a.kode = d.kd_peserta' , 'left')
                                            ->limit($limit ,$start)
                                            ->get()
                                            ->result();
        }else{
            $datas['data'] = NULL;
        }
        $datas;

        $this->load->view('tambahan/laporanEcel',$datas);
    }
	
}
