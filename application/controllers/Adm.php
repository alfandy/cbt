<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Makassar");
class Adm extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model("m_adm");
        $this->db->query("SET time_zone='+8:00'");
        $waktu_sql = $this->db->query("SELECT NOW() AS waktu")->row_array();
        $this->waktu_sql = $waktu_sql['waktu'];
        $this->opsi = array("a","b","c","d","e");
   //     $this->load->model("m_adm");
	}
	
	public function get_servertime() {
		$now = new DateTime(); 
        $dt = $now->format("M j, Y H:i:s O"); 

        j($dt);
	}

	public function cek_aktif() {
		if ($this->session->userdata('admin_valid') == false && $this->session->userdata('admin_id') == "") {
			redirect('adm/login');
		} 
	}
	
	public function index() {
		$this->cek_aktif();
		
		// tes tambah
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		
		$a['p']			= "v_main";
		
		$this->load->view('aaa', $a);
	}
	
	/* == ADMIN == */
	public function m_siswa() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));
		//return as json
		$jeson = array();
		//$a['data'] = $this->db->query("")->result();
		$a['ruangan'] = $this->db->query("SELECT * FROM t_ruangan")->result();
		$a['pilihan_peserta'] = $this->db->query("SELECT * FROM t_pilihan_peserta")->result();
		if ($uri3 == "det") {
			$a = $this->db->query("SELECT * FROM t_peserta WHERE id = '$uri4'")->row();
			j($a);
			exit();
		} else if ($uri3 == "simpan") {
			$ket 	= "";
			if ($p->id != 0) {
				$this->db->query("UPDATE t_peserta SET kode = '".$p->kode."', nama_peserta = '".$p->nama_peserta."', id_ruangan = '".$p->id_ruangan."', id_pilihan_peserta = '".$p->id_pilihan_peserta."'	WHERE id = '".$p->id."'");
				$ket = "edit";
				$ret_arr['status'] 	= "ok";
			} else {
				$ket = "tambah";
				$qrkode = $this->db->get_where('t_peserta', array('kode' => $p->kode));
				if ($qrkode->num_rows() > 0) {
					$ret_arr['status'] = "oks";
				} else {
				$this->db->set("kode",$p->kode);
				$this->db->set("nama_peserta",$p->nama_peserta);
				$this->db->set("id_ruangan",$p->id_ruangan);
				$this->db->set("id_pilihan_peserta",$p->id_pilihan_peserta);
				$this->db->set("id_gelombang",1);
				$this->db->set("level","peserta");
				$this->db->insert("t_peserta");
				$ret_arr['status'] 	= "ok";
				//echo"<pre>";print_r($p);echo"</pre>";
				//$this->db->query("INSERT INTO t_peserta VALUES ('".bersih($p,"kode")."', '".bersih($p,"nama_peserta")."', '', '".bersih($p,"id_ruangan")."', 'peserta')");
				}
			}
			$ret_arr['caption']	= $ket." sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "hapus") {
			$this->db->query("DELETE FROM t_peserta WHERE id = '".$uri4."'");
						
			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= "hapus sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "user") {
			$det_user = $this->db->query("SELECT id, nim FROM m_siswa WHERE id = '$uri4'")->row();

			if (!empty($det_user)) {
				$q_cek_username = $this->db->query("SELECT id FROM m_admin WHERE username = '".$det_user->nim."' AND level = 'siswa'")->num_rows();

				if ($q_cek_username < 1) {

					$this->db->query("INSERT INTO m_admin VALUES (null, '".$det_user->nim."', md5('".$det_user->nim."'), 'siswa', '".$det_user->id."')");
					$ret_arr['status'] 	= "ok";
					$ret_arr['caption']	= "tambah user sukses";
					j($ret_arr);
				} else {
					$ret_arr['status'] 	= "gagal";
					$ret_arr['caption']	= "Username telah digunakan";
					j($ret_arr);					
				}
			} else {
				$ret_arr['status'] 	= "gagal";
				$ret_arr['caption']	= "tambah user gagal";
				j($ret_arr);
			}
			exit();
		} else if ($uri3 == "user_reset") {
			$det_user = $this->db->query("SELECT id, nim FROM m_siswa WHERE id = '$uri4'")->row();

			$this->db->query("UPDATE m_admin SET password = md5('".$det_user->nim."') WHERE level = 'siswa' AND kon_id = '".$det_user->id."'");

			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= "Update password sukses";
			j($ret_arr);

			exit();
		} else if ($uri3 == "ambil_matkul") {
			$matkul = $this->db->query("SELECT t_jenis_soal.*,
										(SELECT COUNT(id) FROM t_soal_peserta WHERE kd_peserta = ".$uri4." AND id_jenis_soal = t_jenis_soal.id) AS ok
										FROM t_jenis_soal
										")->result();
			$ret_arr['status'] = "ok";
			$ret_arr['data'] = $matkul;
			j($ret_arr);
			exit;
		} else if ($uri3 == "simpan_matkul") {
			$ket 	= "";
			//echo var_dump($p);

			$ambil_matkul = $this->db->query("SELECT id FROM t_jenis_soal ORDER BY id ASC")->result();
			if (!empty($ambil_matkul)) {
				foreach ($ambil_matkul as $a) {
					$p_sub = "id_mapel_".$a->id;

					if (!empty($p->$p_sub)) {
						
						$cek_sudah_ada = $this->db->query("SELECT id FROM t_soal_peserta WHERE  kd_peserta = '".$p->id_mhs."' AND id_jenis_soal = '".$a->id."'")->num_rows();
						

						if ($cek_sudah_ada < 1) {
							$this->db->set("kd_peserta",$p->id_mhs);
							$this->db->set("id_jenis_soal",$a->id);
							$this->db->insert("t_soal_peserta");
							//	$this->db->query("INSERT INTO t_soal_peserta VALUES ('',".$p->id_mhs."', '".$a->id."')");
						} else {
							$this->db->query("UPDATE t_soal_peserta SET id_jenis_soal = '".$p->$p_sub."' WHERE kd_peserta = '".$p->id_mhs."' AND id_jenis_soal = '".$a->id."'");
						}
					} else {
						//echo "0<br>";
						$this->db->query("DELETE FROM t_soal_peserta WHERE kd_peserta = '".$p->id_mhs."' AND id_jenis_soal = '".$a->id."'");
					}
				}
			}

			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= $ket." sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "data") {
			$start = $this->input->post('start');
	        $length = $this->input->post('length');
	        $draw = $this->input->post('draw');
	        $search = $this->input->post('search');

	//      $d_total_row = $this->db->query("SELECT id FROM t_peserta a WHERE a.nama_peserta LIKE '%".$search['value']."%'")->num_rows();
	//    	$q_datanya = $this->db->query("SELECT a.id, a.kode, a.nama_peserta, b.pilihan_peserta, c.ruangan FROM t_peserta a, t_pilihan_peserta b, t_ruangan c WHERE a.id_ruangan = c.id_ruangan AND a.id_pilihan_peserta = b.id_pilihan_peserta AND a.nama_peserta LIKE '%".$search['value']."%' ORDER BY a.kode ASC LIMIT ".$start.", ".$length."")->result_array();
	      	/*
	        $q_datanya = $this->db->query("SELECT a.*,
											(SELECT COUNT(id) FROM m_admin WHERE level = 'siswa' AND kon_id = a.id) AS ada
											FROM m_siswa a
	                                        WHERE a.nama LIKE '%".$search['value']."%' ORDER BY a.id DESC LIMIT ".$start.", ".$length."")->result_array();
	        */
			$d_total_row = $this->db->query("SELECT a.*
	        								FROM t_peserta a
	        								INNER JOIN t_pilihan_peserta b ON a.id_pilihan_peserta = b.id_pilihan_peserta
	        								INNER JOIN t_ruangan c ON a.id_ruangan = c.id_ruangan
	        								WHERE a.nama_peserta LIKE '%".$search['value']."%'
	        								OR a.kode LIKE '%".$search['value']."%'")->num_rows();
	    	$q_datanya = $this->db->query("SELECT a.*, b.pilihan_peserta, c.ruangan
	    									FROM t_peserta a
	    									INNER JOIN t_pilihan_peserta b ON a.id_pilihan_peserta = b.id_pilihan_peserta
	    									INNER JOIN t_ruangan c ON a.id_ruangan = c.id_ruangan
	    									WHERE a.nama_peserta LIKE '%".$search['value']."%'
	    									OR a.kode LIKE '%".$search['value']."%'
	    									ORDER BY a.kode ASC LIMIT ".$start.", ".$length."")->result_array();
	        $data = array();
	        $no = ($start+1);

	        foreach ($q_datanya as $d) {
	            $data_ok = array();
	            $data_ok[0] = $no++;
	            $data_ok[1] = $d['kode'];
	            $data_ok[2] = $d['nama_peserta'];
	            $data_ok[3] = $d['ruangan'];
	            $data_ok[4] = $d['pilihan_peserta'];


	            $data_ok[5] = '<div class="btn-group">
                          <a href="#" onclick="return m_siswa_e('.$d['id'].');" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Edit</a>
                          <a href="#" onclick="return m_siswa_h('.$d['id'].');" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Hapus</a>
                          <a href="#" onclick="return m_siswa_matkul('.$d['kode'].');" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-th-list" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Jenis Soal</a>
                         ';
                /*
                if ($d['ada'] == "0") {
                  $data_ok[4] .= '<a href="#" onclick="return m_siswa_u('.$d['id'].');" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-user" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Aktifkan User</a>';
                } else {
                  $data_ok[4] .= '<a href="#" onclick="return m_siswa_ur('.$d['id'].');" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-random" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Reset Password</a>';
                }
				*/
	            $data[] = $data_ok;
	        }

	        $json_data = array(
	                    "draw" => $draw,
	                    "iTotalRecords" => $d_total_row,
	                    "iTotalDisplayRecords" => $d_total_row,
	                    "data" => $data
	                );
	        j($json_data);
	        exit;
		} else if ($uri3 == "import") {
			$a['p']	= "f_siswa_import";
		} else {
			$a['p']	= "m_siswa";
		}
		$this->load->view('aaa', $a);
	}
	public function m_guru() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));
		//return as json
		$jeson = array();
		/*
		$a['data'] = $this->db->query("SELECT m_guru.*,
									(SELECT COUNT(id) FROM m_admin WHERE level = 'guru' AND kon_id = m_guru.id) AS ada
									FROM m_guru")->result();
		*/

		if ($uri3 == "det") {
			$a = $this->db->query("SELECT * FROM t_petugas_soal WHERE id = '$uri4'")->row();
			j($a);
			exit();
		} else if ($uri3 == "simpan") {
			$ket 	= "";
			if ($p->id != 0) {
				$this->db->query("UPDATE t_petugas_soal SET nama = '".$p->nama."'
								WHERE id = '".$p->id."'");
				$ket = "edit";
			} else {
				$ket = "tambah";
				$this->db->set("nama",$p->nama);
				$this->db->insert("t_petugas_soal");
			//	$this->db->query("INSERT INTO t_petugas_soal VALUES (null, '".bersih($p,"nama")."')");
			}
			
			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= $ket." sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "hapus") {
			$this->db->query("DELETE FROM t_petugas_soal WHERE id = '".$uri4."'");
			$this->db->query("DELETE FROM t_admin WHERE level = 'petugas' AND kon_id = '".$uri4."'");
			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= "hapus sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "user") {
			$det_user = $this->db->query("SELECT id FROM t_petugas_soal WHERE id = '$uri4'")->row();

			if (!empty($det_user)) {
				$q_cek_username = $this->db->query("SELECT id FROM t_admin WHERE username = 'petugas".$det_user->id."' AND level = 'petugas'")->num_rows();

				if ($q_cek_username < 1) {
					$this->db->query("INSERT INTO t_admin (username, password, level, kon_id) VALUES ('petugas".$det_user->id."', md5('admin'), 'petugas', '".$det_user->id."')");		
					$ret_arr['status'] 	= "ok";
					$ret_arr['caption']	= "tambah user sukses";
					j($ret_arr);
				} else {
					$ret_arr['status'] 	= "gagal";
					$ret_arr['caption']	= "Username telah digunakan";
					j($ret_arr);					
				}
			} else {
				$ret_arr['status'] 	= "gagal";
				$ret_arr['caption']	= "tambah user gagal";
				j($ret_arr);
			}
			exit();
		} else if ($uri3 == "user_reset") {
			$det_user = $this->db->query("SELECT id FROM t_petugas_soal WHERE id = '$uri4'")->row();

			$this->db->query("UPDATE t_admin SET password = md5('petugas".$det_user->id."') WHERE level = 'guru' AND kon_id = '".$det_user->id."'");

			$ret_arr['status'] 	= "ok";
		 	$ret_arr['caption']	= "Update password sukses";
			j($ret_arr);

			exit();
		} else if ($uri3 == "ambil_matkul") {
			$matkul = $this->db->query("SELECT t_jenis_soal.*,
										(SELECT COUNT(id) FROM t_petugas_jenis_soal WHERE id_petugas = ".$uri4." AND id_jenis_soal = t_jenis_soal.id) AS ok
										FROM t_jenis_soal
										")->result();
			$ret_arr['status'] = "ok";
			$ret_arr['data'] = $matkul;
			j($ret_arr);
			exit;
		} else if ($uri3 == "simpan_matkul") {
			$ket 	= "";
			//echo var_dump($p);

			$ambil_matkul = $this->db->query("SELECT id FROM t_jenis_soal ORDER BY id ASC")->result();
			if (!empty($ambil_matkul)) {
				foreach ($ambil_matkul as $a) {
					$p_sub = "id_mapel_".$a->id;

					if (!empty($p->$p_sub)) {
						
						$cek_sudah_ada = $this->db->query("SELECT id FROM t_petugas_jenis_soal WHERE  id_petugas = '".$p->id_mhs."' AND id_jenis_soal = '".$a->id."'")->num_rows();
						

						if ($cek_sudah_ada < 1) {
							$this->db->query("INSERT INTO t_petugas_jenis_soal (id_petugas, id_jenis_soal) VALUES ('".$p->id_mhs."', '".$a->id."')");
						} else {
							$this->db->query("UPDATE t_petugas_jenis_soal SET id_jenis_soal = '".$p->$p_sub."' WHERE id_petugas = '".$p->id_mhs."' AND id_jenis_soal = '".$a->id."'");
						}
					} else {
						//echo "0<br>";
						$this->db->query("DELETE FROM t_petugas_jenis_soal WHERE id_petugas = '".$p->id_mhs."' AND id_jenis_soal = '".$a->id."'");
					}
				}
			}

			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= $ket." sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "data") {
			$start = $this->input->post('start');
	        $length = $this->input->post('length');
	        $draw = $this->input->post('draw');
	        $search = $this->input->post('search');

	        $d_total_row = $this->db->query("SELECT id FROM t_petugas_soal  WHERE nama LIKE '%".$search['value']."%'")->num_rows();
	    
	        $q_datanya = $this->db->query("SELECT t_petugas_soal.*,
											(SELECT COUNT(id) FROM t_admin WHERE level = 'petugas' AND kon_id = t_petugas_soal.id) AS ada
											FROM t_petugas_soal 
	                                        WHERE t_petugas_soal.nama LIKE '%".$search['value']."%' ORDER BY t_petugas_soal.id DESC LIMIT ".$start.", ".$length."")->result_array();
	        $data = array();
	        $no = ($start+1);

	        foreach ($q_datanya as $d) {
	            $data_ok = array();
	            $data_ok[0] = $no++;
	            $data_ok[1] = $d['nama'];
	            
	            $data_ok[2] = '<div class="btn-group">
                          <a href="#" onclick="return m_guru_e('.$d['id'].');" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Edit</a>
                          <a href="#" onclick="return m_guru_h('.$d['id'].');" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Hapus</a>
                          <a href="#" onclick="return m_guru_matkul('.$d['id'].');" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-th-list" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Jenis Soal</a>
                         ';

                if ($d['ada'] == "0") {
                  $data_ok[2] .= '<a href="#" onclick="return m_guru_u('.$d['id'].');" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-user" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Aktif User</a>';
                } else {
                  $data_ok[2] .= '<a href="#" onclick="return m_guru_ur('.$d['id'].');" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-random" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Reset Pass</a>';
                }

	            $data[] = $data_ok;
	        }

	        $json_data = array(
	                    "draw" => $draw,
	                    "iTotalRecords" => $d_total_row,
	                    "iTotalDisplayRecords" => $d_total_row,
	                    "data" => $data
	                );
	        j($json_data);
	        exit;
		} else if ($uri3 == "import") {
			$a['p']	= "f_guru_import";
		} else {
			$a['p']	= "m_guru";
		}
		$this->load->view('aaa', $a);
	}
	public function m_mapel() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));
		//return as json
		$jeson = array();
		$a['data'] = $this->db->query("SELECT t_jenis_soal.* FROM t_jenis_soal")->result();
		if ($uri3 == "det") {
			$a = $this->db->query("SELECT * FROM t_jenis_soal WHERE id = '$uri4'")->row();
			j($a);
			exit();
		} else if ($uri3 == "simpan") {
			$ket 	= "";
			if ($p->id != 0) {
				$this->db->query("UPDATE t_jenis_soal SET jenis_soal = '".$p->jenis_soal."'
								WHERE id = '".$p->id."'");
				$ket = "edit";
			} else {
				$ket = "tambah";
				$this->db->set("jenis_soal",$p->jenis_soal);
				$this->db->insert("t_jenis_soal");
			//	$this->db->query("INSERT INTO t_jenis_soal VALUES (null, '".bersih($p,"jenis_soal")."')");
			}
			
			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= $ket." sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "hapus") {
			$this->db->query("DELETE FROM t_jenis_soal WHERE id = '".$uri4."'");
			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= "hapus sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "data") {
			$start = $this->input->post('start');
	        $length = $this->input->post('length');
	        $draw = $this->input->post('draw');
	        $search = $this->input->post('search');

	        $d_total_row = $this->db->query("SELECT id FROM t_jenis_soal a WHERE a.jenis_soal LIKE '%".$search['value']."%'")->num_rows();
	    
	        $q_datanya = $this->db->query("SELECT a.*
											FROM t_jenis_soal a
	                                        WHERE a.jenis_soal LIKE '%".$search['value']."%' ORDER BY a.id DESC LIMIT ".$start.", ".$length."")->result_array();
	        $data = array();
	        $no = ($start+1);

	        foreach ($q_datanya as $d) {
	            $data_ok = array();
	            $data_ok[0] = $no++;
	            $data_ok[1] = $d['jenis_soal'];
	            $data_ok[2] = '<div class="btn-group">
                          <a href="#" onclick="return m_mapel_e('.$d['id'].');" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Edit</a>
                          <a href="#" onclick="return m_mapel_h('.$d['id'].');" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Hapus</a>
                         ';

	            $data[] = $data_ok;
	        }

	        $json_data = array(
	                    "draw" => $draw,
	                    "iTotalRecords" => $d_total_row,
	                    "iTotalDisplayRecords" => $d_total_row,
	                    "data" => $data
	                );
	        j($json_data);
	        exit;
		} else {
			$a['p']	= "m_mapel";
		}
		$this->load->view('aaa', $a);
	}

	// ruangan
	public function m_ruangan()
	{
		$this->cek_aktif();
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');

		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);

		$p = json_decode(file_get_contents('php://input'));
		$jeson = array();

		$a['data'] = $this->db->query("SELECT * FROM t_ruangan")->result();

		if ($uri3 == "det") {
			$a = $this->db->query("SELECT * FROM t_ruangan WHERE id = '$uri4'")->row();
			j($a);
			exit();
		} else if ($uri3 == "simpan") {
			$ket = "";
			if ($p->id != 0) {
				$this->db->query("UPDATE t_ruangan SET id_ruangan = '".$p->id_ruangan."', ruangan = '".$p->ruangan."', token_ujian = '".$p->token_ujian."' WHERE id = '".$p->id."'");
				$ket = "edit";
				$ret_arr['status'] = "ok";
			} else {
				$ket = "tambah";
				$qrid_rg = $this->db->get_where('t_ruangan', array('id_ruangan' => $p->id_ruangan));
				if ($qrid_rg->num_rows() > 0) {
					$ret_arr['status'] = "oks";
				} else {
					$this->db->set("id_ruangan",$p->id_ruangan);
					$this->db->set("ruangan",$p->ruangan);
					$this->db->set("token_ujian",$p->token_ujian);
					$this->db->insert("t_ruangan");
					$ret_arr['status'] = "ok";
				}
			//	$this->db->query("INSERT INTO t_ruangan VALUES (null, '".bersih($p,"id_ruangan")."', '".bersih($p,"ruangan")."', '".bersih($p,"token_ujian")."')");
			}
			$ret_arr['caption'] = $ket." sukses";
			j($ret_arr);
			exit();
		} else if($uri3 == "hapus") {
			$this->db->query("DELETE FROM t_ruangan WHERE id='".$uri4."'");
			$ret_arr['status'] = "ok";
			$ret_arr['caption'] = " hapus sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "data") {
			$start = $this->input->post('start');
	        $length = $this->input->post('length');
	        $draw = $this->input->post('draw');
	        $search = $this->input->post('search');

	        $d_total_row = $this->db->query("SELECT id FROM t_ruangan  WHERE ruangan LIKE '%".$search['value']."%'")->num_rows();
	    
	        $q_datanya = $this->db->query("SELECT t_ruangan.*
											FROM t_ruangan 
	                                        WHERE ruangan LIKE '%".$search['value']."%' ORDER BY id ASC LIMIT ".$start.", ".$length."")->result_array();
	        $data = array();
	        $no = ($start+1);

	        foreach ($q_datanya as $d) {
	            $data_ok = array();
	            $data_ok[0] = $no++;
	            $data_ok[1] = $d['id_ruangan'];
	            $data_ok[2] = $d['ruangan'];
	            $data_ok[3] = $this->db->query("SELECT id_ruangan FROM t_peserta WHERE id_ruangan = '".$d['id']."'")->num_rows();
	            $data_ok[4] = $d['token_ujian'];
	            $data_ok[5] = '<div class="btn-group">
                          <a href="#" onclick="return m_ruangan_e('.$d['id'].');" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Edit</a>
                          <a href="#" onclick="return m_ruangan_h('.$d['id'].');" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Hapus</a>
                          <a href="'.base_url().'adm/m_ruangan/d/'.$d['id'].'" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-th-list" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Daftar Peserta</a>
                          <a href="'.base_url().'adm/m_ruangan/f/'.$d['id'].'" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-th-list" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Peserta Online</a>
                         ';

	            $data[] = $data_ok;
	        }

	        $json_data = array(
	                    "draw" => $draw,
	                    "iTotalRecords" => $d_total_row,
	                    "iTotalDisplayRecords" => $d_total_row,
	                    "data" => $data
	                );
	        j($json_data);
	        exit;
		} else if($uri3 == "d") {
			$inp_gel = $this->input->post('id_gel'); 
			$a['detail_ruangan'] = $this->db->query("SELECT * FROM t_peserta WHERE id_ruangan = '$uri4'")->result();
			$a['gelombang'] = $this->db->query("SELECT * FROM t_gelombang")->result();

			$a['p'] = "detail_ruangan";
		} else if($uri3 == "f") {
			$rg = $this->db->query("SELECT ruangan FROM t_ruangan WHERE id_ruangan = '$uri4'")->row();
			$a['rg_aktif'] = $rg->ruangan; 
			$a['peserta_online'] = $this->db->query("SELECT * FROM log_masuk WHERE id_ruangan = '$uri4'")->result();
			$a['p'] = "peserta_online";
		} else {
			$a['p'] = "m_ruangan";
		}
		$this->load->view('aaa', $a);
	}

	public function edit_gelombang(){
		$this->load->library('user_agent');
		$gel = $this->input->post('gel');
		$id_gel = $this->input->post('id_gel');
		$this->db->query("UPDATE t_peserta SET id_gelombang = '$gel' WHERE kode = '$id_gel'");
		redirect($this->agent->referrer());
	}

	/* == GURU == */
	public function m_soal() {
		$this->cek_aktif();
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');

		$a['huruf_opsi'] = array("a","b","c","d","e");
		$a['jml_opsi'] = $this->config->item('jml_opsi');

		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$uri5 = $this->uri->segment(5);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));
		//return as json
		$jeson = array();

		if ($a['sess_level'] == "petugas") {
			$a['p_guru'] = obj_to_array($this->db->query("SELECT * FROM t_petugas_soal WHERE id = '".$a['sess_konid']."'")->result(), "id,nama");
			$a['p_mapel'] = obj_to_array($this->db->query("SELECT 
											b.id, b.jenis_soal
											FROM t_petugas_jenis_soal a
											INNER JOIN t_jenis_soal b ON a.id_jenis_soal = b.id
											WHERE a.id_petugas = '".$a['sess_konid']."'")->result(), "id,jenis_soal");
			$a['p_soal'] = $this->db->query("SELECT id, id_petugas, id_jenis_soal, soal FROM t_soal WHERE id_petugas = '".$a['sess_konid']."'")->result();
		} else {
			$a['p_guru'] = obj_to_array($this->db->query("SELECT * FROM t_petugas_soal")->result(), "id,nama");
			$a['p_mapel'] = obj_to_array($this->db->query("SELECT 
											b.id, b.jenis_soal
											FROM t_petugas_jenis_soal a
											INNER JOIN t_jenis_soal b ON a.id_jenis_soal = b.id")->result(), "id,jenis_soal");
			$a['p_soal'] = $this->db->query("SELECT id, id_petugas, id_jenis_soal, soal FROM t_soal")->result();
		}

		if ($uri3 == "det") {
			$a = $this->db->query("SELECT * FROM t_soal WHERE id = '$uri4' ORDER BY id DESC")->row();
			j($a);
			exit();
		} else if ($uri3 == "import") {
			$a['p']	= "f_soal_import";
		} else if($uri3 == "imports") {
			$a['p']	= "f_jawaban_import";
		} else if ($uri3 == "hapus_gambar") {
			$nama_gambar = $this->db->query("SELECT file FROM t_soal WHERE id = '".$uri5."'")->row();
			$this->db->query("UPDATE t_soal SET file = '', tipe_file = '' WHERE id = '".$uri5."'");
			@unlink("./upload/gambar_soal/".$nama_gambar->file);
			redirect('adm/m_soal/pilih_mapel/'.$uri4);
		} else if ($uri3 == "pilih_mapel") {
			if ($a['sess_level'] == "petugas") {
				$a['data'] = $this->db->query("SELECT t_soal.*, t_petugas_soal.nama AS nama_guru FROM t_soal INNER JOIN t_petugas_soal ON t_soal.id_petugas = t_petugas_soal.id WHERE t_soal.id_petugas = '".$a['sess_konid']."' AND t_soal.id_jenis_soal = '$uri4' ORDER BY id DESC")->result();
			} else {
				$a['data'] = $this->db->query("SELECT t_soal.*, t_petugas_soal.nama AS nama_guru FROM t_soal INNER JOIN t_petugas_soal ON t_soal.id_petugas = t_petugas_soal.id WHERE t_soal.id_petugas = '$uri4' ORDER BY id DESC")->result();
			}
			//echo $this->db->last_query();
			$a['p']	= "m_soal";
		} else if ($uri3 == "simpan") {
			$p = $this->input->post();
			$pembuat_soal = ($a['sess_level'] == "admin") ? $p['id_petugas'] : $a['sess_konid'];
			$pembuat_soal_u = ($a['sess_level'] == "admin") ? ", id_petugas = '".$p['id_petugas']."'" : "";
			//etok2nya config
			$folder_gb_soal = "./upload/gambar_soal/";
			$folder_gb_opsi = "./upload/gambar_opsi/";

			$buat_folder_gb_soal = !is_dir($folder_gb_soal) ? @mkdir("./upload/gambar_soal/") : false;
			$buat_folder_gb_opsi = !is_dir($folder_gb_opsi) ? @mkdir("./upload/gambar_opsi/") : false;

			$allowed_type 	= array("image/jpeg", "image/png", "image/gif", 
			"audio/mpeg", "audio/mpg", "audio/mpeg3", "audio/mp3", "audio/x-wav", "audio/wave", "audio/wav",
			"video/mp4", "application/octet-stream");

			$gagal 		= array();
			$nama_file 	= array();
			$tipe_file 	= array();

			//get mode
			$__mode = $p['mode'];
			$__id_soal = 0;
			//ambil data post sementara
			$pdata = array(
				"id_petugas"=>$p['id_petugas'],
				"id_jenis_soal"=>$p['id_jenis_soal'],
				"bobot"=>$p['bobot'],
				"soal"=>$p['soal'],
//				"jawaban"=>$p['jawaban'],
			);

			if ($__mode == "edit") {
				$this->db->where("id", $p['id']);
				$this->db->update("t_soal", $pdata);
				$__id_soal = $p['id'];
			} else {
				$this->db->insert("t_soal", $pdata);
				$get_id_akhir = $this->db->query("SELECT MAX(id) maks FROM t_soal LIMIT 1")->row_array();
				$__id_soal = $get_id_akhir['maks'];
			}

			//mulai dari sini id soal diambil dari variabel $__id

			//lakukan perulangan sejumlah file upload yang terdeteksi
			foreach ($_FILES as $k => $v) {
				//var file upload
				//$k = nama field di form
				$file_name 		= $_FILES[$k]['name'];
				$file_type		= $_FILES[$k]['type'];
				$file_tmp		= $_FILES[$k]['tmp_name'];
				$file_error		= $_FILES[$k]['error'];
				$file_size		= $_FILES[$k]['size'];
				//kode ref file upload jika error
				$kode_file_error = array("File berhasil diupload", "Ukuran file terlalu besar", "Ukuran file terlalu besar", "File upload error", "Tidak ada file yang diupload", "File upload error");
				
				//jika file error = 0 / tidak ada, tipe file ada di file yang diperbolehkan, dan nama file != kosong
				//echo $file_error."<br>".$file_type;
				//exit;
				//echo var_dump($file_error == 0 || in_array($file_type, $allowed_type) || $file_name != "");
				//exit;
				if ($file_error != 0) {
					$gagal[$k] = $kode_file_error[$file_error];
					$nama_file[$k]	= "";
					$tipe_file[$k]	= "";
				} else if (!in_array($file_type, $allowed_type)) {
					$gagal[$k] = "Tipe file ini tidak diperbolehkan..";
					$nama_file[$k]	= "";
					$tipe_file[$k]	= "";
				} else if ($file_name == "") {
					$gagal[$k] = "Tidak ada file yang diupload";
					$nama_file[$k]	= "";
					$tipe_file[$k]	= "";					
				} else {
					$ekstensi = explode(".", $file_name);

					$file_name = $k."_".$__id_soal.".".$ekstensi[1];

					if ($k == "gambar_soal") {
						@move_uploaded_file($file_tmp, $folder_gb_soal.$file_name);
					} else {
						@move_uploaded_file($file_tmp, $folder_gb_opsi.$file_name);
					}

					$gagal[$k]	 	= $kode_file_error[$file_error]; //kode kegagalan upload file
					$nama_file[$k]	= $file_name; //ambil nama file
					$tipe_file[$k]	= $file_type; //ambil tipe file
				}
			}


			//ambil data awal
			$get_opsi_awal = $this->db->query("SELECT opsi_a, opsi_b, opsi_c, opsi_d, opsi_e FROM t_soal WHERE id = '".$__id_soal."'")->row_array();

			$data_simpan = array();

			if (!empty($nama_file['gambar_soal'])) {
				$data_simpan = array(
								"file"=>$nama_file['gambar_soal'],
								"tipe_file"=>$tipe_file['gambar_soal'],
								);
			}

			for ($t = 0; $t < $a['jml_opsi']; $t++) {
				$idx 	= "opsi_".$a['huruf_opsi'][$t];
				$idx2 	= "gj".$a['huruf_opsi'][$t];


				//jika file kosong
				$pc_opsi_awal = explode("#####", $get_opsi_awal[$idx]);
				$nama_file_opsi = empty($nama_file[$idx2]) ? $pc_opsi_awal[0] : $nama_file[$idx2];

				$data_simpan[$idx] = $nama_file_opsi."#####".$p[$idx];
			}

			$this->db->where("id", $__id_soal);
			$this->db->update("t_soal", $data_simpan);

			$teks_gagal = "";
			foreach ($gagal as $k => $v) {
				$arr_nama_file_upload = array("gambar_soal"=>"File Soal ", "gja"=>"File opsi A ", "gjb"=>"File opsi B ", "gjc"=>"File opsi C ", "gjd"=>"File opsi D ", "gje"=>"File opsi E ");
				$teks_gagal .= $arr_nama_file_upload[$k].': '.$v.'<br>';
			}
			$this->session->set_flashdata('k', '<div class="alert alert-info">'.$teks_gagal.'</div>');
			
			redirect('adm/m_soal/pilih_mapel/'.$id_jenis_soal);
		} else if ($uri3 == "edit") {
			$a['opsij'] = array(""=>"Jawaban","A"=>"A","B"=>"B","C"=>"C","D"=>"D","E"=>"E");
			
			$id_petugas = $this->session->userdata('admin_level') == "petugas" ? "WHERE a.id_petugas = '".$a['sess_konid']."'" : "";

			$a['p_mapel'] = obj_to_array($this->db->query("SELECT b.id, b.jenis_soal FROM t_petugas_jenis_soal a INNER JOIN t_jenis_soal b ON a.id_jenis_soal = b.id $id_petugas")->result(),"id,jenis_soal");

			if ($uri4 == 0) {
				$a['d'] = array("mode"=>"add","id"=>"0","id_petugas"=>$id_petugas,"id_jenis_soal"=>"","bobot"=>"","file"=>"","soal"=>"","opsi_a"=>"#####","opsi_b"=>"#####","opsi_c"=>"#####","opsi_d"=>"#####","opsi_e"=>"#####","jawaban"=>"");
			} else {
				$a['d'] = $this->db->query("SELECT t_soal.*, 'edit' AS mode FROM t_soal WHERE id = '$uri4'")->row_array();

			}

			$data = array();

			for ($e = 0; $e < $a['jml_opsi']; $e++) {
				$iidata = array();
				$idx = "opsi_".$a['huruf_opsi'][$e];
				$idx2 = $a['huruf_opsi'][$e];

				$pc_opsi_edit = explode("#####", $a['d'][$idx]);
				$iidata['opsi'] = $pc_opsi_edit[1];
				$iidata['gambar'] = $pc_opsi_edit[0];
				$data[$idx2] = $iidata;
			}


			$a['data_pc'] = $data;
			$a['p'] = "f_soal";

		} else if ($uri3 == "simpan_jwb") {
			$p = $this->input->post();
		//	$pembuat_soal = ($a['sess_level'] == "admin") ? $p['id_petugas'] : $a['sess_konid'];
		//	$pembuat_soal_u = ($a['sess_level'] == "admin") ? ", id_petugas = '".$p['id_petugas']."'" : "";
			//etok2nya config
			$folder_gb_soal = "./upload/gambar_soal/";
			$folder_gb_opsi = "./upload/gambar_opsi/";

			$buat_folder_gb_soal = !is_dir($folder_gb_soal) ? @mkdir("./upload/gambar_soal/") : false;
			$buat_folder_gb_opsi = !is_dir($folder_gb_opsi) ? @mkdir("./upload/gambar_opsi/") : false;

			$allowed_type 	= array("image/jpeg", "image/png", "image/gif", 
			"audio/mpeg", "audio/mpg", "audio/mpeg3", "audio/mp3", "audio/x-wav", "audio/wave", "audio/wav",
			"video/mp4", "application/octet-stream");

			$gagal 		= array();
			$nama_file 	= array();
			$tipe_file 	= array();

			//get mode
			$__mode = $p['mode'];
			$__id_soal = 0;
			//ambil data post sementara
			$pdata = array(
				"id_soal"=>$p['id_soal'],
				"jawaban"=>$p['jawaban'],
				"benar"=>$p['benar'],
//				"soal"=>$p['soal'],
//				"jawaban"=>$p['jawaban'],
			);

			if ($__mode == "edit") {
				$this->db->where("id_jawaban", $p['id']);
				$this->db->update("t_option_soal", $pdata);
				$__id_soal = $p['id'];
			} else {
				$this->db->insert("t_option_soal", $pdata);
				$get_id_akhir = $this->db->query("SELECT MAX(id_jawaban) maks FROM t_option_soal LIMIT 1")->row_array();
				$__id_soal = $get_id_akhir['maks'];
			}

			//mulai dari sini id soal diambil dari variabel $__id

			//lakukan perulangan sejumlah file upload yang terdeteksi
			foreach ($_FILES as $k => $v) {
				//var file upload
				//$k = nama field di form
				$file_name 		= $_FILES[$k]['name'];
				$file_type		= $_FILES[$k]['type'];
				$file_tmp		= $_FILES[$k]['tmp_name'];
				$file_error		= $_FILES[$k]['error'];
				$file_size		= $_FILES[$k]['size'];
				//kode ref file upload jika error
				$kode_file_error = array("File berhasil diupload", "Ukuran file terlalu besar", "Ukuran file terlalu besar", "File upload error", "Tidak ada file yang diupload", "File upload error");
				
				//jika file error = 0 / tidak ada, tipe file ada di file yang diperbolehkan, dan nama file != kosong
				//echo $file_error."<br>".$file_type;
				//exit;
				//echo var_dump($file_error == 0 || in_array($file_type, $allowed_type) || $file_name != "");
				//exit;
				if ($file_error != 0) {
					$gagal[$k] = $kode_file_error[$file_error];
					$nama_file[$k]	= "";
					$tipe_file[$k]	= "";
				} else if (!in_array($file_type, $allowed_type)) {
					$gagal[$k] = "Tipe file ini tidak diperbolehkan..";
					$nama_file[$k]	= "";
					$tipe_file[$k]	= "";
				} else if ($file_name == "") {
					$gagal[$k] = "Tidak ada file yang diupload";
					$nama_file[$k]	= "";
					$tipe_file[$k]	= "";					
				} else {
					$ekstensi = explode(".", $file_name);

					$file_name = $k."_".$__id_soal.".".$ekstensi[1];

					if ($k == "gambar_soal") {
						@move_uploaded_file($file_tmp, $folder_gb_opsi.$file_name);
					} 

					$gagal[$k]	 	= $kode_file_error[$file_error]; //kode kegagalan upload file
					$nama_file[$k]	= $file_name; //ambil nama file
					$tipe_file[$k]	= $file_type; //ambil tipe file
				}
			}


			//ambil data awal
			$get_opsi_awal = $this->db->query("SELECT * FROM t_option_soal WHERE id_jawaban = '".$__id_soal."'")->row_array();

			$data_simpan = array();

			if (!empty($nama_file['gambar_soal'])) {
				$data_simpan = array(
								"file"=>$nama_file['gambar_soal'],
								"tipe_file"=>$tipe_file['gambar_soal'],
								);
			}
/*
			for ($t = 0; $t < $a['jml_opsi']; $t++) {
				$idx 	= "opsi_".$a['huruf_opsi'][$t];
				$idx2 	= "gj".$a['huruf_opsi'][$t];


				//jika file kosong
				$pc_opsi_awal = explode("#####", $get_opsi_awal[$idx]);
				$nama_file_opsi = empty($nama_file[$idx2]) ? $pc_opsi_awal[0] : $nama_file[$idx2];

				$data_simpan[$idx] = $nama_file_opsi."#####".$p[$idx];
			}
*/			
			$this->db->where("id_jawaban", $__id_soal);
			if (!empty($data_simpan)) {
				$this->db->update("t_option_soal", $data_simpan);
			}

			$teks_gagal = "";
			foreach ($gagal as $k => $v) {
				$arr_nama_file_upload = array("gambar_soal"=>"File Jawaban ", "gja"=>"File opsi A ", "gjb"=>"File opsi B ", "gjc"=>"File opsi C ", "gjd"=>"File opsi D ", "gje"=>"File opsi E ");
				$teks_gagal .= $arr_nama_file_upload[$k].': '.$v.'<br>';
			}
			$this->session->set_flashdata('k', '<div class="alert alert-info">'.$teks_gagal.'</div>');
			if ($__mode == "edit") {
				$idSoalKemabali = $this->db->query("select id_soal from t_option_soal where id_jawaban = '".$p['id']."' ")->row();
				redirect('adm/m_soal/jawaban/'.$idSoalKemabali->id_soal);
			}else{
				redirect('adm/m_soal/pilih_mapel/'.$id_jenis_soal);
			}
		} else if ($uri3 == "edit_jwb") {
//			$a['opsij'] = array(""=>"Jawaban","A"=>"A","B"=>"B","C"=>"C","D"=>"D","E"=>"E");
			
//			$id_petugas = $this->session->userdata('admin_level') == "petugas" ? "WHERE a.id_petugas = '".$a['sess_konid']."'" : "";

//			$a['p_mapel'] = obj_to_array($this->db->query("SELECT b.id, b.jenis_soal FROM t_petugas_jenis_soal a INNER JOIN t_jenis_soal b ON a.id_jenis_soal = b.id $id_petugas")->result(),"id,jenis_soal");

			if ($uri4 == 0) {
				$a['d'] = array("mode"=>"add","id_jawaban"=>"0","id_soal"=>"","file"=>"","jawaban"=>"","benar"=>"");
			} else {
				$a['d'] = $this->db->query("SELECT a.*, b.soal,'edit' AS mode FROM t_option_soal as a LEFT JOIN t_soal as b ON a.id_soal = b.id WHERE a.id_jawaban = '$uri4'")->row_array();

			}
			/*
			$data = array();

			for ($e = 0; $e < $a['jml_opsi']; $e++) {
				$iidata = array();
				$idx = "opsi_".$a['huruf_opsi'][$e];
				$idx2 = $a['huruf_opsi'][$e];

				$pc_opsi_edit = explode("#####", $a['d'][$idx]);
				$iidata['opsi'] = $pc_opsi_edit[1];
				$iidata['gambar'] = $pc_opsi_edit[0];
				$data[$idx2] = $iidata;
			}


			$a['data_pc'] = $data;
			*/
			$a['p'] = "f_jawaban";
		} else if($uri3 == "jawaban") {
			$soal = $this->db->query("SELECT * FROM t_soal WHERE id = '$uri4'")->row();
			$a['soal'] = $soal->soal;
			$a['opsi'] = $this->db->query("SELECT * FROM t_option_soal WHERE id_soal = '$uri4'")->result();
			$a['p'] = "v_opsi";
		}else if($uri3 == "jawaban_edit"){
			$soal = $this->db->query("SELECT * FROM t_option_soal WHERE id_jawaban = '$uri4'")->row();
			if ($soal) {
				$a['soal'] = $soal->id_soal;
				$a['opsi'] = $this->db->query("SELECT * FROM t_option_soal WHERE id_soal = $soal->id_soal")->result();
				$a['d'] = $this->db->query("SELECT t_option_soal.*, 'edit' AS mode FROM t_option_soal WHERE id_jawaban = '$uri4'")->row_array();
				$a['options'] = array('B' => 'B', 'S' => 'S');
				$a['p'] = "tambahan/v_opsi_edit";
			}else{
				redirect('adm','refresh');
			}
		} else if ($uri3 == "hapus") {
			$nama_gambar = $this->db->query("SELECT id_jenis_soal, file, opsi_a, opsi_b, opsi_c, opsi_d, opsi_e FROM t_soal WHERE id = '".$uri4."'")->row();
			$pc_opsi_a = explode("#####", $nama_gambar->opsi_a);
			$pc_opsi_b = explode("#####", $nama_gambar->opsi_b);
			$pc_opsi_c = explode("#####", $nama_gambar->opsi_c);
			$pc_opsi_d = explode("#####", $nama_gambar->opsi_d);
			$pc_opsi_e = explode("#####", $nama_gambar->opsi_e);
			$this->db->query("DELETE FROM t_soal WHERE id = '".$uri4."'");
			@unlink("./upload/gambar_soal/".$nama_gambar->file);
			@unlink("./upload/gambar_soal/".$pc_opsi_a[0]);
			@unlink("./upload/gambar_soal/".$pc_opsi_b[0]);
			@unlink("./upload/gambar_soal/".$pc_opsi_c[0]);
			@unlink("./upload/gambar_soal/".$pc_opsi_d[0]);
			@unlink("./upload/gambar_soal/".$pc_opsi_e[0]);
			
			redirect('adm/m_soal/pilih_mapel/'.$nama_gambar->id_jenis_soal);
		} else if ($uri3 == "cetak") {
			$html = "<link href='".base_url()."___/css/style_print.css' rel='stylesheet' media='' type='text/css'/>";
			if ($a['sess_level'] == "admin") {
				$data = $this->db->query("SELECT * FROM t_soal")->result();
			} else {
				$data = $this->db->query("SELECT * FROM t_soal WHERE id_petugas = '".$a['sess_konid']."'")->result();
			}

			$mapel = $this->db->query("SELECT jenis_soal FROM t_jenis_soal WHERE id = '".$uri4."'")->row();
			if (!empty($data)) {
				
				$no = 1;
				$jawaban = array("A","B","C","D","E");
				foreach ($data as $d) {
					
		            $arr_tipe_media = array(""=>"none","image/jpeg"=>"gambar","image/png"=>"gambar","image/gif"=>"gambar",
					"audio/mpeg"=>"audio","audio/mpg"=>"audio","audio/mpeg3"=>"audio","audio/mp3"=>"audio","audio/x-wav"=>"audio","audio/wave"=>"audio","audio/wav"=>"audio",
					"video/mp4"=>"video", "application/octet-stream"=>"video");
		            $tipe_media = $arr_tipe_media[$d->tipe_file];
		            $file_ada = file_exists("./upload/gambar_soal/".$d->file) ? "ada" : "tidak_ada";
		            $tampil_media = "";
		            if ($file_ada == "ada" && $tipe_media == "audio") {
		              $tampil_media = '<<< Ada media Audionya >>>';
		            } else if ($file_ada == "ada" && $tipe_media == "video") {
		              $tampil_media = '<<< Ada media Videonya >>>';
		            } else if ($file_ada == "ada" && $tipe_media == "gambar") {
		              $tampil_media = '<p><img src="'.base_url().'upload/gambar_soal/'.$d->file.'" class="thumbnail" style="width: 300px; height: 280px; display: inline; float: left"></p>';
		            } else {
		              $tampil_media = '';
		            }
	                $html .= '<table>
	                <tr><td>'.$no.'.</td><td colspan="2">'.$d->soal.'<br>'.$tampil_media.'</td></tr>';
	                for ($j=0; $j<($this->config->item('jml_opsi'));$j++) {
	                  	$opsi = "opsi_".strtolower($jawaban[$j]);
	                    $pc_pilihan_opsi = explode("#####", $d->$opsi);
	                    $tampil_media_opsi = (file_exists('./upload/gambar_soal/'.$pc_pilihan_opsi[0]) AND $pc_pilihan_opsi[0] != "") ? '<img src="'.base_url().'upload/gambar_soal/'.$pc_pilihan_opsi[0].'" style="width: 100px; height: 100px; margin-right: 20px">' : '';
	                  if ($jawaban[$j] == $d->jawaban) {
	                    $html .= '<tr><td width="2%" style="font-weight: bold">'.$jawaban[$j].'</td><td style="font-weight: bold">'.$tampil_media_opsi.$pc_pilihan_opsi[1].'</td></label></tr>';
	                  } else {
	                    $html .= '<tr><td width="2%">'.$jawaban[$j].'</td><td>'.$tampil_media_opsi.$pc_pilihan_opsi[1].'</td></label></tr>';
	                  }
	                }
	                $html .= '</table></div>';
		            $no++;
				}
				}

				echo $html;
				exit;
			} else if ($uri3 == "data") {
				$start = $this->input->post('start');
		        $length = $this->input->post('length');
		        $draw = $this->input->post('draw');
		        $search = $this->input->post('search');

		        $wh = '';

		        if ($a['sess_level'] == "petugas") {
					$wh = "a.id_petugas = '".$a['sess_konid']."' AND ";
				} else if ($a['sess_level'] == "admin") {
					$wh = "";
				}


		        $d_total_row = $this->db->query("SELECT a.*
												FROM t_soal a
												INNER JOIN t_petugas_soal b ON a.id_petugas = b.id
												INNER JOIN t_jenis_soal c ON a.id_jenis_soal = c.id
		                                        WHERE ".$wh." (a.soal LIKE '%".$search['value']."%' 
												OR b.nama LIKE '%".$search['value']."%' 
												OR c.jenis_soal LIKE '%".$search['value']."%')")->num_rows();

		        $q_datanya = $this->db->query("SELECT a.*, b.nama nmguru, c.jenis_soal nmmapel
												FROM t_soal a
												INNER JOIN t_petugas_soal b ON a.id_petugas = b.id
												INNER JOIN t_jenis_soal c ON a.id_jenis_soal = c.id
		                                        WHERE ".$wh." (a.soal LIKE '%".$search['value']."%' 
												OR b.nama LIKE '%".$search['value']."%' 
												OR c.jenis_soal LIKE '%".$search['value']."%')
		                                        ORDER BY a.id DESC LIMIT ".$start.", ".$length."")->result_array();
		        //echo $this->db->last_query();
		    
		        $data = array();
		        $no = ($start+1);

		        foreach ($q_datanya as $d) {
		            $data_ok = array();
		            $data_ok[0] = $no++;
		            $data_ok[1] = $d['id'];
		            $data_ok[2] = substr($d['soal'], 0, 300);
		            $data_ok[3] = $d['nmmapel'].'<br>'.$d['nmguru'];
		    //        $data_ok[3] = "Jml dipakai : ".($d['jml_benar']+$d['jml_salah'])."<br>Benar: ".$d['jml_benar'].", Salah: ".$d['jml_salah'];
		            $data_ok[4] = '<div class="btn-group">
	                          <a href="'.base_url().'adm/m_soal/edit/'.$d['id'].'" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Edit</a>
	                          <a href="'.base_url().'adm/m_soal/hapus/'.$d['id'].'" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Hapus</a>
	                        	<a href="'.base_url().'adm/m_soal/jawaban/'.$d['id'].'" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-list" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Daftar Jawaban</a>
	                        </div>';

		            $data[] = $data_ok;
		        }

		        $json_data = array(
		                    "draw" => $draw,
		                    "iTotalRecords" => $d_total_row,
		                    "iTotalDisplayRecords" => $d_total_row,
		                    "data" => $data
		                );
		        j($json_data);
		        exit;
		} else {
			$a['p']	= "m_soal";
		}
		$this->load->view('aaa', $a);
	}

	// atur waktu per ruangan
	public function m_waktu_ujian() {
		$this->cek_aktif();
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');

		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);

		$p = json_decode(file_get_contents('php://input'));
		$jeson = array();

		$a['dt_tes'] = $this->db->query("SELECT a.id, a.id_jenis_soal, b.jenis_soal FROM tr_guru_tes a, t_jenis_soal b WHERE a.id_jenis_soal = b.id")->result();
		$a['gel'] = $this->db->query("SELECT * FROM t_gelombang")->result();
		$a['ruangan'] = $this->db->query("SELECT * FROM t_ruangan")->result();

		if ($uri3 == "det") {
			$are = array();
			$a = $this->db->query("SELECT * FROM t_waktu_ruangan WHERE id = '$uri4'")->row();
			if (!empty($a)) {
				$pc_waktu = explode(" ", $a->tgl_mulai);
				$pc_tgl = explode("-", $pc_waktu[0]);

				$are['id'] = $a->id;
				$are['id_sesi'] = $a->id_sesi;
				$are['id_ruangan'] = $a->id_ruangan;
				$are['id_gelombang'] = $a->id_gelombang;
				$are['terlambat'] = $a->terlambat;
				$are['tgl_mulai'] = $pc_waktu[0];
				$are['wkt_mulai'] = substr($pc_waktu[1],0,5);
			} else {
				$are['id'] = "";
				$are['id_sesi'] = "";
				$are['id_ruangan'] = "";
				$are['id_gelombang'] = "";
				$are['terlambat'] = "";
				$are['tgl_mulai'] = "";
				$are['wkt_mulai'] = "";
			}
			j($are);
			exit();
		} else if ($uri3 == "simpan") {
			$ket = "";
			if ($p->id != 0) {
				$this->db->query("UPDATE t_waktu_ruangan SET id_sesi = '".$p->id_sesi."', id_ruangan = '".$p->id_ruangan."', id_gelombang = '".$p->id_gelombang."', tgl_mulai = '".$p->tgl_mulai." ".$p->wkt_mulai."', terlambat = '".$p->terlambat."' WHERE id = '".$p->id."'");
				$ket = "edit";	
			} else {
				$ket = "tambah";
				$this->db->query("INSERT INTO t_waktu_ruangan (id_sesi, id_ruangan, id_gelombang, tgl_mulai, terlambat) VALUES ('".$p->id_sesi."', '".$p->id_ruangan."', '".$p->id_gelombang."', '".$p->tgl_mulai." ".$p->wkt_mulai."', '".$p->terlambat."')");
			}
			$ret_arr['status'] = "ok";
			$ret_arr['caption'] = $ket." sukses";
			j($ret_arr);
			exit();
		} else if($uri3 == "hapus"){
			$this->db->query("DELETE FROM t_waktu_ruangan WHERE id = '".$uri4."'");
			$ret_arr['status'] = "ok";
			$ret_arr['caption'] = "hapus sukses";
			j($ret_arr);
			exit();
		} else if($uri3 == "data") {
			$start = $this->input->post('start');
			$length = $this->input->post('length');
			$draw = $this->input->post('draw');
			$search = $this->input->post('search');

			$d_total_row = $this->db->query("SELECT a.id FROM t_waktu_ruangan a INNER JOIN t_ruangan c ON a.id_ruangan = c.id_ruangan WHERE c.ruangan LIKE '%".$search['value']."%'")->num_rows();

			$q_datanya = $this->db->query("SELECT a.*, b.jenis_soal AS sesi, c.ruangan AS rg, d.gelombang AS gel
							FROM t_waktu_ruangan a
							INNER JOIN t_jenis_soal b ON a.id_sesi = b.id
							INNER JOIN t_ruangan c ON a.id_ruangan = c.id_ruangan
							INNER JOIN t_gelombang d ON a.id_gelombang = d.id_gelombang
							WHERE c.ruangan LIKE '%".$search['value']."%'
							ORDER BY a.id ASC LIMIT ".$start.", ".$length."")->result_array();
			$data = array();
			$no = ($start+1);

			foreach ($q_datanya as $d) {
				$data_ok = array();
				$data_ok[0] = $no++;
				$data_ok[1] = $d['sesi'];
				$data_ok[2] = $d['rg'];
				$data_ok[3] = "Sesi ".$d['gel'];
				$data_ok[4] = tjs($d['tgl_mulai'],"s");
				$data_ok[5] = $d['terlambat']." Menit";
				$data_ok[6] = ' 
				<div class="btn-group"> 
					<a href="#" onclick="return m_waktu_ujian_e('.$d['id'].');" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Edit</a>
					<a href="#" onclick="return m_waktu_ujian_h('.$d['id'].');" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Hapus</a>
				</div>';
				$data[] = $data_ok; 				
			}
			$json_data = array(
						 "draw" => $draw,
						 "iTotalRecords" => $d_total_row,
						 "iTotalDisplayRecords" => $d_total_row,
						 "data" => $data
							);
			j($json_data);
			exit;
		} else {
			$a['p'] = "m_ujian_waktu";	
		}

		$this->load->view('aaa', $a);
	}

	public function m_ujian() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));
		//return as json
		$jeson = array();
		
		//$a['data'] = $this->db->query("SELECT tr_guru_tes.*, m_mapel.nama AS mapel FROM tr_guru_tes INNER JOIN m_mapel ON tr_guru_tes.id_mapel = m_mapel.id WHERE tr_guru_tes.id_guru = '".$a['sess_konid']."'")->result();

		$a['pola_tes'] = array(""=>"Pengacakan Soal", "acak"=>"Soal Diacak", "set"=>"Soal Diurutkan");

		$a['p_mapel'] = obj_to_array($this->db->query("SELECT * FROM t_jenis_soal")->result(), "id,jenis_soal");
		
		if ($uri3 == "det") {
			$are = array();

			$a = $this->db->query("SELECT * FROM tr_guru_tes WHERE id = '$uri4'")->row();
			
			if (!empty($a)) {
//				$pc_waktu = explode(" ", $a->tgl_mulai);
//				$pc_tgl = explode("-", $pc_waktu[0]);

				$are['id'] = $a->id;
//				$are['id_petugas'] = $a->id_petugas;
				$are['id_jenis_soal'] = $a->id_jenis_soal;
//				$are['nama_ujian'] = $a->nama_ujian;
				$are['jumlah_soal'] = $a->jumlah_soal;
				$are['waktu'] = $a->waktu;
//				$are['terlambat'] = $a->terlambat;
				$are['jenis'] = $a->jenis;
				$are['detil_jenis'] = $a->detil_jenis;
//				$are['tgl_mulai'] = $pc_waktu[0];
//				$are['wkt_mulai'] = substr($pc_waktu[1],0,5);
//				$are['token'] = $a->token;
			} else {
				$are['id'] = "";
//				$are['id_petugas'] = "";
				$are['id_jenis_soal'] = "";
//				$are['nama_ujian'] = "";
				$are['jumlah_soal'] = "";
				$are['waktu'] = "";
//				$are['terlambat'] = "";
				$are['jenis'] = "";
				$are['detil_jenis'] = "";
//				$are['tgl_mulai'] = "";
//				$are['wkt_mulai'] = "";
//				$are['token'] = "";
			}

			j($are);
			exit();
		} else if ($uri3 == "simpan") {
			$ket 	= "";

			if ($p->id != 0) {
				$this->db->query("UPDATE tr_guru_tes SET id_jenis_soal = '".$p->jenis_soal."', 
								jumlah_soal = '".$p->jumlah_soal."', 
								waktu = '".$p->waktu."', jenis = '".$p->acak."'
								WHERE id = '".$p->id."'");
				$ket = "edit";
			} else {
				$ket = "tambah";
		//		$token = strtoupper(random_string('alpha', 5));
				
				$this->db->query("INSERT INTO tr_guru_tes (id_jenis_soal, jumlah_soal, waktu, jenis) VALUES ('".$p->jenis_soal."',
								'".$p->jumlah_soal."', '".$p->waktu."', '".$p->acak."')");
			}
			
			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= $ket." sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "hapus") {
			$this->db->query("DELETE FROM tr_guru_tes WHERE id = '".$uri4."'");
			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= "hapus sukses";
			j($ret_arr);
			exit();
		} else if ($uri3 == "jumlah_soal") {
			$ambil_data = $this->db->query("SELECT id FROM t_soal WHERE id_jenis_soal = '$uri4'")->num_rows();
			$ret_arr['jumlah'] = $ambil_data;
			j($ret_arr);
			exit();			
		} else if ($uri3 == "data") {
				$start = $this->input->post('start');
		        $length = $this->input->post('length');
		        $draw = $this->input->post('draw');
		        $search = $this->input->post('search');

		        $d_total_row = $this->db->query("SELECT a.id
		        	FROM tr_guru_tes a
		        	INNER JOIN t_jenis_soal b ON a.id_jenis_soal = b.id 
		        	WHERE b.jenis_soal LIKE '%".$search['value']."%'")->num_rows();
		    	
		    	//echo $this->db->last_query();

		        $q_datanya = $this->db->query("SELECT a.*, b.jenis_soal AS mapel
												FROM tr_guru_tes a
									        	INNER JOIN t_jenis_soal b ON a.id_jenis_soal = b.id 
									        	WHERE  b.jenis_soal LIKE '%".$search['value']."%' 
		                                        ORDER BY a.id ASC LIMIT ".$start.", ".$length."")->result_array();
		        $data = array();
		        $no = ($start+1);

		        foreach ($q_datanya as $d) {
		        	$jenis_soal = $d['jenis'] == "acak" ? "Soal diacak" : "Soal urut";
                
		            $data_ok = array();
		            $data_ok[0] = $no++;
		    //        $data_ok[1] = $d['nama_ujian']."<br>Token : <b>".$d['token']."</b> &nbsp;&nbsp; <a href='#' onclick='return refresh_token(".$d['id'].")' title='Perbarui Token'><i class='fa fa-refresh'></i></a>";
		            $data_ok[1] = $d['mapel'];
		            $data_ok[2] = $d['jumlah_soal'];
		  	        $data_ok[3] = $d['waktu']." (menit)";
		            $data_ok[4] = $jenis_soal;
		            $data_ok[5] = '
		            	<div class="btn-group">
                          <a href="#" onclick="return m_ujian_e('.$d['id'].');" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Edit</a>
                          <a href="#" onclick="return m_ujian_h('.$d['id'].');" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Hapus</a>
                        </div>
	                         ';

		            $data[] = $data_ok;
		        }

		        $json_data = array(
		                    "draw" => $draw,
		                    "iTotalRecords" => $d_total_row,
		                    "iTotalDisplayRecords" => $d_total_row,
		                    "data" => $data
		                );
		        j($json_data);
		        exit;
		} else if ($uri3 == "refresh_token") {
			$token = strtoupper(random_string('alpha', 5));

			$this->db->query("UPDATE tr_guru_tes SET token = '$token' WHERE id = '$uri4'");

			$ret_arr['status'] = "ok";
			j($ret_arr);
			exit();
		} else {
			$a['p']	= "m_guru_tes";
		}
		$this->load->view('aaa', $a);
	}
	public function h_ujian() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$uri5 = $this->uri->segment(5);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));
		//return as json
		$jeson = array();

		$wh_1 = $a['sess_level'] == "admin" ;
		//$a['data'] = $this->db->query($wh_1)->result();
		

		$a['p_mapel'] = obj_to_array($this->db->query("SELECT * FROM t_jenis_soal")->result(), "id,jenis_soal");
		
		if ($uri3 == "det") {
			$a['detil_tes'] = $this->db->query("SELECT t_jenis_soal.jenis_soal AS namaMapel,  
												tr_guru_tes.* 
												FROM tr_guru_tes 
												INNER JOIN t_jenis_soal ON tr_guru_tes.id_jenis_soal = t_jenis_soal.id
												WHERE tr_guru_tes.id_jenis_soal = '$uri4'")->row();
			$a['statistik'] = $this->db->query("SELECT MAX(nilai) AS max_, MIN(nilai) AS min_, AVG(nilai) AS avg_ 
											FROM t_log_soal
											WHERE t_log_soal.kd_soal = '$uri4'")->row();

			//$a['hasil'] = $this->db->query("")->result();
			$a['p'] = "m_guru_tes_hasil_detil";
			//echo $this->db->last_query();
		} else if ($uri3 == "data_det") {
			$start = $this->input->post('start');
	        $length = $this->input->post('length');
	        $draw = $this->input->post('draw');
	        $search = $this->input->post('search');

	        $d_total_row = $this->db->query("
	        	SELECT a.id
				FROM t_log_soal a
				INNER JOIN t_peserta b ON a.kd_peserta = b.kode
				WHERE a.kd_soal = '$uri4' 
				AND b.nama_peserta LIKE '%".$search['value']."%'")->num_rows();

	        $q_datanya = $this->db->query("
	        	SELECT a.id, b.kode, b.nama_peserta, a.nilai, a.jml_benar, a.jml_salah, a.nilai_bobot
				FROM t_log_soal a
				INNER JOIN t_peserta b ON a.kd_peserta = b.kode
				WHERE a.kd_soal = '$uri4' 
				AND b.nama_peserta LIKE '%".$search['value']."%' ORDER BY a.nilai DESC LIMIT ".$start.", ".$length."")->result_array();

	        $data = array();
	        $no = ($start+1);


	        foreach ($q_datanya as $d) {
	            $data_ok = array();
	            $data_ok[0] = $no++;

	            $data_ok[1] = $d['kode'];
	            $data_ok[2] = $d['nama_peserta'];
	            $data_ok[3] = $d['jml_benar'];
	            $data_ok[4] = $d['jml_salah'];
	            $data_ok[5] = $d['nilai'];
	    //        $data_ok[5] = $d['nilai_bobot'];
	            $data_ok[6] = '<a href="'.base_url().'adm/h_ujian/batalkan_ujian/'.$d['id'].'/'.$this->uri->segment(4).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Anda yakin...?\');"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Batalkan Ujian</a>
	            			   <a href="'.base_url().'adm/h_ujian/detail_soal_jawaban/'.$d['id'].'" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-list"></i> &nbsp;&nbsp;Detail</a>';
	            
	            $data[] = $data_ok;
	        }

	        $json_data = array(
	                    "draw" => $draw,
	                    "iTotalRecords" => $d_total_row,
	                    "iTotalDisplayRecords" => $d_total_row,
	                    "data" => $data
	                );
	        j($json_data);
	        exit;
		} else if ($uri3 == "detail_soal_jawaban") 
		{
			$data_user = $this->db->query("SELECT a.id, b.kode, b.nama_peserta
							FROM t_log_soal a
							INNER JOIN t_peserta b ON a.kd_peserta = b.kode
							WHERE a.id = '$uri4'")->row();
			$a['kd'] = $data_user->kode;
			$a['nm'] = $data_user->nama_peserta;
			// ambil field soal
			$dt = $this->db->query("SELECT list_soal, list_jawaban FROM t_log_soal WHERE id = '$uri4'")->row();
			$ds = $dt->list_soal;
			$dj = $dt->list_jawaban;
			// buat array list_soal
    		$a['data_soal'] = explode(",", $ds);
    		$a['data_jawaban'] = explode(",", $dj);
			$data_soalnya = explode(",", $ds);
			// hitung jumlah array 
			$a['jum_data'] = count($data_soalnya);
		//	$jm_data = count($data_soalnya);

		//	$a['soal_jawaban'] = $this->db->query("SELECT soal FROM t_soal WHERE id=''")->row();
			$a['p'] = "v_detail_soal_jawaban";
		} else if ($uri3 == "batalkan_ujian") {
			$this->db->query("DELETE FROM t_log_soal WHERE id = '$uri4'");
			redirect('adm/h_ujian/det/'.$uri5);
		} else if ($uri3 == "data") {
			$start = $this->input->post('start');
	        $length = $this->input->post('length');
	        $draw = $this->input->post('draw');
	        $search = $this->input->post('search');

	        $d_total_row = $this->db->query("SELECT a.id FROM tr_guru_tes a
	        	INNER JOIN t_jenis_soal b ON a.id_jenis_soal = b.id 
	            WHERE b.jenis_soal LIKE '%".$search['value']."%'")->num_rows();
	    	//echo $this->db->last_query();

	        $q_datanya = $this->db->query("SELECT a.*, b.jenis_soal AS mapel FROM tr_guru_tes a
	        	INNER JOIN t_jenis_soal b ON a.id_jenis_soal = b.id 
	            WHERE b.jenis_soal LIKE '%".$search['value']."%' ORDER BY a.id ASC LIMIT ".$start.", ".$length."")->result_array();

	        $data = array();
	        $no = ($start+1);


	        foreach ($q_datanya as $d) {
	            $data_ok = array();
	            $data_ok[0] = $no++;
	//            $data_ok[1] = $d['nama_ujian'];
	//            $data_ok[2] = $d['nama_guru'];
	            $data_ok[1] = $d['mapel'];
	            $data_ok[2] = $d['jumlah_soal'];
	            $data_ok[3] = $d['waktu']." menit";
	            $data_ok[4] = '<a href="'.base_url().'adm/h_ujian/det/'.$d['id_jenis_soal'].'" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-search" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Lihat Hasil</a>
                         ';

	            $data[] = $data_ok;
	        }

	        $json_data = array(
	                    "draw" => $draw,
	                    "iTotalRecords" => $d_total_row,
	                    "iTotalDisplayRecords" => $d_total_row,
	                    "data" => $data
	                );
	        j($json_data);
	        exit;
		} else {
			$a['p']	= "m_guru_tes_hasil";
		}


		$this->load->view('aaa', $a);
	}
	public function hasil_ujian_cetak() {
		$this->cek_aktif();
		
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$a['detil_tes'] = $this->db->query("SELECT t_jenis_soal.jenis_soal AS namaMapel,  
												tr_guru_tes.* 
												FROM tr_guru_tes 
												INNER JOIN t_jenis_soal ON tr_guru_tes.id_jenis_soal = t_jenis_soal.id
												WHERE tr_guru_tes.id_jenis_soal = '$uri3'")->row();
		
		$a['statistik'] = $this->db->query("SELECT MAX(nilai) AS max_, MIN(nilai) AS min_, AVG(nilai) AS avg_ 
										FROM t_log_soal
										WHERE t_log_soal.kd_soal = '$uri3'")->row();
		$a['hasil'] = $this->db->query("SELECT t_peserta.kode, t_peserta.nama_peserta, t_log_soal.nilai, t_log_soal.jml_benar, t_log_soal.jml_salah, t_log_soal.nilai_bobot
										FROM t_log_soal
										INNER JOIN t_peserta ON t_log_soal.kd_peserta = t_peserta.kode
										WHERE t_log_soal.kd_soal = '$uri3' ORDER BY t_log_soal.nilai DESC")->result();
		$this->load->view("m_guru_tes_hasil_detil_cetak", $a);
	}
	/* == SISWA == */
	public function ikuti_ujian() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_nama');
//		$a['sess_konid'] = $this->session->userdata('admin_konid');
		$a['sess_kode'] = $this->session->userdata('admin_kode');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));
		//return as json
		$jeson = array();
		//$a['sess_konid']
		$a['data'] = $this->db->query("SELECT 
									a.*, x.ruangan rg, y.gelombang gl, b.jumlah_soal, b.waktu,
									c.jenis_soal nmmapel,
									IF((d.status='Y' AND NOW() BETWEEN d.waktu_mulai AND d.waktu_selesai),'Sedang Tes',
									IF(d.status='Y' AND NOW() NOT BETWEEN d.waktu_mulai AND d.waktu_selesai,'Sedang Tes',
									IF(d.status='N','Selesai','Belum Ikut'))) status 
									FROM t_waktu_ruangan a
									INNER JOIN tr_guru_tes b ON a.id_sesi = b.id_jenis_soal
									INNER JOIN t_jenis_soal c ON a.id_sesi = c.id
									INNER JOIN t_ruangan x ON a.id_ruangan = x.id_ruangan
									INNER JOIN t_gelombang y ON a.id_gelombang = y.id_gelombang
									LEFT JOIN t_log_soal d ON CONCAT('".$a['sess_kode']."',a.id_sesi) = CONCAT(d.kd_peserta,d.kd_soal)
									WHERE a.id_ruangan IN (SELECT id_ruangan FROM t_peserta WHERE kode = ".$a['sess_kode'].") AND a.id_gelombang IN (SELECT id_gelombang FROM t_peserta WHERE kode = ".$a['sess_kode'].") AND a.id_sesi IN (SELECT id_jenis_soal FROM t_soal_peserta WHERE kd_peserta = ".$a['sess_kode'].")
									ORDER BY a.id ASC")->result();
		//echo $this->db->last_query();
		$a['p']	= "m_list_ujian_siswa";
		$this->load->view('aaa', $a);
	}
	public function ikut_ujian() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_nama');
//		$a['sess_konid'] = $this->session->userdata('admin_konid');
		$a['sess_kode'] = $this->session->userdata('admin_kode');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));
		$a['detil_user'] = $this->db->query("SELECT * FROM t_peserta WHERE kode = '".$a['sess_kode']."'")->row();
		if ($uri3 == "simpan_satu") {
			$p			= json_decode(file_get_contents('php://input'));
			
			$update_ 	= "";
			for ($i = 1; $i < $p->jml_soal; $i++) {
				$wktu       = date('Y-m-d H:i:s'); 
				$_tjawab 	= "opsi_".$i;
				$_tidsoal 	= "id_soal_".$i;
				$jawaban_ 	= empty($p->$_tjawab) ? "X" : $p->$_tjawab;
				$update_	.= "".$p->$_tidsoal.":".$jawaban_.",";
			}
			$update_		= substr($update_, 0, -1);
			$this->db->query("UPDATE t_log_soal SET list_jawaban = '".$update_."', waktu = '".$wktu."' WHERE kd_soal = '$uri4' AND kd_peserta = '".$a['sess_kode']."'");
			//echo $this->db->last_query();

			$q_ret_urn 	= $this->db->query("SELECT list_jawaban FROM t_log_soal WHERE kd_soal = '$uri4' AND kd_peserta = '".$a['sess_kode']."'");
			
			$d_ret_urn 	= $q_ret_urn->row_array();
			$ret_urn 	= explode(",", $d_ret_urn['list_jawaban']);
			$hasil 		= array();
			foreach ($ret_urn as $key => $value) {
				$pc_ret_urn = explode(":", $value);
				$idx 		= $pc_ret_urn['0'];
				$val 		= $pc_ret_urn['1'];
				$hasil[]= $val;
			}

			$d['data'] = $hasil;
			$d['status'] = "ok";

			j($d);
			exit;		

		} else if ($uri3 == "simpan_akhir") {
			$p			= json_decode(file_get_contents('php://input'));
			
			$jumlah_soal = $p->jml_soal;
			$jumlah_benar = 0;
			$jumlah_bobot = 0;
			$jumlah_salah = 0;
			$update_ = "";
			//nilai bobot 
			for ($i = 1; $i < $p->jml_soal; $i++) {
				$_tjawab 	= "opsi_".$i;
				$_tidsoal 	= "id_soal_".$i;
				$jawaban_ 	= empty($p->$_tjawab) ? "X" : $p->$_tjawab;
				$cek_jwb 	= $this->db->query("SELECT * FROM t_option_soal WHERE id_soal = '".$p->$_tidsoal."' AND benar = 'B'")->row();
				$cek_bobot	= $this->db->query("SELECT bobot FROM t_soal WHERE id = '".$p->$_tidsoal."'")->row();
				
				if ($jawaban_ == "X")  {
					$jumlah_benar = $jumlah_benar + 0;
				}
				else if ($jawaban_ == $cek_jwb->id_jawaban) {
					$jumlah_benar++;
				    $jumlah_bobot = $jumlah_bobot + $cek_bobot->bobot;
				} else {
					$jumlah_salah++;
				}
				$update_	.= "".$p->$_tidsoal.":".$jawaban_.",";
			}
			$update_		= substr($update_, 0, -1);

			$nilai = $jumlah_bobot;
			$this->db->query("UPDATE t_log_soal SET jml_benar = ".$jumlah_benar.", jml_salah = ".$jumlah_salah.", nilai_bobot = ".$jumlah_bobot.", nilai = '".$nilai."', list_jawaban = '".$update_."', status = 'N' WHERE kd_soal = '$uri4' AND kd_peserta = '".$a['sess_kode']."'");
			$a['status'] = "ok";
			j($a);
			exit;		
		} else if ($uri3 == "token") {
			$a['du'] = $this->db->query("SELECT a.id, a.id_sesi, a.tgl_mulai, a.terlambat, 
										b.jumlah_soal js, b.waktu wkt,
										c.jenis_soal nmmapel FROM t_waktu_ruangan a 
										INNER JOIN tr_guru_tes b ON a.id_sesi = b.id_jenis_soal
										INNER JOIN t_jenis_soal c ON a.id_sesi = c.id 
										WHERE a.id = '$uri4'")->row_array();
			$a['dp'] = $this->db->query("SELECT * FROM t_peserta WHERE kode = '".$a['sess_kode']."'")->row_array();

			if (!empty($a['du']) || !empty($a['dp'])) {
				$tgl_selesai = $a['du']['tgl_mulai'];
			    $tgl_selesai = strtotime($tgl_selesai);
			    $tgl_baru = date('F j, Y H:i:s', $tgl_selesai);

			    $tgl_terlambat = strtotime("+".$a['du']['terlambat']." minutes", $tgl_selesai);	
				$tgl_terlambat_baru = date('F j, Y H:i:s', $tgl_terlambat);

				$a['tgl_mulai'] = $tgl_baru;
				$a['terlambat'] = $tgl_terlambat_baru;

				$a['p']	= "m_token";
				$this->load->view('aaa', $a);
			} else {
				redirect('adm/ikuti_ujian');
			}
		} else if($uri3 == "tokenic") {
			$a['du'] = $this->db->query("SELECT a.id, a.id_sesi, a.tgl_mulai, a.terlambat, 
										b.jumlah_soal js, b.waktu wkt,
										c.jenis_soal nmmapel FROM t_waktu_ruangan a 
										INNER JOIN tr_guru_tes b ON a.id_sesi = b.id_jenis_soal
										INNER JOIN t_jenis_soal c ON a.id_sesi = c.id 
										WHERE a.id = '$uri4'")->row_array();
			$a['dp'] = $this->db->query("SELECT * FROM t_peserta WHERE kode = '".$a['sess_kode']."'")->row_array();
	//		$this->db->query("UPDATE t_log_soal SET list_jawaban = '".$update_."', waktu = '".$wktu."' WHERE kd_soal = '$uri4' AND kd_peserta = '".$a['sess_kode']."'");
			// waktu tersimpan terakhir di db
			$waktu_akhir = $this->db->query("SELECT waktu_selesai FROM t_log_soal WHERE kd_peserta = '".$a['sess_kode']."'")->row();
			$time1 = $this->db->query("SELECT waktu FROM t_log_soal WHERE kd_peserta = '".$a['sess_kode']."'")->row();
			$time_now = date('Y-m-d H:i:s');
			$time1_unix = strtotime($time1->waktu);
			$time_now_unix = strtotime($time_now);
			$begin_day_unix = strtotime(date('Y-m-d').'00:00:00');
			$jumlah_time = date('Y-m-d H:i:s', ($time_now_unix - ($time1_unix - $begin_day_unix)));
			$waktu_akhir_unix = strtotime($waktu_akhir->waktu_selesai);
			$jumlah_time_unix = strtotime($jumlah_time);
			$waktu_akhir_kerja = date('Y-m-d H:i:s', ($waktu_akhir_unix + ($jumlah_time_unix - $begin_day_unix)));
			$this->db->query("UPDATE t_log_soal SET waktu_selesai = '$waktu_akhir_kerja' WHERE kd_peserta = '".$a['sess_kode']."'");
			if (!empty($a['du']) || !empty($a['dp'])) {
				$tgl_selesai = $a['du']['tgl_mulai'];
			    $tgl_selesai = strtotime($tgl_selesai);
			    $tgl_baru = date('F j, Y H:i:s', $tgl_selesai);

			    $tgl_terlambat = strtotime("+".$a['du']['terlambat']." minutes", $tgl_selesai);	
				$tgl_terlambat_baru = date('F j, Y H:i:s', $tgl_terlambat);

				$a['tgl_mulai'] = $tgl_baru;
				$a['terlambat'] = $tgl_terlambat_baru;

				$a['p']	= "m_tokenic";
				$this->load->view('aaa', $a);
			} else {
				redirect('adm/ikuti_ujian');
			}
		} else {
			$cek_sdh_selesai= $this->db->query("SELECT id FROM t_log_soal WHERE kd_soal = '$uri4' AND kd_peserta = '".$a['sess_kode']."' AND status = 'N'")->num_rows();
			
			//sekalian validasi waktu sudah berlalu...
			if ($cek_sdh_selesai < 1) {
				//ini jika ujian belum tercatat, belum ikut
				//ambil detil soal
				$cek_detil_tes = $this->db->query("SELECT * FROM tr_guru_tes WHERE id_jenis_soal = '$uri4'")->row();
				$q_cek_sdh_ujian= $this->db->query("SELECT id FROM t_log_soal WHERE kd_soal = '$uri4' AND kd_peserta = '".$a['sess_kode']."'");
				$d_cek_sdh_ujian= $q_cek_sdh_ujian->row();
				$cek_sdh_ujian	= $q_cek_sdh_ujian->num_rows();
				$acakan = $cek_detil_tes->jenis == "acak" ? "ORDER BY RAND()" : "ORDER BY id ASC";

				if ($cek_sdh_ujian < 1)	{		
					$soal_urut_ok = array();
					$q_soal			= $this->db->query("SELECT id, file, tipe_file, soal, opsi_a, opsi_b, opsi_c, opsi_d, opsi_e, '' AS jawaban FROM t_soal WHERE id_jenis_soal = '".$cek_detil_tes->id_jenis_soal."'".$acakan." LIMIT ".$cek_detil_tes->jumlah_soal)->result();
					$i = 0;
					foreach ($q_soal as $s) {
						$soal_per = new stdClass();
						$soal_per->id = $s->id;
						$soal_per->soal = $s->soal;
						$soal_per->file = $s->file;
						$soal_per->tipe_file = $s->tipe_file;
						$soal_per->opsi_a = $s->opsi_a;
						$soal_per->opsi_b = $s->opsi_b;
						$soal_per->opsi_c = $s->opsi_c;
						$soal_per->opsi_d = $s->opsi_d;
						$soal_per->opsi_e = $s->opsi_e;
						$soal_per->jawaban = $s->jawaban;
						$soal_urut_ok[$i] = $soal_per;
						$i++;
					}
					$soal_urut_ok = $soal_urut_ok;
					$list_id_soal	= "";
					$list_jw_soal 	= "";
					if (!empty($q_soal)) {
						foreach ($q_soal as $d) {
							$list_id_soal .= $d->id.",";
							$list_jw_soal .= $d->id.":,";
						}
					}
					$list_id_soal = substr($list_id_soal, 0, -1);
					$list_jw_soal = substr($list_jw_soal, 0, -1);
					$waktu_selesai = tambah_jam_sql($cek_detil_tes->waktu);
					$time_mulai		= date('Y-m-d H:i:s');
					$this->db->query("INSERT INTO t_log_soal (kd_soal, kd_peserta, list_soal, list_jawaban, jml_benar, jml_salah, nilai, nilai_bobot, waktu_mulai, waktu_selesai, waktu, status) VALUES ('$uri4', '".$a['sess_kode']."', '$list_id_soal', '$list_jw_soal', 0, 0, 0, 0, '$time_mulai', ADDTIME('$time_mulai', '$waktu_selesai'), '$time_mulai', 'Y')");
					
					$detil_tes = $this->db->query("SELECT * FROM t_log_soal WHERE kd_soal = '$uri4' AND kd_peserta = '".$a['sess_kode']."'")->row();

					$soal_urut_ok= $soal_urut_ok;
				} else {
					$q_ambil_soal 	= $this->db->query("SELECT * FROM t_log_soal WHERE kd_soal = '$uri4' AND kd_peserta = '".$a['sess_kode']."'")->row();

					$urut_soal 		= explode(",", $q_ambil_soal->list_jawaban);
					$soal_urut_ok	= array();
					for ($i = 0; $i < sizeof($urut_soal); $i++) {
						$pc_urut_soal = explode(":",$urut_soal[$i]);
						$pc_urut_soal1 = empty($pc_urut_soal[1]) ? "''" : "'".$pc_urut_soal[1]."'";
						$ambil_soal = $this->db->query("SELECT *, $pc_urut_soal1 AS jawaban FROM t_soal WHERE id = '".$pc_urut_soal[0]."'")->row();
						$soal_urut_ok[] = $ambil_soal; 
					}
					
					$detil_tes = $q_ambil_soal;

					$soal_urut_ok = $soal_urut_ok;
				}


				$pc_list_jawaban = explode(",", $detil_tes->list_jawaban);

				$arr_jawab = array();
				foreach ($pc_list_jawaban as $v) {
				  $pc_v = explode(":", $v);
				  $idx = $pc_v[0];
				  $val = $pc_v[1];

				  $arr_jawab[$idx] = $val;
				}

				$html = '';
				$no = 1;
				if (!empty($soal_urut_ok)) {
				    foreach ($soal_urut_ok as $d) { 
				        $tampil_media = tampil_media("./upload/gambar_soal/".$d->file, 'auto','auto');
				        $html .= '<input type="hidden" name="id_soal_'.$no.'" value="'.$d->id.'">';
				        $html .= '<div class="step" id="widget_'.$no.'">';

				        $html .= '<p>'.$d->soal.'</p><p>'.$tampil_media.'</p><div class="funkyradio">';
						$q_jawaban = $this->db->query("SELECT * FROM t_option_soal WHERE id_soal = '".$d->id."' ORDER BY RAND()")->result();
						$j = 0;
						foreach ($q_jawaban as $jw) 
						{
							$checked = $arr_jawab[$d->id] == $jw->id_jawaban ? "checked" : "";
							$pc_pilihan_opsi = $jw->jawaban;
							$tampil_media_opsi =  tampil_media("./upload/gambar_opsi/".$jw->file,'auto','auto') ;
							$html .= '<div class="funkyradio-success">
				                <input type="radio" id="'.$jw->id_jawaban.'_'.$d->id.'" name="opsi_'.$no.'" value="'.$jw->id_jawaban.'" '.$checked.'> <label for="'.$jw->id_jawaban.'_'.$d->id.'"><div class="huruf_opsi">'.$this->opsi[$j].'</div> <p>'.$pc_pilihan_opsi.'</p><p>'.$tampil_media_opsi.'</p></label></div>';			
						$j++;
						}				        
				        
				        $html .= '</div></div>';
				        $no++;
				    }
				}

				$a['jam_mulai'] = $detil_tes->waktu_mulai;
				$a['jam_selesai'] = $detil_tes->waktu_selesai;
				$a['id_tes'] = $cek_detil_tes->id_jenis_soal;
				$a['no'] = $no;
				$a['html'] = $html;

				$this->load->view('v_ujian', $a);
			} else {
				redirect('adm/sudah_selesai_ujian/'.$uri4);
			}
		}

	}
	public function jvs() {
		$this->cek_aktif();
		
		$data_soal 		= $this->db->query("SELECT id, gambar, soal, opsi_a, opsi_b, opsi_c, opsi_d, opsi_e FROM t_soal ORDER BY RAND()")->result();
		
		j($data_soal);
		exit;
	}
	public function rubah_password() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_admin_id'] = $this->session->userdata('admin_id');
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));
		$ret = array();
		if ($uri3 == "simpan") {
			$p1_md5 = md5($p->p1);
			$p2_md5 = md5($p->p2);
			$p3_md5 = md5($p->p3);
			$cek_pass_lama = $this->db->query("SELECT password FROM t_admin WHERE id = '".$a['sess_admin_id']."'")->row();
			if ($cek_pass_lama->password != $p1_md5) {
				$ret['status'] = "error";
				$ret['msg'] = "Password lama tidak sama...";
			} else if ($p2_md5 != $p3_md5) {
				$ret['status'] = "error";
				$ret['msg'] = "Password baru konfirmasinya tidak sama...";
			} else if (strlen($p->p2) < 6) {
				$ret['status'] = "error";
				$ret['msg'] = "Password baru minimal terdiri dari 6 huruf..";
 			} else {
				$this->db->query("UPDATE m_admin SET password = '".$p3_md5."' WHERE id = '".$a['sess_admin_id']."'");
				$ret['status'] = "ok";
				$ret['msg'] = "Password berhasil diubah...";
			}
			j($ret);
			exit;
		} else {
			$data = $this->db->query("SELECT id, kon_id, level, username FROM m_admin WHERE id = '".$a['sess_admin_id']."'")->row();
			j($data);
			exit;
		}
	}
	public function sudah_selesai_ujian() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_nama');
//		$a['sess_konid'] = $this->session->userdata('admin_konid');
		$a['sess_kode'] = $this->session->userdata('admin_kode');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		
		$q_nilai = $this->db->query("SELECT nilai, waktu_selesai FROM t_log_soal WHERE kd_soal = $uri3 AND kd_peserta = '".$a['sess_kode']."' AND status = 'N'")->row();
		if (empty($q_nilai)) {
			redirect('adm/ikut_ujian/_/'.$uri3);
		} else {
			$a['p'] = "v_selesai_ujian";
			$a['data'] = "<div class='alert alert-danger'>Anda telah selesai mengikuti ujian ini pada : <strong style='font-size: 16px'>".tjs($q_nilai->waktu_selesai, "l")."</strong>, dan mendapatkan nilai : <strong style='font-size: 16px'>".$q_nilai->nilai."</strong></div>";
		}
		$this->load->view('aaa', $a);
	}

	/* Login Logout */
	// login peserta
	public function login_utama()
	{
		$this->load->view('v_login_utama');
	} 

	// MAU BIKIN INPUT TEXT AUTOCOMPLETE
	public function get_peserta()
	{
		$kode = $this->input->post('kode',TRUE);
		$query = $this->m_adm->get_peserta();
		$peserta = array();
		foreach($query as $d) { 
			$peserta[] = array(
				'label'=> $d->kode,
				'kd'=> $d->kode,
				'nama_peserta' => $d->nama_peserta,
			//	'pilihan_peserta' => $d->pilihan_peserta,
				'ruangan' => $d->ruangan
				);
		}
		echo json_encode($peserta);
	}

	public function act_login_peserta()
	{
		$username	= $this->input->post('username');
		$password	= $this->input->post('password');
		
	//	$password2	= md5($password);
		
		$q_data		= $this->db->query("SELECT t_peserta.*, t_ruangan.token_ujian FROM t_peserta INNER JOIN t_ruangan ON t_peserta.id_ruangan = t_ruangan.id_ruangan WHERE t_peserta.kode = '".$username."' AND t_ruangan.token_ujian = '$password'");
		$j_data		= $q_data->num_rows();
		$a_data		= $q_data->row();

		$_log		= array();
		if ($j_data === 1) {
			$sess_nama_user = "";
			if ($a_data->level == "peserta") {
				$det_user = $this->db->query("SELECT nama_peserta FROM t_peserta WHERE kode = '".$a_data->kode."'")->row();
				if (!empty($det_user)) {
					$sess_nama_user = $det_user->nama_peserta;
				}
				$this->db->query("INSERT INTO log_masuk (kode, nama, id_ruangan) VALUES ('$username', '".$a_data->nama_peserta."', '".$a_data->id_ruangan."')");
			}
			

			$data = array(
            //        'admin_id' => $a_data->id,
                    'admin_kode' => $a_data->kode,
                    'admin_level' => $a_data->level,
                    'admin_nama' => $sess_nama_user,
					'admin_valid' => true
                    );
            $this->session->set_userdata($data);
			$_log['log']['status']			= "1";
			$_log['log']['keterangan']		= "Login berhasil";
			$_log['log']['detil_admin']		= $this->session->userdata;
		} else {
			$_log['log']['status']			= "0";
			$_log['log']['keterangan']		= "Maaf, username dan password tidak ditemukan";
			$_log['log']['detil_admin']		= null;
		}
		
		j($_log);	
	}


	/* Login Logout */
	// panitia
	public function login() {
		$this->load->view('aaa_login');
	}
	
	public function act_login() {
		
		$username	= $this->input->post('username');
		$password	= $this->input->post('password');
		
		$password2	= md5($password);
		
		$q_data		= $this->db->query("SELECT * FROM t_admin WHERE username = '".$username."' AND password = '$password2'");
		$j_data		= $q_data->num_rows();
		$a_data		= $q_data->row();
		
		$_log		= array();
		if ($j_data === 1) {
			$sess_nama_user = "";
			/*
			if ($a_data->level == "siswa") {
				$det_user = $this->db->query("SELECT nama FROM m_siswa WHERE id = '".$a_data->kon_id."'")->row();
				if (!empty($det_user)) {
					$sess_nama_user = $det_user->nama;
				}
			} else */ 
			if ($a_data->level == "petugas") {
				$det_user = $this->db->query("SELECT nama FROM t_petugas_soal WHERE id = '".$a_data->kon_id."'")->row();
				if (!empty($det_user)) {
					$sess_nama_user = $det_user->nama;
				}
			} else {
				$sess_nama_user = "Administrator Pusat";
			}
			$data = array(
                    'admin_id' => $a_data->id,
                    'admin_user' => $a_data->username,
                    'admin_level' => $a_data->level,
                    'admin_konid' => $a_data->kon_id,
                    'admin_nama' => $sess_nama_user,
					'admin_valid' => true
                    );
            $this->session->set_userdata($data);
			$_log['log']['status']			= "1";
			$_log['log']['keterangan']		= "Login berhasil";
			$_log['log']['detil_admin']		= $this->session->userdata;
		} else {
			$_log['log']['status']			= "0";
			$_log['log']['keterangan']		= "Maaf, username dan password tidak ditemukan";
			$_log['log']['detil_admin']		= null;
		}
		
		j($_log);
	}
	
	public function logout() {
		/*
		$data = array(
                    'admin_id' 		=> "",
                    'admin_user' 	=> "",
                    'admin_level' 	=> "",
                    'admin_kode'	=> "",
                    'admin_konid' 	=> "",
                    'admin_nama' 	=> "",
					'admin_valid' 	=> false
                    );
        */
		$a['sess_kode'] = $this->session->userdata('admin_kode');
		$this->db->query("DELETE FROM log_masuk WHERE kode = '".$a['sess_kode']."' ");
        $this->session->sess_destroy();
        
		redirect('adm/login_utama');
	}
	//fungsi tambahan
	public function get_akhir($tabel, $field, $kode_awal, $pad) {
		$get_akhir	= $this->db->query("SELECT MAX($field) AS max FROM $tabel LIMIT 1")->row();
		$data		= (intval($get_akhir->max)) + 1;
		$last		= $kode_awal.str_pad($data, $pad, '0', STR_PAD_LEFT);
	
		return $last;
	}
	
	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
